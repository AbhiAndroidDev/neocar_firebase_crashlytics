package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.AddRiderRatingInterface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.AddRiderRatingModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 13/11/18.
 */

public class AddRiderRatingWebService {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private JSONObject details;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;
    private AddRiderRatingModel addRatingModel;
    private ArrayList<AddRiderRatingModel> arrListaddRatingModel;

    public void addRiderRating(Activity context, final String booking_id, final String riview_by_entity, String riview_by_id,
                               final String strWriteReview, final String strRiderRating,
                               final String riderId, final String isCheckedAnonymous){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest addRatingRequest = new StringRequest(Request.Method.POST, Urlstring.owner_add_review,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d(":: Response details:: ", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("booking_id", booking_id);
                params.put("riview_by_entity", riview_by_entity);
                params.put("riview_by_id", UserLoginDetails.getId());
                params.put("comment", strWriteReview);
                params.put("rider_rating", strRiderRating);
                params.put("rider_id", riderId);
                params.put("send_anonymously", isCheckedAnonymous);

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        addRatingRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(addRatingRequest);

    }


    private void Apiparsedata(String response){
        JSONObject jobj_main = null;

        try{
            jobj_main = new JSONObject(response);
            strUserDeleted = jobj_main.optJSONObject("owner_add_review").optString("user_deleted");
            Msg = jobj_main.optJSONObject("owner_add_review").optString("message");
            Status= jobj_main.optJSONObject("owner_add_review").optString("status");
            details=jobj_main.optJSONObject("owner_add_review").optJSONObject("details");

            addRatingModel = new AddRiderRatingModel();
            arrListaddRatingModel = new ArrayList<>();

            if (Status.equals(StaticClass.SuccessResult) && details != null){
                addRatingModel.setRiview_by_entity(details.optString("riview_by_entity"));
                addRatingModel.setRiview_by_id(details.optString("riview_by_id"));
                addRatingModel.setRiview_by_id(details.optString("booking_id"));
                addRatingModel.setRider_rating(details.optString("rider_rating"));
                addRatingModel.setComment(details.optString("comment"));
                addRatingModel.setRider_id(details.optString("rider_id"));
                addRatingModel.setReview_id(details.optString("review_id"));

                arrListaddRatingModel.add(addRatingModel);

            } else if (Status.equals(StaticClass.ErrorResult)){
                details = jobj_main.optJSONObject("owner_add_review").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        if ("Y".equalsIgnoreCase(strUserDeleted)){
            Log.d("d", "***strUserDeleted***"+strUserDeleted);
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            Log.d("d", "***Status 12***"+StaticClass.SuccessResult);
            if (Status.equals(StaticClass.SuccessResult)) {
                Log.d("d", "***Status 34***"+StaticClass.SuccessResult);
                ((AddRiderRatingInterface)mcontext).onAddRiderRating(arrListaddRatingModel);

                new CustomToast(mcontext, Msg);
            }
        }
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext ,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
