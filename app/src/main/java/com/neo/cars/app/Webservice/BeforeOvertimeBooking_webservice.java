package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.OverTimePaymentActivity;
import com.neo.cars.app.OvertimeBookingActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.PassengerModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomDialogText;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 14/5/18.
 */

public class BeforeOvertimeBooking_webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "",BookingId="",ExpectedRoute="", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialogText pdCusomeDialog;
    private Gson gson;
    private JSONObject details;
    private ArrayList<PassengerModel> arrlistAddlPassenger;
    private PassengerModel passengerModel;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;
    private String strbookingdate="",strDuration="",strStartTime="", strEndTime="",strReasonforextension="", strexpectedReleaseTime="";
    private String strAdditionalInformation="",strNoofBaggage="",strTotalAmount="", strNightCharge = "", strNetPayableAmount = "",
    strparking_charge = "", vehicle_city_name = "";


    public void processOvertimeBooking (Activity context, final String strBookingId, final String strDuration, final String strExpectedRoute,
                                        final String strExtensionReason, final String strExpectedTime,
                                        final String strAddlInfo, final String strBaggage,final String AddPassenger) {

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        BookingId=strBookingId;
        ExpectedRoute=strExpectedRoute;

        sharedPref = new SharedPrefUserDetails(mcontext);
        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);
        showProgressDialog();

        StringRequest processovertimebookingStringRequest = new StringRequest(Request.Method.POST, Urlstring.before_overtime_booking,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response Booking hours", response);

                        StaticClass.isBookNow = true;
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            StaticClass.isBookNow = true;
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("booking_id", strBookingId);
                params.put("duration", strDuration);
                params.put("user_id", UserLoginDetails.getId());

                if(strExpectedRoute != null){
                    params.put("expected_route", strExpectedRoute);
                }

                if(strExtensionReason != null){
                    params.put("reason_for_extension", strExtensionReason);
                }

                if(strExpectedTime != null){
                    params.put("expected_releasing_time", strExpectedTime);
                }

                if(strAddlInfo != null){
                    params.put("addl_info", strAddlInfo);
                }

                if(strBaggage != null){
                    params.put("no_of_baggage", strBaggage);
                }

                if(AddPassenger != null){
                    params.put("addl_passenger", AddPassenger);
                }

                new PrintClass("process overtime booking params******getParams***"+params);
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        processovertimebookingStringRequest.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(context).add(processovertimebookingStringRequest);

    }


    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialogText(mcontext, mcontext.getResources().getString(R.string.Loading_PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;

        JSONArray JsonArrayAdditionalPassenger=null;



        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("before_overtime_booking").optString("user_deleted");
            Msg = jobj_main.optJSONObject("before_overtime_booking").optString("message");
            Status= jobj_main.optJSONObject("before_overtime_booking").optString("status");

            details=jobj_main.optJSONObject("before_overtime_booking").optJSONObject("details");

            if (Status.equals(StaticClass.SuccessResult)){
                strbookingdate=details.optString("booking_date");
                strDuration=details.optString("duration");
                strStartTime=details.optString("start_time");
                strEndTime=details.optString("end_time");
                strReasonforextension=details.optString("reason_for_extension");
                strexpectedReleaseTime=details.optString("expected_releasing_time");
                strAdditionalInformation=details.optString("addl_info");
                strNoofBaggage=details.optString("no_of_baggage");
                strTotalAmount=details.optString("total_amount");
                strNightCharge=details.optString("night_charge");
                strNetPayableAmount=details.optString("net_payable_amount");
                strparking_charge=details.optString("parking_charge");
                vehicle_city_name=details.optString("vehicle_city_name");

                JsonArrayAdditionalPassenger=details.optJSONArray("addl_passenger");

                if (JsonArrayAdditionalPassenger!=null){
                    arrlistAddlPassenger = new ArrayList<PassengerModel>();

                    for (int i=0; i<JsonArrayAdditionalPassenger.length(); i++){
                        JSONObject jsonObjectAddlPassenger = JsonArrayAdditionalPassenger.getJSONObject(i);
                        passengerModel = new PassengerModel();
                        passengerModel.setPassenger_name(jsonObjectAddlPassenger.optString("name"));
                        passengerModel.setPassenger_contact(jsonObjectAddlPassenger.optString("contact_no"));

                        arrlistAddlPassenger.add(passengerModel);
                    }
                }

            }else if (Status.equals(StaticClass.ErrorResult)){
                details = jobj_main.optJSONObject("overtime_avalibility").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);
            }

        }catch (Exception e){
            e.printStackTrace();
        }


        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                //navigation for success status
                Intent intentOvertimePayment=new Intent(mcontext, OverTimePaymentActivity.class);

                intentOvertimePayment.putExtra("BookingId",BookingId);
                intentOvertimePayment.putExtra("ExpectedRoute",ExpectedRoute);
                intentOvertimePayment.putExtra("Date",strbookingdate);
                intentOvertimePayment.putExtra("Duration",strDuration);
                intentOvertimePayment.putExtra("StartTime",strStartTime);
                intentOvertimePayment.putExtra("EndTime",strEndTime);
                intentOvertimePayment.putExtra("ReasonExtension",strReasonforextension);
                intentOvertimePayment.putExtra("ExpectedReleaseTime",strexpectedReleaseTime);
                intentOvertimePayment.putExtra("AdditionalInformation",strAdditionalInformation);
                intentOvertimePayment.putExtra("NoofBaggage",strNoofBaggage);
                intentOvertimePayment.putExtra("TotalAmount",strTotalAmount);
                intentOvertimePayment.putExtra("NightCharge",strNightCharge);
                intentOvertimePayment.putExtra("NetPayableAmount",strNetPayableAmount);
                intentOvertimePayment.putExtra("parking_charge",strparking_charge);
                intentOvertimePayment.putExtra("vehicle_city_name",vehicle_city_name);
                if(JsonArrayAdditionalPassenger != null) {
                    intentOvertimePayment.putExtra("addl_passenger", JsonArrayAdditionalPassenger.toString());
                }
                intentOvertimePayment.putParcelableArrayListExtra("PassengerList", arrlistAddlPassenger);
                mcontext.startActivity(intentOvertimePayment);
                mcontext.finish();


            } else {
                new CustomToast(mcontext, Msg);
            }
        }
    }
}
