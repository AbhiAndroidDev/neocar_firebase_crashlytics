package com.neo.cars.app.VolleyParser;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Avijit Ballav
 * Developer: Avijit Ballav
 * Designation: Senior Android Application Developer
 * Contact: 9836484275/ 9062908025
 */

public class VolleyRequestHandler {
    public ProgressDialog pDialog;
    HashMap<String, String> postParams=new HashMap<>();

    public StringRequest GeneralVolleyRequestWithPostParam(Context context, final int progress, String requestUrl,  HashMap<String, String> postParams1, final VolleyCallback volleyCallback) {

        postParams=postParams1;

        if (progress == 1) {
            try {
                pDialog = new ProgressDialog(context);
                pDialog.setMessage("Please wait...");
                pDialog.setCancelable(false);
                pDialog.setIndeterminate(true);
                pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        StringRequest str = new StringRequest(Request.Method.POST,
                requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (progress == 1) {
                            try {
                                pDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        Log.e("Volley Result>>", response);
                        volleyCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progress == 1) {
                            try {
                                pDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        Log.e("Volley Error>>", error.toString());
                        JSONObject errorObject = new JSONObject();
                        try {
                            errorObject.put("Data not found", "1");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        volleyCallback.onFailure(errorObject.toString());
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                return params;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = postParams;
                params=postParams;
                System.out.println("params**********"+params);
                return params;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded";
            }
        };
        return str;
    }


    public StringRequest GeneralVolleyRequest(Context context, String requestUrl, final VolleyCallback volleyCallback) {
        try {
            pDialog = new ProgressDialog(context);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.setIndeterminate(true);
            pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

        StringRequest str = new StringRequest(Request.Method.POST,
                requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            pDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        volleyCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            pDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Log.e("", error.toString());
                        JSONObject errorObject = new JSONObject();
                        try {
                            errorObject.put("Data not found", "1");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        volleyCallback.onFailure(errorObject.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        return str;
    }

    public StringRequest GeneralVolleyRequestwithoutDialog(Context context, String requestUrl, final VolleyCallback volleyCallback) {

        StringRequest str = new StringRequest(Request.Method.POST,
                requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        volleyCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Log.e("", error.toString());
                        JSONObject errorObject = new JSONObject();
                        try {
                            errorObject.put("Data not found", "1");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        volleyCallback.onFailure(errorObject.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/x-www-form-urlencoded");
                return params;
            }
        };
        return str;
    }


    public StringRequest GeneralVolleyRequestusinGet(final Context context, final int progress, String requestUrl, final VolleyCallback volleyCallback) {

        if (progress == 1) {
            try {
                pDialog = new ProgressDialog(context);
                pDialog.setMessage("Please wait...");
                pDialog.setCancelable(false);
                pDialog.setIndeterminate(true);
                pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        StringRequest str = new StringRequest(Request.Method.GET,
                requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (progress == 1) {
                            try {
                                pDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        volleyCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progress == 1) {
                            try {
                                pDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        Log.e("", error.toString());
                        //Toast.makeText(context,error.toString(),Toast.LENGTH_LONG).show();
                        JSONObject errorObject = new JSONObject();
                        try {
                            errorObject.put("Data not found", "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        volleyCallback.onFailure(errorObject.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json; charset=utf-8");
                return params;
            }
        };
        return str;
    }


    public StringRequest GeneralVolleyRequestusinPost(final Context context, final int progress, String requestUrl, final VolleyCallback volleyCallback) {

        if (progress == 1) {
            try {
                pDialog = new ProgressDialog(context);
                pDialog.setMessage("Please wait...");
                pDialog.setCancelable(false);
                pDialog.setIndeterminate(true);
                pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        StringRequest str = new StringRequest(Request.Method.POST,
                requestUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (progress == 1) {
                            try {
                                pDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                        volleyCallback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (progress == 1) {
                            try {
                                pDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        Log.e("", error.toString());
                        //Toast.makeText(context,error.toString(),Toast.LENGTH_LONG).show();
                        JSONObject errorObject = new JSONObject();
                        try {
                            errorObject.put("volley_error", "1");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        volleyCallback.onFailure(errorObject.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                return params;
            }
        };
        return str;
    }

    public interface VolleyCallback {
        void onSuccess(String result);

        void onFailure(String fail);
    }

}
