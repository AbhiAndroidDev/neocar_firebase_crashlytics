package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.UpdatePassword_Webservice;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

/**
 * Created by parna on 20/3/18.
 */

public class UpdatePasswordActivity extends AppCompatActivity {

    private CustomEditTextTitilliumWebRegular etOldPassword, etNewPassword, etConfNewPassword;
    private CustomButtonTitilliumSemibold btnUpdatePasword;
    private Context context;
    private String strOldPass="", strNewPass="", strConfNewPass="";
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private ImageButton ibNavigateMenu;
    private RelativeLayout rlBackLayout;

    private int transitionflag = StaticClass.transitionflagNext;
    private BottomView bottomview = new BottomView();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_password);

        new AnalyticsClass(UpdatePasswordActivity.this);

        Initialize();
        Listener();
    }

    private void Initialize(){
        context = this;

        etOldPassword = findViewById(R.id.etOldPassword);
        etNewPassword = findViewById(R.id.etNewPassword);
        etConfNewPassword = findViewById(R.id.etConfNewPassword);
        btnUpdatePasword = findViewById(R.id.btnUpdatePasword);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvUpdatePass));
        rlBackLayout = findViewById(R.id.rlBackLayout);

        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);

        bottomview.BottomView(UpdatePasswordActivity.this, StaticClass.Menu_profile);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Listener(){

        btnUpdatePasword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updatePasswordChecking();
            }
        });

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
    }


    private void updatePasswordChecking(){
        etOldPassword.setError(null);
        etNewPassword.setError(null);
        etConfNewPassword.setError(null);

        boolean cancel = false;
        View focusView = null;

        strOldPass = etOldPassword.getText().toString().trim();
        strNewPass = etNewPassword.getText().toString().trim();
        strConfNewPass = etConfNewPassword.getText().toString().trim();


        //Reg field validation check
        if (TextUtils.isEmpty(strOldPass)) {
            etOldPassword.setError(getString(R.string.error_field_required));
            focusView = etOldPassword;
            cancel = true;

        }else if (TextUtils.isEmpty(strNewPass)) {
            etNewPassword.setError(getString(R.string.error_field_required));
            focusView = etNewPassword;
            cancel = true;

        }else if (TextUtils.isEmpty(strConfNewPass)) {
            etConfNewPassword.setError(getString(R.string.error_field_required));
            focusView = etConfNewPassword;
            cancel = true;

        }else if (cancel) {
            // form field with an error.
            focusView.requestFocus();

        }else{
            if (NetWorkStatus.isNetworkAvailable(this)) {
                new UpdatePassword_Webservice().UpdatePassword(this, strOldPass, strNewPass, strConfNewPass);
            }else{
                startActivity(new Intent(this, NetworkNotAvailable.class));
            }
        }
    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(UpdatePasswordActivity.this, transitionflag);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfile){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) UpdatePasswordActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
