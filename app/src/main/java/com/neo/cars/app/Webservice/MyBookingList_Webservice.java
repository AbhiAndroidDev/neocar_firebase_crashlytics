package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.MyBookingList_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.MyBookingListModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by joydeep on 4/5/18.
 */

public class MyBookingList_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private ArrayList<MyBookingListModel> arrlistVehicle = new ArrayList<>();
    private MyBookingList_Interface IMyBookingList_Interface;
    private int transitionflag = StaticClass.transitionflagNext;

    public void CarseekerBookingList(Activity context,final String
            booking_type, MyBookingList_Interface mybookingList_Interface ){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        IMyBookingList_Interface = mybookingList_Interface;

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest userVehicleListRequest  = new StringRequest(Request.Method.POST, Urlstring.carseeker_booking_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response****", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id",UserLoginDetails.getId());
                params.put("booking_type",booking_type);

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        userVehicleListRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(userVehicleListRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Apiparsedata(String response){

        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("carseeker_booking_list").optString("user_deleted");
            Msg = jobj_main.optJSONObject("carseeker_booking_list").optString("message");
            Status= jobj_main.optJSONObject("carseeker_booking_list").optString("status");

            JSONArray details=jobj_main.optJSONObject("carseeker_booking_list").optJSONArray("details");
            arrlistVehicle = new ArrayList<>();
            if (Status.equals(StaticClass.SuccessResult)){
                for (int i=0; i<details.length(); i++){

                    MyBookingListModel vehicleTypeModel = new MyBookingListModel();

                    vehicleTypeModel.setBooking_id(details.getJSONObject(i).optString("booking_id"));
                    vehicleTypeModel.setVehicle_id(details.getJSONObject(i).optString("vehicle_id"));
                    vehicleTypeModel.setDriver_id(details.getJSONObject(i).optString("driver_id"));
                    vehicleTypeModel.setBooking_date(details.getJSONObject(i).optString("booking_date"));
                    vehicleTypeModel.setStart_time(details.getJSONObject(i).optString("start_time"));
                    vehicleTypeModel.setEnd_time(details.getJSONObject(i).optString("end_time"));
                    vehicleTypeModel.setTotal_passenger(details.getJSONObject(i).optString("total_passenger"));
                    vehicleTypeModel.setPickup_location(details.getJSONObject(i).optString("pickup_location"));
                    vehicleTypeModel.setDrop_location(details.getJSONObject(i).optString("drop_location"));
                    vehicleTypeModel.setCustomer_id(details.getJSONObject(i).optString("customer_id"));
                    vehicleTypeModel.setVehicle_image(details.getJSONObject(i).optString("vehicle_image"));
                    vehicleTypeModel.setParent_booking_id(details.getJSONObject(i).optString("parent_booking_id"));
                    vehicleTypeModel.setDuration(details.getJSONObject(i).optString("duration"));
                    vehicleTypeModel.setTotal_amount(details.getJSONObject(i).optString("total_amount"));
                    vehicleTypeModel.setStatus(details.getJSONObject(i).optString("status"));
                    vehicleTypeModel.setReview_added(details.getJSONObject(i).optString("review_added"));
                    vehicleTypeModel.setVehicle_rating(details.getJSONObject(i).optString("vehicle_rating"));
                    vehicleTypeModel.setDriver_rating(details.getJSONObject(i).optString("driver_rating"));
                    vehicleTypeModel.setIs_overtime(details.getJSONObject(i).optString("is_overtime"));
                    vehicleTypeModel.setOvertime_ongoing(details.getJSONObject(i).optString("overtime_ongoing"));


                    arrlistVehicle.add(vehicleTypeModel);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }


        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                IMyBookingList_Interface.onMyBookingList(arrlistVehicle);
            }


        }

    }
}
