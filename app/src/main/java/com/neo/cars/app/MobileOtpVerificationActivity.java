package com.neo.cars.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.neo.cars.app.Interface.MobileOtpVerificationInterface;
import com.neo.cars.app.Interface.OtpVerification_Interface;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.GetLoginOtp_Webservice;
import com.neo.cars.app.Webservice.MobileOTPWebservice;

public class MobileOtpVerificationActivity extends AppCompatActivity implements TextWatcher, View.OnClickListener, MobileOtpVerificationInterface {

    private TextView tvMobNoForOtp, tvHereForMoNoEdit, tvHereForResendOtp, tvIncorrectOtpMsg;
    private EditText etOtpDigit1, etOtpDigit2, etOtpDigit3, etOtpDigit4, etOtpDigit5, etOtpDigit6;
    private Button btnSubmit;
    private String enteredOtp ="", otpDigit1 = "";
    private LinearLayout ll_editNumber, ll_ResendOtp;
    private ConnectionDetector cd;
    private UserLoginDetailsModel UserLoginDetails;
    private SharedPrefUserDetails shprefs;
    private Gson gson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mobile_otp_verification);
        new AnalyticsClass(MobileOtpVerificationActivity.this);
        initialize();
        listener();

        if (cd.isConnectingToInternet()){
            (new GetLoginOtp_Webservice()).GetLoginOtp(this, UserLoginDetails.getMobile());
        }
    }

    private void initialize() {

        cd = new ConnectionDetector(this);
        shprefs = new SharedPrefUserDetails(this);
        gson = new Gson();
        UserLoginDetails = new UserLoginDetailsModel();
        shprefs = new SharedPrefUserDetails(this);
        String struserdetails = shprefs.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);


        tvMobNoForOtp = findViewById(R.id.tvMobNoForOtp);
        tvMobNoForOtp.setText(UserLoginDetails.getMobile());
        tvHereForMoNoEdit = findViewById(R.id.tvHereForMoNoEdit);
        tvHereForResendOtp = findViewById(R.id.tvHereForResendOtp);
        tvIncorrectOtpMsg = findViewById(R.id.tvIncorrectOtpMsg);
        ll_editNumber = findViewById(R.id.ll_editNumber);
        ll_ResendOtp = findViewById(R.id.ll_ResendOtp);

        etOtpDigit1 = findViewById(R.id.etOtpDigit1);
        etOtpDigit2 = findViewById(R.id.etOtpDigit2);
        etOtpDigit3 = findViewById(R.id.etOtpDigit3);
        etOtpDigit4 = findViewById(R.id.etOtpDigit4);
        etOtpDigit5 = findViewById(R.id.etOtpDigit5);
        etOtpDigit6 = findViewById(R.id.etOtpDigit6);

        btnSubmit = findViewById(R.id.btnSubmit);

    }

    private void listener() {

        etOtpDigit1.addTextChangedListener(this);
        etOtpDigit2.addTextChangedListener(this);
        etOtpDigit3.addTextChangedListener(this);
        etOtpDigit4.addTextChangedListener(this);
        etOtpDigit5.addTextChangedListener(this);
        etOtpDigit6.addTextChangedListener(this);

        btnSubmit.setOnClickListener(this);
        ll_editNumber.setOnClickListener(this);
        ll_ResendOtp.setOnClickListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!TextUtils.isEmpty(shprefs.getMobile()))
        tvMobNoForOtp.setText(shprefs.getMobile());

        if (StaticClass.IsMobileNoChanged){
            etOtpDigit1.setText("");
            etOtpDigit2.setText("");
            etOtpDigit3.setText("");
            etOtpDigit4.setText("");
            etOtpDigit5.setText("");
            etOtpDigit6.setText("");
            etOtpDigit1.requestFocus();
            StaticClass.IsMobileNoChanged = false;
            tvIncorrectOtpMsg.setVisibility(View.GONE);
        }
    }


    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.ll_editNumber:
                startActivity(new Intent(MobileOtpVerificationActivity.this, EditMobileActivity.class));
                break;

            case R.id.ll_ResendOtp:

                if (!TextUtils.isEmpty(shprefs.getMobile())){
                    (new GetLoginOtp_Webservice()).GetLoginOtp(this, shprefs.getMobile());
                }else {

                    (new GetLoginOtp_Webservice()).GetLoginOtp(this, UserLoginDetails.getMobile());
                }

                etOtpDigit1.setText(""); etOtpDigit2.setText(""); etOtpDigit3.setText(""); etOtpDigit4.setText(""); etOtpDigit5.setText(""); etOtpDigit6.setText("");
                tvIncorrectOtpMsg.setVisibility(View.GONE);

                break;

            case R.id.btnSubmit:
                checkValidation();
                break;

            default:
                break;
        }
    }


    private void checkValidation(){

        if (!TextUtils.isEmpty(etOtpDigit1.getText().toString()) && !TextUtils.isEmpty(etOtpDigit2.getText().toString())
                && !TextUtils.isEmpty(etOtpDigit3.getText().toString()) && !TextUtils.isEmpty(etOtpDigit4.getText().toString())
                && !TextUtils.isEmpty(etOtpDigit5.getText().toString()) && !TextUtils.isEmpty(etOtpDigit6.getText().toString())){

            enteredOtp = etOtpDigit1.getText().toString()+etOtpDigit2.getText().toString()+etOtpDigit3.getText().toString()
                    +etOtpDigit4.getText().toString()+etOtpDigit5.getText().toString()+etOtpDigit6.getText().toString();

            if (cd.isConnectingToInternet()){

                if (!TextUtils.isEmpty(shprefs.getMobile())) {

                    (new MobileOTPWebservice()).mobileOTPWebservice(this, shprefs.getMobile(), enteredOtp, UserLoginDetails.getUser_type(), this);
                }else {
                    (new MobileOTPWebservice()).mobileOTPWebservice(this, UserLoginDetails.getMobile(), enteredOtp, UserLoginDetails.getUser_type(), this);

                }

            }else {
                startActivity(new Intent(this, NetworkNotAvailable.class));
            }

        }else{
            new CustomToast(this, getResources().getString(R.string.otpValidMsg));
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

        if (editable.length() == 1){
            if (etOtpDigit1.length() == 1){
                etOtpDigit2.requestFocus();
            }
            if (etOtpDigit2.length() == 1){
                etOtpDigit3.requestFocus();
            }
            if (etOtpDigit3.length() == 1){
                etOtpDigit4.requestFocus();
            }
            if (etOtpDigit4.length() == 1){
                etOtpDigit5.requestFocus();
            }
            if (etOtpDigit5.length() == 1){
                etOtpDigit6.requestFocus();
            }
            if (etOtpDigit6.length() == 1){
//                etOtpDigit1.requestFocus();
                hideKeyboard(this);
//                etOtpDig8it6.setFocusable(false);
            }
        }
//        else if (editable.length() == 0){
//
//            if (etOtpDigit1.length() == 0){
//                etOtpDigit1.requestFocus();
//
//            }else if (etOtpDigit2.length() == 0){
//                etOtpDigit2.requestFocus();
//
//            }else if (etOtpDigit3.length() == 0){
//                etOtpDigit3.requestFocus();
//
//            }else if (etOtpDigit4.length() == 0){
//                etOtpDigit4.requestFocus();
//
//            }else if (etOtpDigit5.length() == 0){
//                etOtpDigit5.requestFocus();
//
//            }else if (etOtpDigit6.length() == 0){
//                etOtpDigit6.requestFocus();
//            }
//        }
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public void mobileOtpVerification(String status, String st_mobile, String otp, String user_type) {

        if (status.equalsIgnoreCase(StaticClass.ErrorResult)){
            tvIncorrectOtpMsg.setVisibility(View.VISIBLE);

            etOtpDigit1.setText("");
            etOtpDigit2.setText("");
            etOtpDigit3.setText("");
            etOtpDigit4.setText("");
            etOtpDigit5.setText("");
            etOtpDigit6.setText("");
            etOtpDigit1.requestFocus();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) MobileOtpVerificationActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

}
