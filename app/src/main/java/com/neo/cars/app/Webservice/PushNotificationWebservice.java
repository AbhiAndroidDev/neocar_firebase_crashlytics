package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.PushNotificationInterface;
import com.neo.cars.app.R;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 31/5/18.
 */

public class PushNotificationWebservice {

    Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    JSONObject details;
    private SharedPrefUserDetails sharedPref;
    private Gson gson;
    String userid="",notificationstatus="";
    private CustomDialog pdCusomeDialog;
    String strNotificationStatus="";
    private int transitionflag = StaticClass.transitionflagNext;


    public void PushNotification(Activity context, final String struser_id,String strnotification_status){
        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        sharedPref = new SharedPrefUserDetails(mcontext);
        userid=struser_id;
        notificationstatus=strnotification_status;
        gson = new Gson();
        showProgressDialog();


        StringRequest PushNotificationRequest = new StringRequest(Request.Method.POST, Urlstring.toggle_notification,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                Log.e("paramUserId",userid);

                params.put("user_id", userid);
                params.put("notification_status",notificationstatus);


                new PrintClass("PushNotification******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }

        };

        PushNotificationRequest.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(context).add(PushNotificationRequest);
    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext, mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("toggle_notification").optString("user_deleted");
            Msg = jobj_main.optJSONObject("toggle_notification").optString("message");
            Status= jobj_main.optJSONObject("toggle_notification").optString("status");



        }catch (Exception e){
            e.printStackTrace();
        }


        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)){

                details=jobj_main.optJSONObject("toggle_notification").optJSONObject("details");

                strNotificationStatus=details.optString("notification_status");

            }else if (Status.equals(StaticClass.ErrorResult)){
                new CustomToast(mcontext, Msg);
            }
            if (Status.equals(StaticClass.SuccessResult)) {
                ((PushNotificationInterface)mcontext).onpushNotificationInterface(strNotificationStatus);
            } else {
                new CustomToast(mcontext, Msg);
            }
        }

    }

}
