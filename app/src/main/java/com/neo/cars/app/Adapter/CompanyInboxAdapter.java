package com.neo.cars.app.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.InboxModal;

import java.util.List;

public class CompanyInboxAdapter extends RecyclerView.Adapter<CompanyInboxAdapter.MyViewHolder> {

    private List<InboxModal> InboxModelList;
    private Context mcontext;
    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_messagetitle,tv_message,tv_Datetime;
        private RelativeLayout relative_messageListBackground;

        public MyViewHolder(View view) {
            super(view);

            tv_messagetitle= view.findViewById(R.id.tv_messagetitle_company);
            tv_message= view.findViewById(R.id.tv_message_company);
            tv_Datetime= view.findViewById(R.id.tv_Datetime_company);
            relative_messageListBackground= view.findViewById(R.id.relative_messageListBackground_company);
        }
    }

    public CompanyInboxAdapter(Context context,List<InboxModal> myInboxModelList) {
        mcontext= context;
        InboxModelList = myInboxModelList;
    }

    @Override
    public CompanyInboxAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.company_inbox_inflate, parent, false);
        return new CompanyInboxAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CompanyInboxAdapter.MyViewHolder holder, int position) {
        holder.tv_messagetitle.setText(InboxModelList.get(position).getTitle());
        holder.tv_message.setText(InboxModelList.get(position).getDescription());
        holder.tv_Datetime.setText(InboxModelList.get(position).getCreated_at());

        if(InboxModelList.get(position).getIs_read().equalsIgnoreCase("Y")) {
            holder.relative_messageListBackground.setBackgroundColor(mcontext.getResources().getColor(R.color.white));
        }else if(InboxModelList.get(position).getIs_read().equalsIgnoreCase("N")){
            holder.relative_messageListBackground.setBackgroundColor(mcontext.getResources().getColor(R.color.unread_message));
        }
    }

    @Override
    public int getItemCount() {
        return InboxModelList.size();
    }
}
