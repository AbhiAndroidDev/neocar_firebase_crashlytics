package com.neo.cars.app;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

public class CompanyHelpActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvProfile;
    private ImageButton ibNavigateMenu;
    private RelativeLayout rlBackLayout;
    private CustomTextviewTitilliumWebRegular tvHowToUse, tvFAQ, tvContactUs, tvAboutUs, tvVersion;

    private int transitionflag = StaticClass.transitionflagNext;
    private BottomViewCompany bottomview = new BottomViewCompany();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_help);

        new AnalyticsClass(CompanyHelpActivity.this);

        Initialize();
        Listener();
    }

    private void Initialize(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvHelpHeader));
        rlBackLayout = findViewById(R.id.rlBackLayout);
        tvHowToUse = findViewById(R.id.tvHowToUse);
        tvFAQ = findViewById(R.id.tvFAQ);
        tvContactUs = findViewById(R.id.tvContactUs);
        tvAboutUs = findViewById(R.id.tvAboutUs);
        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        tvVersion = findViewById(R.id.tvVersion);


        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionName = pInfo.versionName;

            tvVersion.setText("Version "+versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        bottomview.BottomViewCompany(CompanyHelpActivity.this,StaticClass.Menu_profile_company);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Listener(){
        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        tvHowToUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent howtouseIntent = new Intent(CompanyHelpActivity.this, CompanyCmsPageActivity.class);
                howtouseIntent.putExtra("cms", StaticClass.HowToUse);
                startActivity(howtouseIntent);

            }
        });

        tvFAQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent faqIntent = new Intent(CompanyHelpActivity.this, CompanyCmsPageActivity.class);
                faqIntent.putExtra("cms", StaticClass.FAQ);
                startActivity(faqIntent);

            }
        });

        tvContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contactusIntent = new Intent(CompanyHelpActivity.this, CompanyCmsPageActivity.class);
                contactusIntent.putExtra("cms", StaticClass.ContactUs);
                startActivity(contactusIntent);

            }
        });

        tvAboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent contactusIntent = new Intent(CompanyHelpActivity.this, CompanyCmsPageActivity.class);
                contactusIntent.putExtra("cms", StaticClass.AboutUs);
                startActivity(contactusIntent);

            }
        });
    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyHelpActivity.this, transitionflag);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfileCompany){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }
    }
}
