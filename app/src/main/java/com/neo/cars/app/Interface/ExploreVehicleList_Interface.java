package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.ExploreModel;

import java.util.ArrayList;

/**
 * Created by parna on 11/4/18.
 */

public interface ExploreVehicleList_Interface {

    public void ExploreVehicleListInterface(String strTotalRecord, String strPageLimit,  ArrayList<ExploreModel> arrlistExplore);

}
