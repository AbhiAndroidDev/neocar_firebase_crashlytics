package com.neo.cars.app;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.Interface.DeleteNotification_Interface;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.DeleteNotification_Webservice;
import com.neo.cars.app.dialog.BottomSheetDialogPositiveNegative;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

public class CompanyInboxDetails extends AppCompatActivity implements CallBackButtonClick,DeleteNotification_Interface {

    private int transitionflag = StaticClass.transitionflagNext;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private TextView tv_messagetitle,tv_message,tv_Datetime;
    private RelativeLayout iv_delete;

    private RelativeLayout rlBackLayout;
    private SwipeRefreshLayout swipeRefreshLayout;
    private SharedPrefUserDetails sharedPrefUser;
    private BottomViewCompany bottomview = new BottomViewCompany();

    private String str_messagetitle,str_message,str_Datetime,notificationId, notification_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_inbox_details);

        str_messagetitle=getIntent().getExtras().getString("messagetitle");
        str_message=getIntent().getExtras().getString("message");
        str_Datetime=getIntent().getExtras().getString("Datetime");
        notificationId=getIntent().getExtras().getString("notificationId");
        notification_type=getIntent().getExtras().getString("notification_type");

        sharedPrefUser = new SharedPrefUserDetails(CompanyInboxDetails.this);
//        sharedPrefUser.setNewNotificationStatus(false);
        sharedPrefUser.putBottomViewCompany( StaticClass.Menu_Messages_company);

        new AnalyticsClass(CompanyInboxDetails.this);

        Initialize();
        Listener();
    }

    private void Initialize() {

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.inboxDetails));

        rlBackLayout = findViewById(R.id.rlBackLayout);

        tv_messagetitle= findViewById(R.id.tv_messagetitle);
        tv_message= findViewById(R.id.tv_message);
        tv_Datetime= findViewById(R.id.tv_Datetime);

        iv_delete= findViewById(R.id.iv_delete);

        // modified by tb
        if ( notification_type !=null && notification_type.equals("admin_send")){
            iv_delete.setVisibility(View.GONE);
        }else {
            iv_delete.setVisibility(View.VISIBLE);
        }

        tv_messagetitle.setText(str_messagetitle);
        tv_message.setText(str_message);
        tv_Datetime.setText(str_Datetime);

        bottomview.BottomViewCompany(CompanyInboxDetails.this, StaticClass.Menu_Messages_company);

    }

    private void Listener() {

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });


        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BottomSheetDialogPositiveNegative bsd = new BottomSheetDialogPositiveNegative(CompanyInboxDetails.this,
                        CompanyInboxDetails.this,
                        CompanyInboxDetails.this.getResources().getString(R.string.Delete_alert_msg),
                        CompanyInboxDetails.this.getResources().getString(R.string.yes),
                        CompanyInboxDetails.this.getResources().getString(R.string.no));
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyInboxDetails.this, transitionflag);
    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        sharedPrefUser.putBottomViewCompany( StaticClass.Menu_Messages_company);
//        if(StaticClass.BottomProfileCompany){
//            finish();
//        }

//        if (StaticClass.isLoginFalg) {
//            transitionflag = StaticClass.transitionflagBack;
//            finish();
//
//        }
    }


    @Override
    public void onButtonClick(String strButtonText) {

        if (strButtonText.equals(CompanyInboxDetails.this.getResources().getString(R.string.yes))) {

            if (NetWorkStatus.isNetworkAvailable(CompanyInboxDetails.this)) {
                new DeleteNotification_Webservice().DeleteNotification(CompanyInboxDetails.this,notificationId);
            } else {
                new CustomToast(CompanyInboxDetails.this,getResources().getString(R.string.no_internet_connection_try_later));
            }
        }

    }

    @Override
    public void onDeleteNotification() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }
}
