package com.neo.cars.app.SetGet;

/**
 * Created by parna on 23/10/18.
 */

public class VehicleSearchModel {

    String id;
    String name;
    String isState;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsState() {
        return isState;
    }

    public void setIsState(String isState) {
        this.isState = isState;
    }
}
