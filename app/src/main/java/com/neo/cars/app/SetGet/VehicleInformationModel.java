package com.neo.cars.app.SetGet;

import java.util.ArrayList;

/**
 * Created by joydeep on 7/5/18.
 */

public class VehicleInformationModel {

    String vehicle_make,vehicle_type, vehicle_model,vehicle_year,vehicle_km_travelled,vehicle_special_info,vehicle_reg_number,
            license_plate_no,max_luggage,max_passenger;


    String id;
    String vehicle_type_id;
    String make;
    String model;
    String year;
    String state_id;
    String city_id;
    String km_travelled, description, ac_available, hourly_rate, hourly_overtime_rate, overtime_available, user_id, driver_id,
            license_plate_image, tax_token_image, pcu_paper_image, status, is_active_by_user, is_deleted,
            created_at, updated_at;


    public String getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    ArrayList<VehicleGalleryModel> image_gallery;

    public String getVehicle_make() {
        return vehicle_make;
    }

    public void setVehicle_make(String vehicle_make) {
        this.vehicle_make = vehicle_make;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public String getVehicle_year() {
        return vehicle_year;
    }

    public void setVehicle_year(String vehicle_year) {
        this.vehicle_year = vehicle_year;
    }

    public String getVehicle_km_travelled() {
        return vehicle_km_travelled;
    }

    public void setVehicle_km_travelled(String vehicle_km_travelled) {
        this.vehicle_km_travelled = vehicle_km_travelled;
    }

    public String getVehicle_special_info() {
        return vehicle_special_info;
    }

    public void setVehicle_special_info(String vehicle_special_info) {
        this.vehicle_special_info = vehicle_special_info;
    }

    public String getVehicle_reg_number() {
        return vehicle_reg_number;
    }

    public void setVehicle_reg_number(String vehicle_reg_number) {
        this.vehicle_reg_number = vehicle_reg_number;
    }

    public String getLicense_plate_no() {
        return license_plate_no;
    }

    public void setLicense_plate_no(String license_plate_no) {
        this.license_plate_no = license_plate_no;
    }

    public String getMax_luggage() {
        return max_luggage;
    }

    public void setMax_luggage(String max_luggage) {
        this.max_luggage = max_luggage;
    }

    public String getMax_passenger() {
        return max_passenger;
    }

    public void setMax_passenger(String max_passenger) {
        this.max_passenger = max_passenger;
    }

    public ArrayList<VehicleGalleryModel> getImage_gallery() {
        return image_gallery;
    }

    public void setImage_gallery(ArrayList<VehicleGalleryModel> image_gallery) {
        this.image_gallery = image_gallery;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVehicle_type_id() {
        return vehicle_type_id;
    }

    public void setVehicle_type_id(String vehicle_type_id) {
        this.vehicle_type_id = vehicle_type_id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getKm_travelled() {
        return km_travelled;
    }

    public void setKm_travelled(String km_travelled) {
        this.km_travelled = km_travelled;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAc_available() {
        return ac_available;
    }

    public void setAc_available(String ac_available) {
        this.ac_available = ac_available;
    }

    public String getHourly_rate() {
        return hourly_rate;
    }

    public void setHourly_rate(String hourly_rate) {
        this.hourly_rate = hourly_rate;
    }

    public String getHourly_overtime_rate() {
        return hourly_overtime_rate;
    }

    public void setHourly_overtime_rate(String hourly_overtime_rate) {
        this.hourly_overtime_rate = hourly_overtime_rate;
    }

    public String getOvertime_available() {
        return overtime_available;
    }

    public void setOvertime_available(String overtime_available) {
        this.overtime_available = overtime_available;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getLicense_plate_image() {
        return license_plate_image;
    }

    public void setLicense_plate_image(String license_plate_image) {
        this.license_plate_image = license_plate_image;
    }

    public String getTax_token_image() {
        return tax_token_image;
    }

    public void setTax_token_image(String tax_token_image) {
        this.tax_token_image = tax_token_image;
    }

    public String getPcu_paper_image() {
        return pcu_paper_image;
    }

    public void setPcu_paper_image(String pcu_paper_image) {
        this.pcu_paper_image = pcu_paper_image;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIs_active_by_user() {
        return is_active_by_user;
    }

    public void setIs_active_by_user(String is_active_by_user) {
        this.is_active_by_user = is_active_by_user;
    }

    public String getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(String is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
