package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.neo.cars.app.Adapter.ExploreCarAdapter;
import com.neo.cars.app.Adapter.ExploreGrideCarAdapter;
import com.neo.cars.app.Interface.ExploreVehicleList_Interface;
import com.neo.cars.app.Interface.ExploreViewMore_Interface;
import com.neo.cars.app.SetGet.ExploreModel;
import com.neo.cars.app.SetGet.ExploreVehicleListModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.ExploreVehicleList_Webservice;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;
import java.util.List;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_FLING;
import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

/**
 * Created by parna on 13/3/18.
 */

public class ExploreActivity  extends AppCompatActivity implements ExploreVehicleList_Interface,
        ExploreViewMore_Interface {

    //ExploreReffresh_Interface
    //SwipeRefreshLayout.OnRefreshListener

    private int transitionflag = StaticClass.transitionflagNext;
    private Context context;
    private RecyclerView rcvExploreCar;
    private ExploreCarAdapter exploreCarAdapter;
    private ExploreGrideCarAdapter exploreGrideCarAdapter;
    private List<ExploreModel> exploreCarModelList = new ArrayList<>();
    private ArrayList<ExploreVehicleListModel> arr_ExploreVehicleListModel=new ArrayList<>();
    private LinearLayoutManager layoutManagerVertical;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvSearchCity, filterDate, filterDuration, tvBadge ;
    private ImageView ivFilter, ivCancel;
    private RelativeLayout rlSearchCity;
    private CustomTitilliumTextViewSemiBold tvNoExploreCarFound;
    private String strMinPrice = Integer.toString(StaticClass.minPrice), strMaxPrice=Integer.toString(StaticClass.maxPrice), strStartDate="", strEndDate="", strDuration="", strVehicleTypeId="",
            strVehicleTypeName="", strNoOfPassenger="0", strNoOfBaggage="0", strNeedOvertime="", strNeedAc="", strCityId="",
            strStateId = "", strPickUpTime="", strModelId="", strCityName="", strPageCount="", strVehicleBrandId="", strVehicleBrandName = "";
    private LinearLayout llFilterLinearLayout;

    private RelativeLayout rlBackLayout;
    private String isState = "";

    private SwipeRefreshLayout swipeRefreshLayout;
    private BottomView bottomview = new BottomView();
    private SharedPrefUserDetails sharedPrefUser;

    private int pageCount=0, intOffset=5;
    private ArrayList<ExploreModel> arrlistExplore;

    private String monthName, day;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_explore);

        new AnalyticsClass(ExploreActivity.this);

        Initialize();
        Listener();
        StaticClass.isClearClicked = false;
        Intent intent = getIntent();
        if (intent != null) {
            strCityId = intent.getStringExtra("CityId");
            strCityName = intent.getStringExtra("CityName");
            rcvExploreCar.setVisibility(View.GONE);

            if (!"".equals(strCityName) && strCityName != null) {
                tvSearchCity.setText(strCityName);
//                ivCancel.setVisibility(View.VISIBLE);
            }

            strStartDate = intent.getStringExtra("FromDate");
            strPickUpTime = intent.getStringExtra("PickUpTime");
            strDuration = intent.getStringExtra("Duration");
            monthName = intent.getStringExtra("MonthName");
            day = intent.getStringExtra("day");
            strCityId = intent.getStringExtra("strCityId");
            strStateId = intent.getStringExtra("strStateId");

            if (!TextUtils.isEmpty(strStartDate)) {
                llFilterLinearLayout.setVisibility(View.VISIBLE);
                filterDate.setVisibility(View.VISIBLE);
                filterDate.setText(monthName+" "+day);

            }

            if (!TextUtils.isEmpty(strDuration)) {
                llFilterLinearLayout.setVisibility(View.VISIBLE);
                filterDuration.setVisibility(View.VISIBLE);
                filterDuration.setText(strDuration + " hours");
            }

//            pageCount=0;
        }
        DataFetching();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


//    @Override
//    public void onRefresh(){
//        swipeRefreshLayout.setRefreshing(true);
//        refreshList();
//    }


    private void  refreshList(){
        if (arrlistExplore.size() < Integer.parseInt(strPageCount)) {
            pageCount=0;
            DataFetching();
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        Log.d("d", "requestCode::  "+requestCode);
        Log.d("d", "resultCode::  "+resultCode);
        pageCount = 0;

        if (resultCode == StaticClass.SearchCityRequestCode) {

            if(data!=null) {
                if (!data.getExtras().getString("CityId").equals("")) {
//                    strCityId = data.getExtras().getString("CityId");
                    strCityName = data.getExtras().getString("CityName");
                    isState = data.getExtras().getString("isState");
                    Log.d("d", "City id::::" + strCityId);
                    Log.d("d", "City name::::" + strCityName);

                    if (!"".equals(strCityName) && !"null".equals(strCityName)) {
                        tvSearchCity.setText(strCityName);
//                        ivCancel.setVisibility(View.VISIBLE);
                    }

                    if (isState.equalsIgnoreCase("N")){
                        strCityId = data.getExtras().getString("CityId");
                        strStateId = "";
                    }else if (isState.equalsIgnoreCase("Y")){
                        strStateId = data.getExtras().getString("CityId");
                        strCityId = "";
                    }

                    DataFetching();
                }
            }
        }else if (resultCode == StaticClass.SearchFilterRequestCode) {
            int batchcount=6;
//            llFilterLinearLayout.setVisibility(View.GONE);
            filterDuration.setVisibility(View.GONE);
//            filterDate.setVisibility(View.GONE);

//            if (StaticClass.isClearClicked){
//                tvBadge.setVisibility(View.GONE);
//            }else {
//                tvBadge.setVisibility(View.VISIBLE);
//            }

            if(data!=null) {

                strMinPrice = data.getExtras().getString("MinValue");
                strMaxPrice = data.getExtras().getString("MaxValue");
                strStartDate = data.getExtras().getString("FromDate");
                strEndDate= data.getExtras().getString("strToDate");
                strDuration = data.getExtras().getString("Duration");
                strPickUpTime = data.getExtras().getString("PickUpTime");
                strVehicleTypeId = data.getExtras().getString("VehicleType");
                strVehicleTypeName = data.getExtras().getString("VehicleTypeName");
                strNoOfPassenger = data.getExtras().getString("PassengerNo");
                strNoOfBaggage = data.getExtras().getString("BaggageAllowance");
                strNeedOvertime = data.getExtras().getString("OvertimeAvailable");
                strVehicleBrandId = data.getExtras().getString("strVehicleBrandId");
                strVehicleBrandName = data.getExtras().getString("strVehicleBrandName");

                monthName = data.getStringExtra("MonthName");
                day = data.getStringExtra("day");

                if (!TextUtils.isEmpty(strStartDate)) {
                    llFilterLinearLayout.setVisibility(View.VISIBLE);
                    filterDate.setVisibility(View.VISIBLE);
//                    filterDate.setText(strStartDate);
                    filterDate.setText(monthName+" "+day);
                    batchcount++;
                }

                if (!TextUtils.isEmpty(strDuration)) {
                    llFilterLinearLayout.setVisibility(View.VISIBLE);
                    filterDuration.setVisibility(View.VISIBLE);
                    filterDuration.setText(strDuration + " hours");
                    batchcount++;
                }

//                tvBadge.setVisibility(View.VISIBLE);
//                tvBadge.setText("");

                DataFetching();
            }

            System.out.println("batchcount***************"+batchcount);
//            tvBadge.setText("");
//            tvBadge.setVisibility(View.VISIBLE);

        }
    }


    private void DataFetching(){
      //  rcvExploreCar.setVisibility(View.GONE);
        if (NetWorkStatus.isNetworkAvailable(ExploreActivity.this)) {
            Log.d("d", "** page count**"+pageCount);
            new ExploreVehicleList_Webservice().exploreVehicleList(ExploreActivity.this, strMinPrice,
                    strMaxPrice, strStartDate, strEndDate, strDuration, strVehicleTypeId, strVehicleBrandId, strNoOfPassenger,
                    strNoOfBaggage, strNeedOvertime, strNeedAc, strCityId, strStateId, strPickUpTime , strModelId, pageCount);

        } else {
            Intent i = new Intent(ExploreActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }
    }

    private void Initialize(){
        context = this;
        sharedPrefUser = new SharedPrefUserDetails(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvExploreHeader));

        rlBackLayout = findViewById(R.id.rlBackLayout);
        rlBackLayout.setVisibility(View.VISIBLE);

        arrlistExplore = new ArrayList<>();

        ivFilter = findViewById(R.id.ivFilter);
        ivCancel = findViewById(R.id.ivCancel);
        tvSearchCity = findViewById(R.id.tvSearchCity);

//        swipeRefreshLayout = findViewById(R.id.refresh);
//        swipeRefreshLayout.setOnRefreshListener(ExploreActivity.this);

        rcvExploreCar = findViewById(R.id.rcvExploreCar);
        rlSearchCity = findViewById(R.id.rlSearchCity);
        tvNoExploreCarFound = findViewById(R.id.tvNoExploreCarFound);

        llFilterLinearLayout = findViewById(R.id.llFilterLinearLayout);
        filterDate = findViewById(R.id.filterDate);
        filterDuration = findViewById(R.id.filterDuration);

        tvBadge = findViewById(R.id.tvBadgeExplore);

//        bottomview.BottomView(ExploreActivity.this, StaticClass.Menu_Explore);

    }

    private void Listener(){


        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(ExploreActivity.this,
                        ExploreActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                        ExploreActivity.this.getResources().getString(R.string.yes),
                        ExploreActivity.this.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();
                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
                transitionflag = StaticClass.transitionflagBack;
            }
        });

        ivFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                transitionflag=StaticClass.transitionflagNext;
                Intent applyintent = new Intent(ExploreActivity.this, FilterActivity.class);
                applyintent.putExtra("MinValue", strMinPrice);
                applyintent.putExtra("MaxValue", strMaxPrice);
                applyintent.putExtra("FromDate", strStartDate );
                applyintent.putExtra("ToDate", strEndDate );
                applyintent.putExtra("Duration", strDuration );
                applyintent.putExtra("PickUpTime", strPickUpTime );
                applyintent.putExtra("VehicleType", strVehicleTypeId);
                applyintent.putExtra("VehicleTypeName", strVehicleTypeName);
                applyintent.putExtra("PassengerNo", strNoOfPassenger);
                applyintent.putExtra("BaggageAllowance", strNoOfBaggage);
                applyintent.putExtra("OvertimeAvailable", strNeedOvertime);
                applyintent.putExtra("strVehicleBrandId",strVehicleBrandId);
                applyintent.putExtra("strVehicleBrandName",strVehicleBrandName);
                applyintent.putExtra("monthName",monthName);
                applyintent.putExtra("day",day);
                startActivityForResult(applyintent, StaticClass.SearchFilterRequestCode);
            }
        });

        rcvExploreCar.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState == SCROLL_STATE_FLING) {

                } else if (newState == SCROLL_STATE_IDLE) {
                    System.out.println("**********onScrolled************");
                    System.out.println("**********strCityId************" + strCityId);
                    System.out.println("**********strPageCount************" + strPageCount);

                    if(strCityId == null || strCityId.equals("")) {
                        if (arrlistExplore.size() < Integer.parseInt(strPageCount)) {
                            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) rcvExploreCar.getLayoutManager();
                            if (linearLayoutManager.findLastCompletelyVisibleItemPosition() >= (arrlistExplore.size() - 1)) {
                                System.out.println("**********findLastVisibleItemPosition************");
                                pageCount++;
                                DataFetching();
                            }
                        }
                    }else {

                        Log.d("arrListExploreModelSize", ""+arr_ExploreVehicleListModel.size());
                        Log.d("strPageCount", strPageCount);

                        if (arr_ExploreVehicleListModel.size() < Integer.parseInt(strPageCount)) {
                            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) rcvExploreCar.getLayoutManager();
                            if (linearLayoutManager.findLastCompletelyVisibleItemPosition() >= (arr_ExploreVehicleListModel.size() - 1)) {
                                System.out.println("**********findLastVisibleItemPosition************");
                                pageCount++;
                                DataFetching();
                            }
                        }
                    }
                }
            }
        });
    }


    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(ExploreActivity.this, transitionflag);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(ExploreActivity.this,
                ExploreActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                ExploreActivity.this.getResources().getString(R.string.yes),
                ExploreActivity.this.getResources().getString(R.string.no));

        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
        transitionflag = StaticClass.transitionflagBack;

    }

    @Override
    public void ExploreVehicleListInterface(String total_count, String strPageLimit, ArrayList<ExploreModel> arr_arrlistExplore) {
        Log.d("d", "ArrlistExplore size::  "+arrlistExplore.size());
        Log.d("d", "strCityId::  "+strCityId);
        if(pageCount==0){
            arrlistExplore = new ArrayList<>();
            arrlistExplore = arr_arrlistExplore;

            arr_ExploreVehicleListModel=new ArrayList<>();
            if (arr_arrlistExplore.size() > 0)
            arr_ExploreVehicleListModel=arrlistExplore.get(pageCount).getArr_exploreVehicleListModel();

        }else {
            for (int i = 0; i < arr_arrlistExplore.size(); i++) {
                arrlistExplore.add(arr_arrlistExplore.get(i));
                arr_ExploreVehicleListModel.addAll(arr_arrlistExplore.get(i).getArr_exploreVehicleListModel());
            }
        }
        strPageCount = total_count;
        intOffset = Integer.parseInt(strPageLimit);

         if (arrlistExplore.size() > 0){

            rcvExploreCar.setVisibility(View.VISIBLE);
            tvNoExploreCarFound.setVisibility(View.GONE);
            rcvExploreCar.removeAllViews();
            if(strCityId == null || strCityId.equals("")) {
                Log.d("d", "hfdfhfsfshgfsh");
                exploreCarAdapter = new ExploreCarAdapter(ExploreActivity.this, arrlistExplore, strStartDate, strDuration, strPickUpTime);
                layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
                rcvExploreCar.setLayoutManager(layoutManagerVertical);
                rcvExploreCar.setItemAnimator(new DefaultItemAnimator());
                rcvExploreCar.setHasFixedSize(true);
                rcvExploreCar.setAdapter(exploreCarAdapter);
                if (pageCount > 0){
                    rcvExploreCar.smoothScrollToPosition(pageCount*intOffset-1);
                }

            }else{
                ArrayList<ExploreVehicleListModel> listOfVehical = new ArrayList<>();
                listOfVehical = arrlistExplore.get(0).getArr_exploreVehicleListModel();
                exploreGrideCarAdapter = new ExploreGrideCarAdapter(ExploreActivity.this, listOfVehical, strStartDate, strDuration, strPickUpTime);
                rcvExploreCar.setLayoutManager(new GridLayoutManager(this, 2));
                rcvExploreCar.setItemAnimator(new DefaultItemAnimator());
                rcvExploreCar.setHasFixedSize(true);
                rcvExploreCar.setAdapter(exploreGrideCarAdapter);

                if (pageCount > 0){
                    rcvExploreCar.smoothScrollToPosition(pageCount*intOffset+1);
                }
            }

        }else{
            rcvExploreCar.setVisibility(View.GONE);
            tvNoExploreCarFound.setVisibility(View.VISIBLE);
        }
    }


    @Override
    public void onExploreViewMore(String CityId, String CityName) {
        rcvExploreCar.setVisibility(View.GONE);
//        strCityId = CityId;
        strCityName = CityName;
        if (!TextUtils.isEmpty(strCityName)) {
            tvSearchCity.setText(strCityName);
//            ivCancel.setVisibility(View.VISIBLE);
        }
//        if (isState.equalsIgnoreCase("N")){
            strCityId = CityId;
//        }else if (isState.equalsIgnoreCase("Y")){
//            strStateId = CityId;
//        }
        pageCount=0;
        DataFetching();
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (StaticClass.isClearClicked){
//            llFilterLinearLayout.setVisibility(View.GONE);
//            filterDate.setVisibility(View.GONE);
//            tvBadge.setVisibility(View.GONE);
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }

        if(StaticClass.BottomExplore){
            StaticClass.BottomExplore = false;
//            Intent mybookingIntent = new Intent(this, FilterOnBookActivity.class);
            Intent mybookingIntent = new Intent(this, SearchActivity.class);
            this.startActivity(mybookingIntent);
            finish();
        }

        sharedPrefUser.putBottomView(StaticClass.Menu_Explore);
        bottomview = new BottomView();
        bottomview.BottomView(ExploreActivity.this, StaticClass.Menu_Explore);
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) ExploreActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

//    @Override
//    public void onExploreReffresh() {
//
//        strMinPrice=Integer.toString(StaticClass.minPrice);
//        strMaxPrice=Integer.toString(StaticClass.maxPrice);
//        strStartDate=""; strEndDate=""; strDuration=""; strVehicleTypeId=""; strVehicleTypeName="";
//        strNoOfPassenger="0"; strNoOfBaggage="0"; strNeedOvertime=""; strNeedAc=""; strCityId=""; strPickUpTime="";
//
//        tvSearchCity.setText(getResources().getString(R.string.tvSeacrhCity));
//        strCityId ="";
//        strCityName="";
//        strStateId="";
////        tvBadge.setVisibility(View.GONE);
//        pageCount=0;
//        DataFetching();
//    }
}
