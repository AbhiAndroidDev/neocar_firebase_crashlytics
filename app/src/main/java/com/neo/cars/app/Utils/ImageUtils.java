package com.neo.cars.app.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Grishma on 14/3/16.
 */
public class ImageUtils {
    private static final float maxHeight = 1280.0f;
    private static final float maxWidth = 1280.0f;

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }
        return inSampleSize;
    }

    /**
     * Reduces the size of an image without affecting its quality.
     *
     * @param imagePath -Path of an image
     * @return
     */
    public static Bitmap compressImage(String imagePath, String filepath) {
        Bitmap scaledBitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(imagePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

        float imgRatio = (float) actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;
            }
        }
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];
        try {
            bmp = BitmapFactory.decodeFile(imagePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.RGB_565);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);
        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        if (bmp != null) {
            bmp.recycle();
        }
//        ExifInterface exif;
//        try {
//            exif = new ExifInterface(imagePath);
//            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
//            Matrix matrix = new Matrix();
//            if (orientation == 6) {
//                matrix.postRotate(90);
//            } else if (orientation == 3) {
//                matrix.postRotate(180);
//            } else if (orientation == 8) {
//                matrix.postRotate(270);
//            }
//            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        FileOutputStream out = null;
//        try {
//            //new File(imageFilePath).delete();
//            out = new FileOutputStream(filepath);
//
//            //write the compressed bitmap at the destination specified by filename.
//            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }

//        return filepath;
        return scaledBitmap;
    }

    public static String getFilename() {
        String state = Environment.getExternalStorageState();
//        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath() + "/.SalaryDistributionSystem/");
        File mediaStorageDir;

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            Log.d("folder ", "External storage");
             mediaStorageDir = new File(Environment.getExternalStorageDirectory() , "SalaryDistributionSystem");
        } else {
            Log.d("folder ", "internal storage");
             mediaStorageDir =  new File(Environment.getDataDirectory(),  "SalaryDistributionSystem");
        }

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if(!mediaStorageDir.mkdirs())
            Log.d("folder", "Folder creation failed!");
        }

        if (!mediaStorageDir.exists()){
            Log.d("folder ", "Folder doesn't esist");
        } else {
            Log.d("folder ", "Folder  esists");
        }
//        mediaStorageDir.getAbsolutePath();
        Log.d("ImagefilePath", mediaStorageDir.getAbsolutePath());

        String mImageName = "IMG_" + String.valueOf(System.currentTimeMillis()) + ".jpg";
        String uriString = mediaStorageDir.getAbsolutePath() + "/" + mImageName;
        return uriString;
    }

    public static File getFile() {
        File image_file = null;
        File folder = Environment.getExternalStoragePublicDirectory("/NeoCar");// the file path
//        File folder = new File(new File(Environment.getDataDirectory(), "/NeoCar"), "/NeoCarMain");
        //if it doesn't exist the folder will be created
        if(!folder.exists())
        {
            folder.mkdir();
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "NeoCar_"+ timeStamp + "_";


        try {
            image_file = File.createTempFile(imageFileName,".jpg",folder);
        } catch (IOException e) {
            e.printStackTrace();
        }

//        mCurrentPhotoPath = image_file.getAbsolutePath();
        return image_file;
    }

    public static String createImageFile() throws IOException {
        // Create an image file name

        //String imageFileName = "JPEG_" + timeStamp + "_";
        String imageFileName = "IMG_" + String.valueOf(System.currentTimeMillis());
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        //File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)+ File.separator
        //+"NeoCars");
        Log.e("storageDir path",""+ storageDir);

        // Create the storage directory if it does not exist
        if (!storageDir.exists()) {
            if(!storageDir.mkdirs())
                Log.e("folder", "Folder creation failed!");
            else{
                Log.e("folder", "Folder created!");
            }
        } else {
            Log.e("folder", "Folder exists!");
        }

        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        Log.e("Image path",""+ image.getAbsolutePath());

        return image.getAbsolutePath();
    }
    public static void copyFile(String selectedImagePath, String mDestinationPath) throws IOException {
        InputStream in = new FileInputStream(selectedImagePath);
        OutputStream out = new FileOutputStream(mDestinationPath);

        // Transfer bytes from in to out
        byte[] buf = new byte[1024];
        int len;
        while ((len = in.read(buf)) > 0) {
            out.write(buf, 0, len);
        }
        in.close();
        out.close();
    }

    //Added later when camera picture is not saving in gallery in samsung device

    //added later to camera picture saving

    public static Bitmap decodeSampledBitmapFromResource(final byte[] data, int reqWidth, int reqHeight)
    {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(data, 0, data.length,options);
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(data, 0, data.length,options);
    }

    public static File saveImage(Bitmap myBitmap, Context context) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        //byte[] byteArray = bytes.toByteArray();
        //Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        File folder = new File(Environment.getExternalStorageDirectory() + "/NeoCar");
        // have the object build the directory structure, if needed.
        if (!folder.exists()) {
            folder.mkdirs();
        }

        try {
            File f = new File(folder, Calendar.getInstance().getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(context,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();

            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());
//            prescription_filename_tv.setText("" + f.getAbsolutePath());
            return f;

        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return null;
    }




    public static File saveImageGallery(Bitmap myBitmap, Context context) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 80, bytes);
        //byte[] byteArray = bytes.toByteArray();
        //Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);

        File folder = new File(Environment.getExternalStorageDirectory() + "/NeoCar/NeoCarGallery");
        // have the object build the directory structure, if needed.
        if (!folder.exists()) {
            folder.mkdirs();
        }

        try {
            File f = new File(folder, Calendar.getInstance().getTimeInMillis() + ".jpg");
//            File f = new File(folder,  "galleryImage.jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(context,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();

            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());
//            prescription_filename_tv.setText("" + f.getAbsolutePath());
            return f;

        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return null;
    }


    public static void deleteImageGallery() {
        try {

            File dir = new File(Environment.getExternalStorageDirectory() + "/NeoCar/NeoCarGallery");
            if (dir.exists()) {
                // have the object build the directory structure, if needed.
                if (dir.isDirectory()) {
                    String[] children = dir.list();
                    for (int i = 0; i < children.length; i++) {
                        new File(dir, children[i]).delete();
                    }
                }

                dir.delete();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }



}
