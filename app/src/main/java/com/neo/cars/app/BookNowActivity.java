package com.neo.cars.app;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TimePicker;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.Interface.BookingHours_Interface;
import com.neo.cars.app.Interface.CallBackCustomTimePicker;
import com.neo.cars.app.Interface.CurrentLatLong_Interface;
import com.neo.cars.app.Interface.HirePurpose_Interface;
import com.neo.cars.app.Interface.VehicleBookingAvailability_Interface;
import com.neo.cars.app.LocationTracker.CurrentLatLong;
import com.neo.cars.app.SetGet.BookingHoursModel;
import com.neo.cars.app.SetGet.HirePurposeModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.MessageText;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.NoDefaultSpinner;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.AddStopOverAdapterBooking;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.BeforeProcessBooking_Webservice;
import com.neo.cars.app.Webservice.BookingHours_Webservice;
import com.neo.cars.app.Webservice.HirePurpose_webservice;
import com.neo.cars.app.Webservice.VehicleBookingAvailability_Webservice;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.dialog.CustomTimePickerDialog1;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomButtonTitilliumWebRegular;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by parna on 12/3/18.
 */

public class BookNowActivity extends RootActivity implements BookingHours_Interface, VehicleBookingAvailability_Interface,
        HirePurpose_Interface, CallBackCustomTimePicker, CurrentLatLong_Interface {

    private static final int REQUEST_CODE_PICK_CONTACTS = 105;
    private static final int PLACE_AUTOCOMPLETE_REQUEST_CODE = 189;
    private static final String TAG = BookNowActivity.class.getSimpleName();
    private ScrollView scv_booknow;
    private RelativeLayout rlParentRelativeLayout, rlBackLayout, rlSpnrPickUpLoc, rlSpnrFilterDuration, rlSpnrPuposeOfHire;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvPickUpLoc, tvDropOffPoint, tvNoOfPassengersNo,tvBookDate,tvBookStartTime,tvBookDuration;
    //private CustomTextviewTitilliumWebRegular tvLocationName;
    private CustomTextviewTitilliumWebRegular tvOtherPickUp, tvOtherDrop;
    private CustomEditTextTitilliumWebRegular tvName, tvMobNo, tvTravelItinerary, tvAdditionalRequests, tvAdditionalInfo;
   // private NoDefaultSpinner  spnrPurposeOfHire, spnrPickUploc, spnrDropOff;
   private NoDefaultSpinner  spnrPurposeOfHire;
   // private NoDefaultSpinner spnrFilterDuration;
    private CustomButtonTitilliumSemibold btnBookNow;
    private String strAvailableFor="", strBookingHourLabel="", strBookingHourValue="", strDate="", /*strPickUptime="",*/
            strVehicleId="", strPickUpLocation="", strPickUpLocationId="", strDropLocation="",
            strDropLocationId="", strHirePurpose="", strHirePurposeId="", strUserId="", strDriverId="", strAdditionalRequest="",
            strAdditionalInfo="", strExpectedRoute="", strName="", strMobNo="", strFinalName="", strFinalMobNo="",strLocation="",
            format = "";

    private String strStartDate = "", strDuration = "", strPickUptime = "";

    private CustomTextviewTitilliumWebRegular tvDate;
   // private CustomTextviewTitilliumWebRegular tvStartTime;
    private AddStopOverAdapterBooking addStopOverAdapterBooking;
    private Context context;
    private Toolbar toolbar;
    private ImageButton ibNavigateMenu;
    private Calendar mcalendar;
    private int day,month,year;
    private ConnectionDetector cd;
    private ArrayList<String> listOfBookingHour, listOfPickUpLocation, listOfHirePurpose;
    private ArrayList<BookingHoursModel> arrlistBookingHours;
    private ArrayAdapter<String> arrayAdapterBookingHour, arrayAdapterPickuploc, arrayAdapterDropOffLoc, arrayAdapterHirePurpose;
    private Bundle bundle;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private SharedPrefUserDetails sharedPref;
    private String struserdetails;
    private CustomButtonTitilliumWebRegular btnAddMMore;
    private LinearLayout lladdmore_name_mobno;

    private int transitionflag = StaticClass.transitionflagNext;
    private BottomView bottomview =new BottomView();

    private String strMaxAddlPassengerCounter="0",strAddressFromLatLong="";
    private ArrayList<String> Al_NameBooking=new ArrayList<String>();
    private ArrayList<String> Al_MobileNoBooking=new ArrayList<String>();
    private CustomTimePickerDialog1 customTimePickerDialog;

    private Uri uriContact;
    private String contactID;

    boolean selectOtherPick = false, selectOtherDrop = false;
    private  String currentDate = "", currentHour = "", time24="";
    private ImageView ivInfoTravelItinerary;

    private int selectedHour, addlPassengerCounter = 0;
    private boolean isLocationEnableCancelled = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_now);


        bundle = getIntent().getExtras();
        if(bundle != null){
            strVehicleId = bundle.getString("vehicleId");
            strDriverId = bundle.getString("driverId");
            Log.e("driver_id", strDriverId);
            strLocation = bundle.getString("CityName");
            strStartDate = bundle.getString("FromDate"); //format - 2018-12-29
//            Log.d("strStartDate", strStartDate);
            strDate = strStartDate;
            strDuration = bundle.getString("Duration");
            strBookingHourValue = strDuration;
            strPickUptime = bundle.getString("PickUpTime");

//            Log.d("d", "***BookNowActivity get vehicle id***"+strVehicleId);
//            Log.d("d", "***BookNowActivity get driver id***"+strDriverId);
//            Log.d("d", "***BookNowActivity get location city****"+strLocation);
        }

        StaticClass.setAl_NameBooking(Al_NameBooking);
        StaticClass.setAl_MobileNoBooking(Al_MobileNoBooking);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        sharedPref = new SharedPrefUserDetails(BookNowActivity.this);

        struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        new AnalyticsClass(BookNowActivity.this);

        Initialize();
        Listener();

//        new CurrentLatLong().currentlatlong(BookNowActivity.this);

        //Booking hours duration webservice
        if(cd.isConnectingToInternet()){
            new BookingHours_Webservice().bookinghoursWebservice(BookNowActivity.this, strAvailableFor );

        }else {
            Intent i = new Intent(BookNowActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }

        //pick and drop location webservice
        if(cd.isConnectingToInternet()){
            new VehicleBookingAvailability_Webservice().vehicleBookingAvailability(BookNowActivity.this, strVehicleId);

        }else{
            Intent i = new Intent(BookNowActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }


        //purpose of hire webservice
        if(cd.isConnectingToInternet()){
            new HirePurpose_webservice().hirePurposeListWebservice(BookNowActivity.this);

        }else{
            Intent i = new Intent(BookNowActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }

        currentDate = getCurrentDate();
        Log.d("currentDate", currentDate);
        currentHour = getCurrentHour();

        Log.d("currentTime**", currentHour);

    }

    public void convert12(String strTime)
    {
        String timeFinal = "";
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
            final Date dateObj = sdf.parse(strTime);
            timeFinal = new SimpleDateFormat("hh:mm a").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }

        tvBookStartTime.setText(timeFinal);
        //tvStartTime.setText(timeFinal);

    }

    private String getCurrentDate() {

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);
        Log.d("formattedDate", formattedDate); //format - 2018-12-28

        return formattedDate;
    }

    private String getCurrentHour() {
        DateFormat dateFormat = new SimpleDateFormat("HH");
        Date date = new Date();
        return dateFormat.format(date);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Initialize(){

        context = this;
        cd = new ConnectionDetector(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title =  findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvBookNowHeaderText));

        // Booking date field mandatory
       // tvDate =  findViewById(R.id.tvDate);
      //  tvStartTime =  findViewById(R.id.tvStartTime);

        scv_booknow = findViewById(R.id.scv_booknow);
        rlParentRelativeLayout = findViewById(R.id.rlParentRelativeLayout);

        tvAdditionalRequests = findViewById(R.id.tvAdditionalRequests);
        tvAdditionalInfo = findViewById(R.id.tvAdditionalInfo);
        tvName = findViewById(R.id.tvName);
        tvMobNo = findViewById(R.id.tvMobNo);
        tvTravelItinerary = findViewById(R.id.tvTravelItinerary);
        rlSpnrFilterDuration = findViewById(R.id.rlSpnrFilterDuration);
        rlSpnrPuposeOfHire = findViewById(R.id.rlSpnrPuposeOfHire);
        //spnrFilterDuration = findViewById(R.id.spnrFilterDuration);
        spnrPurposeOfHire = findViewById(R.id.spnrPurposeOfHire);
        btnBookNow = findViewById(R.id.btnBookNow);
        tvNoOfPassengersNo =  findViewById(R.id.tvNoOfPassengersNo);
        rlSpnrPickUpLoc = findViewById(R.id.rlSpnrPickUpLoc);
       // spnrPickUploc = findViewById(R.id.spnrPickUploc);
       // spnrDropOff = findViewById(R.id.spnrDropOff);
        btnAddMMore = findViewById(R.id.btnAddMMore);
        lladdmore_name_mobno = findViewById(R.id.lladdmore_name_mobno);

        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        rlBackLayout = findViewById(R.id.rlBackLayout);

        tvOtherPickUp =  findViewById(R.id.tvOtherPickUp);
        tvOtherDrop =  findViewById(R.id.tvOtherDrop);
        tvBookDate =  findViewById(R.id.tvBookDate);
        tvBookStartTime =  findViewById(R.id.tvBookStartTime);
        tvBookDuration=  findViewById(R.id.tvBookDuration);

       // tvLocationName =  findViewById(R.id.tvLocationName);

        ivInfoTravelItinerary = findViewById(R.id.ivInfoTravelItinerary);
       /* if(!"".equals(strLocation) && !"null".equals(strLocation)){
            tvLocationName.setText(strLocation);
        }else{
            tvLocationName.setText("N/A");
        }*/



        if (!TextUtils.isEmpty(strStartDate)){
           // tvBookDate.setText(strStartDate);
            tvBookDate.setText(getDateFormat(strStartDate));
        }

        if (!TextUtils.isEmpty(strBookingHourValue)){
            tvBookDuration.setText(strBookingHourValue + " HOURS");
        }




       /* if (!TextUtils.isEmpty(strStartDate)){
            tvDate.setText(strStartDate);
        }*/

        if (!TextUtils.isEmpty(strPickUptime)){

            convert12(strPickUptime);
        }

        mcalendar = Calendar.getInstance();

        day=mcalendar.get(Calendar.DAY_OF_MONTH);
        year=mcalendar.get(Calendar.YEAR);
        month=mcalendar.get(Calendar.MONTH);

        bottomview.BottomView(BookNowActivity.this,StaticClass.Menu_Explore);
        StaticClass.isBookNow = true;





    }

    private String getDateFormat(String strStartDate) {
        String dateTOSend = "";
        try {

            final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            final Date dateObj = sdf.parse(strStartDate);
            dateTOSend = new SimpleDateFormat("dd-MM-yyyy").format(dateObj);
        } catch (final ParseException e) {
            e.printStackTrace();
        }

       return dateTOSend;

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(StaticClass.IsPaymentDone){
            finish();
        }

        //StaticClass.BottomExplore = false;
        if(StaticClass.BottomExplore){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }

        if(!isLocationEnableCancelled) {

            if (!isLocationEnable()) {

                locationAlert();

            } else {

                new CurrentLatLong().currentlatlong(BookNowActivity.this);

            }
        }

    }

    private void locationAlert(){
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage("Please enable your device location.");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(myIntent);
                //get gps
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub

                isLocationEnableCancelled = true;

            }
        });
        dialog.show();
    }

    private void Listener(){

        ivInfoTravelItinerary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new CustomToast(BookNowActivity.this, getResources().getString(R.string.msg_expeted_travel));
            }
        });

        btnBookNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean cancel = false, isMobileValid = true;
                View focusView = null;

                strAdditionalRequest = tvAdditionalRequests.getText().toString();
                strAdditionalInfo = tvAdditionalInfo.getText().toString();
                strExpectedRoute = tvTravelItinerary.getText().toString();
                strName = tvName.getText().toString();
                strMobNo = tvMobNo.getText().toString();

                /********* add more data pass *******/
                ArrayList<String> arrNamebooking = new ArrayList<String>();
                ArrayList<String> arrMobileNoBooking = new ArrayList<String>();

                if (StaticClass.getAl_NameBooking() != null) {
                    arrNamebooking = StaticClass.getAl_NameBooking();
                }

                if (StaticClass.getAl_MobileNoBooking() != null) {
                    arrMobileNoBooking = StaticClass.getAl_MobileNoBooking();
                }

                JSONArray jsonArrayNameMobNo = new JSONArray();

                try {

                    for (int i=0;i< arrNamebooking.size();i++){

                        JSONObject Jobject1=new JSONObject();

                            if(!arrNamebooking.get(i).trim().equalsIgnoreCase("")){
                                Jobject1.put("name", arrNamebooking.get(i).trim());
                            }else{
                                Jobject1.put("name", "");
                            }

                            if(!arrMobileNoBooking.get(i).trim().equalsIgnoreCase("")){
                                Jobject1.put("contact_no", arrMobileNoBooking.get(i).trim());
                            }else{
                                Jobject1.put("contact_no", "");
                            }

                            jsonArrayNameMobNo.put(Jobject1);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                System.out.println("jsonArrayNameMobNo: "+jsonArrayNameMobNo);

                for(int i = 0; i < arrMobileNoBooking.size(); i++){

                    if(!arrMobileNoBooking.get(i).trim().equalsIgnoreCase("")) {
                        String mobileNo = arrMobileNoBooking.get(i).trim();

                        if (!new Emailvalidation().phone_validation(mobileNo)) {
                            new CustomToast(BookNowActivity.this, "Please enter valid phone number");
                            isMobileValid = false;
                        } else {

                        }
                    }
                }

//                if (TextUtils.isEmpty(strExpectedRoute)){
//                    new CustomToast(BookNowActivity.this, getResources().getString(R.string.enter_expeted_travel));
//                }else {

                /***** call webservice for booking process *****/
                if (cd.isConnectingToInternet()) {
                    if(isMobileValid) {

                        if (StaticClass.isBookNow) {
                            StaticClass.isBookNow=false;
                            new BeforeProcessBooking_Webservice().beforeProcessBooking(BookNowActivity.this, strVehicleId, strDriverId, strDate,
                                    strPickUptime, strPickUpLocation, strDropLocation, strBookingHourValue, strHirePurpose, strHirePurposeId,
                                    strPickUpLocationId, strDropLocationId, jsonArrayNameMobNo.toString(), strAdditionalRequest,
                                    strAdditionalInfo, strExpectedRoute, strLocation, String.valueOf(addlPassengerCounter));
                        }
                    }

                } else {
                    Intent i = new Intent(BookNowActivity.this, NetworkNotAvailable.class);
                    startActivity(i);
                }
//                }
            }
        });

/*        tvDate.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                DoBDialog();
            }
        });*/


     /*   tvStartTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("d", "***tvStartTime***");
            }
        });*/

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(BookNowActivity.this,
                        BookNowActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                        BookNowActivity.this.getResources().getString(R.string.yes),
                        BookNowActivity.this.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();
                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();

            }
        });

        btnAddMMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if(tvName.getText().toString().trim().equals("") && tvMobNo.getText().toString().trim().equals("")){
                    new CustomToast(BookNowActivity.this, MessageText.Enter_name_mobno);
                }else{*/
                AddStopOverMethod();


                // }
            }
        });
        final ConnectionDetector cd = new ConnectionDetector(BookNowActivity.this);

        tvOtherPickUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectOtherPick = true;

                if(NetWorkStatus.isNetworkAvailable(BookNowActivity.this)) {

                    // Initialize Places.
                    Places.initialize(getApplicationContext(), StaticClass.API_KEY);

                    PlacesClient placesClient = Places.createClient(BookNowActivity.this);

                    try {

                        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS);


                        // Start the autocomplete intent.
                        Intent intent = new Autocomplete.IntentBuilder(
                                AutocompleteActivityMode.FULLSCREEN, fields)
                                .setCountry("IN")
                                //                                .setLocationBias(bounds)
                                //                                .setTypeFilter(TypeFilter.ADDRESS)
                                .build(BookNowActivity.this);

                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }else{
                    startActivity(new Intent(BookNowActivity.this, NetworkNotAvailable.class));
                }

            }
        });

        tvOtherDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectOtherDrop = true;

                if(NetWorkStatus.isNetworkAvailable(BookNowActivity.this)) {

                    // Initialize Places.
                    Places.initialize(getApplicationContext(), StaticClass.API_KEY);

                    PlacesClient placesClient = Places.createClient(BookNowActivity.this);

                    try {

                        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS);

                        // Start the autocomplete intent.
                        Intent intent = new Autocomplete.IntentBuilder(
                                AutocompleteActivityMode.FULLSCREEN, fields)
                                .setCountry("IN")
                                .build(BookNowActivity.this);
                        startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }else{
                    startActivity(new Intent(BookNowActivity.this, NetworkNotAvailable.class));
                }

            }
        });

    }

    private void AddStopOverMethod(){

        try {
            if((StaticClass.getAl_NameBooking().size()<Integer.parseInt(strMaxAddlPassengerCounter))){

                addlPassengerCounter++;

                if(Al_NameBooking.size()>0 || Al_MobileNoBooking.size() > 0){
                    boolean isVerified=true;
                    for (int i=0;i<Al_NameBooking.size();i++){
                        if(Al_NameBooking.get(i).equals("") && Al_MobileNoBooking.get(i).equals("")){

//                            isVerified=false;
                        }else if(!Al_MobileNoBooking.get(i).equals("")){
                            if(!new Emailvalidation().phone_validation(Al_MobileNoBooking.get(i).trim()) ){
                                isVerified=false;
                            }
                        }
                    }
                    if(isVerified){
                        Al_MobileNoBooking.add("");
                        Al_NameBooking.add("");
                        StaticClass.setAl_MobileNoBooking(Al_MobileNoBooking);
                        StaticClass.setAl_NameBooking(Al_NameBooking);

                        addStopOverAdapterBooking =  new AddStopOverAdapterBooking(BookNowActivity.this, lladdmore_name_mobno);
                    }else {

                        new CustomToast(BookNowActivity.this, MessageText.Enter_name_mobno);
                    }
                }else {
                    Al_MobileNoBooking.add("");
                    Al_NameBooking.add("");
                    StaticClass.setAl_MobileNoBooking(Al_MobileNoBooking);
                    StaticClass.setAl_NameBooking(Al_NameBooking);

                    addStopOverAdapterBooking =  new AddStopOverAdapterBooking(BookNowActivity.this, lladdmore_name_mobno);
                }


            }else {
                new CustomToast(BookNowActivity.this, getResources().getString(R.string.tvMaxAdditionalPassenger));
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();

        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
    }

    public void DoBDialog(){
        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("d", "monthOfYear:" +monthOfYear);
                try {
                    String dateSelected = "";
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date FromDate = sdf.parse(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                    strDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;
                    if (dayOfMonth < 10 && monthOfYear < 9) {
                        dateSelected = "0"+dayOfMonth + "/0" + (monthOfYear + 1) + "/" + year;
                    }else if (dayOfMonth < 10){
                        dateSelected = "0"+dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                    }else if (monthOfYear < 9){
                        dateSelected = dayOfMonth + "/0" + (monthOfYear + 1) + "/" + year;
                    }else {
                        dateSelected = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;
                    }

                    tvDate.setText(dateSelected);

                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    String formattedDate = df.format(FromDate);
                    strStartDate = formattedDate;

                    Log.d("d", "Date: "+strDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        };

        DatePickerDialog dpDialog = new DatePickerDialog(BookNowActivity.this, listener, year, month, day);
        //dpDialog.getDatePicker().setMaxDate(mcalendar.getTimeInMillis());
        dpDialog.getDatePicker().setMinDate(mcalendar.getTimeInMillis());
        dpDialog.show();

    }


    @Override
    public void onBackPressed() {
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(BookNowActivity.this,
                BookNowActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                BookNowActivity.this.getResources().getString(R.string.yes),
                BookNowActivity.this.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }

    @Override
    public void BookingHoursList(final ArrayList<BookingHoursModel> arrlistBookingHour) {

        int pos = -1;

        arrlistBookingHours = new ArrayList<>();
        arrlistBookingHours = arrlistBookingHour;

        if (arrlistBookingHours != null){
            listOfBookingHour = new ArrayList<String>();

            if(arrlistBookingHours.size() > 0){

                for (int x=0; x<arrlistBookingHours.size(); x++){
                    listOfBookingHour.add(arrlistBookingHours.get(x).getLabel());
                    if(arrlistBookingHours.get(x).getValue().equals(strBookingHourValue)){
                        pos=x;
                    }
                }

                Log.d("d", "Array list vehicle booking hour size::"+arrlistBookingHours.size());

                String strStartTimeFirst = getResources().getString(R.string.tvDurationDropDownBox);
                String strStartTimeSecond = "<font color='#FF0202'> "+getResources().getString(R.string.tvhintPurposeOfHireStar)+"</font>";

                arrayAdapterBookingHour = new ArrayAdapter<String>(context, R.layout.countryitem, listOfBookingHour);
                arrayAdapterBookingHour.setDropDownViewResource(R.layout.simpledropdownitem);
            /*    spnrFilterDuration.setPrompt(Html.fromHtml(strStartTimeFirst + strStartTimeSecond));
                spnrFilterDuration.setAdapter(arrayAdapterBookingHour);

                spnrFilterDuration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                        strBookingHourLabel = arrlistBookingHours.get(position).getLabel();
                        strBookingHourValue = arrlistBookingHours.get(position).getValue();

                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {
                    }
                });

                if(pos!=-1){
                    spnrFilterDuration.setSelection(pos);
                }*/
            }
        }else{
            new CustomToast(this, getResources().getString(R.string.tvDurationNtFound));
        }
    }

    @Override
    public void vehiclebookingavailabilityinfo(final VehicleTypeModel vehicleTypeModel) {
        if(!vehicleTypeModel.getMax_addl_passenger().equals("") && !vehicleTypeModel.getMax_addl_passenger().equals("null")){
            tvNoOfPassengersNo.setText(" (Maximum "+vehicleTypeModel.getMax_addl_passenger()+")");
            strMaxAddlPassengerCounter = vehicleTypeModel.getMax_addl_passenger();
        }

        if(vehicleTypeModel.getArr_PickUpLocationModel().size() > 0){
            listOfPickUpLocation = new ArrayList<String>();
            for (int i=0; i<vehicleTypeModel.getArr_PickUpLocationModel().size(); i++){
                listOfPickUpLocation.add(vehicleTypeModel.getArr_PickUpLocationModel().get(i).getPickup_location());
            }
            Log.d("d", "Array list vehicle booking pick up location size::"+vehicleTypeModel.getArr_PickUpLocationModel().size());

            String strPickFirst = getResources().getString(R.string.tvhintPickUpLoc);
            String strPickSecond = "<font color='#FF0202'> "+getResources().getString(R.string.tvhintPurposeOfHireStar)+"</font>";
            arrayAdapterPickuploc = new ArrayAdapter<String>(context, R.layout.countryitem, listOfPickUpLocation);
            arrayAdapterPickuploc.setDropDownViewResource(R.layout.simpledropdownitem);
            //spnrPickUploc.setPrompt(Html.fromHtml(strPickFirst + strPickSecond));
           // spnrPickUploc.setAdapter(arrayAdapterPickuploc);

            String strDropFirst = getResources().getString(R.string.tvhintDropOffLoc);
            String strDropSecond = "<font color='#FF0202'> "+getResources().getString(R.string.tvhintPurposeOfHireStar)+"</font>";
            arrayAdapterDropOffLoc = new ArrayAdapter<String>(context, R.layout.countryitem, listOfPickUpLocation);
            arrayAdapterDropOffLoc.setDropDownViewResource(R.layout.simpledropdownitem);
            //spnrDropOff.setPrompt(Html.fromHtml(strDropFirst + strDropSecond));
           // spnrDropOff.setAdapter(arrayAdapterDropOffLoc);

           /* spnrPickUploc.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {

                    if("(custom location)".equalsIgnoreCase(vehicleTypeModel.getArr_PickUpLocationModel().get(position).getPickup_location())){
                        tvOtherPickUp.setVisibility(View.VISIBLE);
                        strPickUpLocation = tvOtherPickUp.getText().toString();

                        if(NetWorkStatus.isNetworkAvailable(BookNowActivity.this)) {

                            Dexter.withActivity(BookNowActivity.this).withPermissions(Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)
                                    .withListener(new MultiplePermissionsListener() {

                                        @Override
                                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                                            if (report.areAllPermissionsGranted()) {

                                                new CurrentLatLong().currentlatlong(BookNowActivity.this);

                                                tvOtherPickUp.setVisibility(View.VISIBLE);
                                                if (strAddressFromLatLong != null && !strAddressFromLatLong.equals("")) {
                                                    tvOtherPickUp.setText(strAddressFromLatLong);
                                                    strPickUpLocation = strAddressFromLatLong;
                                                }
                                                strPickUpLocationId = "";
                                                tvOtherPickUp.addTextChangedListener(new TextWatcher() {
                                                    @Override
                                                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                                    }

                                                    @Override
                                                    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                                                        strPickUpLocation = s.toString();
                                                    }

                                                    @Override
                                                    public void afterTextChanged(Editable s) {

                                                    }
                                                });
                                                Log.d("d", "****other strPickUpLocation****" + strPickUpLocation);

                                            }

                                            // check for permanent denial of any permission
                                            if (report.isAnyPermissionPermanentlyDenied()) {
                                                // permission is denied permenantly, navigate user to app settings if not cancelled


//                                                showAlert();
                                                showAlertLocation();
                                                StaticClass.isLocationPermissionCanceled = true;
                                            }
                                        }

                                        @Override
                                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {*//* ... *//*

                                            token.continuePermissionRequest();
                                        }
                                    }).onSameThread().check();


                        }else{
                            startActivity(new Intent(BookNowActivity.this, NetworkNotAvailable.class));
                        }


                    }else{
                        tvOtherPickUp.setVisibility(View.GONE);
                        strPickUpLocation = vehicleTypeModel.getArr_PickUpLocationModel().get(position).getPickup_location();
                        strPickUpLocationId = vehicleTypeModel.getArr_PickUpLocationModel().get(position).getId();
                        Log.d("d", "****strPickUpLocationId****"+strPickUpLocationId);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });*/

            /*spnrDropOff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    if("(custom location)".equalsIgnoreCase(vehicleTypeModel.getArr_PickUpLocationModel().get(position).getPickup_location())){

                        tvOtherDrop.setVisibility(View.VISIBLE);
                        strDropLocation = tvOtherDrop.getText().toString();

                        if(NetWorkStatus.isNetworkAvailable(BookNowActivity.this)) {

                            Dexter.withActivity(BookNowActivity.this).withPermissions(Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION)
                                    .withListener(new MultiplePermissionsListener() {

                                        @Override
                                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                                            if (report.areAllPermissionsGranted()) {

                                                new CurrentLatLong().currentlatlong(BookNowActivity.this);

                                                tvOtherDrop.setVisibility(View.VISIBLE);
                                                if (strAddressFromLatLong != null && !strAddressFromLatLong.equals("")) {
                                                    tvOtherDrop.setText(strAddressFromLatLong);
                                                    strDropLocation = strAddressFromLatLong;
                                                }
                                                strDropLocationId="";
                                                tvOtherDrop.addTextChangedListener(new TextWatcher() {
                                                    @Override
                                                    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                                                    }

                                                    @Override
                                                    public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                                                        strDropLocation = s.toString();
                                                    }

                                                    @Override
                                                    public void afterTextChanged(Editable s) {

                                                    }
                                                });
                                                Log.d("d", "****other strDropLocation****"+strDropLocation);
                                            }

                                            // check for permanent denial of any permission
                                            if (report.isAnyPermissionPermanentlyDenied()) {
                                                // permission is denied permenantly, navigate user to app settings
//                                                showAlert();
                                                showAlertLocation();
                                                StaticClass.isLocationPermissionCanceled = true;
                                            }
                                        }

                                        @Override
                                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) { ...

                                            token.continuePermissionRequest();
                                        }
                                    }).onSameThread().check();
                        }else{
                            startActivity(new Intent(BookNowActivity.this, NetworkNotAvailable.class));
                        }


                    }else{
                        tvOtherDrop.setVisibility(View.GONE);
                        strDropLocation = vehicleTypeModel.getArr_PickUpLocationModel().get(position).getPickup_location();
                        strDropLocationId = vehicleTypeModel.getArr_PickUpLocationModel().get(position).getId();
                        Log.d("d", "***strDropLocationId***"+strDropLocationId);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });*/
        }else{
            new CustomToast(this, getResources().getString(R.string.tvLocNotFound));
        }
    }



    public void showAlertLocation(){

        if(!StaticClass.isLocationPermissionCanceled) {


            final android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Permissions Required")
                    .setMessage("You have forcefully denied some of the required permissions " +
                            "for this action. Please open settings, go to permissions and allow them.")
                    .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", getPackageName(), null));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    })
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    @Override
    public void HirePurposeList(final ArrayList<HirePurposeModel> arrlistHirePurpose) {
        if(arrlistHirePurpose != null){
            listOfHirePurpose = new ArrayList<String>();
            if (arrlistHirePurpose.size() > 0){
                for (int x=0; x<arrlistHirePurpose.size(); x++){
                    listOfHirePurpose.add(arrlistHirePurpose.get(x).getPurpose());
                }
                Log.d("d", "Array list hire purpose size::"+arrlistHirePurpose.size());

                String first = getResources().getString(R.string.tvhintPurposeOfHire);
                String next = "<font color='#FF0202'> "+getResources().getString(R.string.tvhintPurposeOfHireStar)+"</font>";

                arrayAdapterHirePurpose = new ArrayAdapter<String>(context, R.layout.countryitem, listOfHirePurpose);
                arrayAdapterHirePurpose.setDropDownViewResource(R.layout.simpledropdownitem);
                spnrPurposeOfHire.setPrompt(Html.fromHtml(first + next));


                spnrPurposeOfHire.setAdapter(arrayAdapterHirePurpose);
                spnrPurposeOfHire.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                        strHirePurpose = arrlistHirePurpose.get(position).getPurpose();
                        strHirePurposeId = arrlistHirePurpose.get(position).getId();
                        Log.d("d", "***strHirePurposeId***"+strHirePurposeId);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
            }
        }else{
            new CustomToast(this, getResources().getString(R.string.tvHireNotFound));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(BookNowActivity.this, transitionflag);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) BookNowActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    @Override
    public void onButtonClick(TimePicker mTimePicker, int hour, int minute) {
        System.out.println("*****mTimePicker*****"+mTimePicker);
        System.out.println("*****hour*****"+hour);
        System.out.println("*****minute*****"+minute);

        selectedHour = hour;

        Log.d("d", "***strPickUptime***"+strPickUptime);

        //here minute have 4 value : 0 for 00, 1 for 15, 2 for 30, 3 for 45

        if(minute == 0){
            minute = 0;
        }else if (minute == 1){
            minute = 15;
        }else if (minute == 2){
            minute = 30;
        }else if (minute == 3){
            minute = 45;
        }

        //strPickUptime to send in webservice

        time24="";
        if (hour < 10 && minute == 0){
            strPickUptime = "0"+hour+":0"+minute;
        }else if (hour < 10){
            strPickUptime = "0"+hour+":"+minute;
        }else if (minute == 0){
            strPickUptime = hour+":0"+minute;
        }else{
            strPickUptime = hour+":"+minute;
        }

        if (hour == 0) {
            hour += 12;
            format = "AM";

        } else if (hour == 12) {
            format = "PM";

        } else if (hour > 12) {
            hour -= 12;
            format = "PM";

        } else {
            format = "AM";
        }


        if (minute == 0){
            time24 = hour+":0"+minute+" "+format;
            if (hour < 10){
                time24 = "0"+hour+":0"+minute+" "+format;
            }
        }else if (hour < 10){
            time24 = "0"+hour+":"+minute+" "+format;
        }else {
            time24 = hour+":"+minute+" "+format;
        }


        if (currentDate.equalsIgnoreCase(strStartDate)) {
//            Toast.makeText(context, "Date is equal", Toast.LENGTH_SHORT).show();
            Log.d("currentDateEqualSDate", "Yes");

            int currentHourPlusOne;
            if (Integer.parseInt(currentHour) < 24) {
                currentHourPlusOne = Integer.parseInt(currentHour) + 3;
            } else {
                currentHourPlusOne = 1;
            }
            Log.d("currentHourPlusOne", "" + currentHourPlusOne);

            if (selectedHour < currentHourPlusOne) {

                final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this);
                builder.setMessage("Please select two hour forward time from current time")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                strPickUptime = "";
                                //tvStartTime.setText("");

                            }
                        });
                android.app.AlertDialog alert = builder.create();
                alert.setTitle(getResources().getString(R.string.app_name));
                alert.show();


            } else {
               // tvStartTime.setText(time24);
               // tvStartTime.setError(null);
            }

        } else {
           // tvStartTime.setText(time24);
           // tvStartTime.setError(null);
        }

        customTimePickerDialog.dismiss();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK) {
            System.out.println("requestCode: "+requestCode);
            System.out.println("data: "+data);
            switch (requestCode) {
                case REQUEST_CODE_PICK_CONTACTS:
                    Log.d(TAG, "Response: " + data.toString());
                    uriContact = data.getData();

                    getContactNumber(data, context);
                    break;

                case PLACE_AUTOCOMPLETE_REQUEST_CODE:

                    if (resultCode == RESULT_OK) {
                        Place place = Autocomplete.getPlaceFromIntent(data);
                        Log.i(TAG, "Place: " + place.getName() + ", " + place.getAddress());
                        if (selectOtherPick) {
                            tvOtherPickUp.setText(place.getAddress());
                            selectOtherPick = false;
                            strPickUpLocation = place.getAddress();
                        }else if (selectOtherDrop){
                            tvOtherDrop.setText(place.getAddress());
                            selectOtherDrop = false;
                            strDropLocation = place.getAddress();
                        }


                    } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                        // TODO: Handle the error.
                        Status status = Autocomplete.getStatusFromIntent(data);
                        Log.i(TAG, status.getStatusMessage());
                    } else if (resultCode == RESULT_CANCELED) {
                        // The user canceled the operation.
                    }

                    break;

                default:
                    break;
            }
        }
    }


    public void getContactNumber(Intent data, final Context context) {
        Cursor cursorPhone = null;
        String contactNumber = null;

        try {
            Uri result = data.getData();
//            Log.e(TAG, result.toString());
            String id = result.getLastPathSegment();
            cursorPhone = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);

            if (cursorPhone.moveToFirst()) {

                assert cursorPhone != null;
                contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            } else {
                // no results actions
            }

            Log.d(TAG, "Contact Phone Number: " + contactNumber);
            contactNumber = contactNumber.replaceAll("[^0-9\\+]", "");
            if (contactNumber != null && (new Emailvalidation()).phone_validation(contactNumber)){

                Al_MobileNoBooking.set(StaticClass.book_now_contact_pos,contactNumber);
                StaticClass.setAl_MobileNoBooking(Al_MobileNoBooking);
                StaticClass.setAl_NameBooking(Al_NameBooking);

            }else {
                new CustomToast(BookNowActivity.this, getResources().getString(R.string.txt_message_mobile_no));

                Al_MobileNoBooking.set(StaticClass.book_now_contact_pos,"");
                Al_NameBooking.set(StaticClass.book_now_contact_pos,"");
                StaticClass.setAl_MobileNoBooking(Al_MobileNoBooking);
                StaticClass.setAl_NameBooking(Al_NameBooking);
            }

            addStopOverAdapterBooking =  new AddStopOverAdapterBooking(BookNowActivity.this, lladdmore_name_mobno);

        } catch (Exception e) {
            // error actions
        } finally {
            if (cursorPhone != null) {
                cursorPhone.close();
            }
        }
    }

    @Override
    public void onLatLong(Double Latitude, Double Longitude) {
        System.out.println("Latitude****"+Latitude);
        System.out.println("Longitude****"+Longitude);
        strAddressFromLatLong = getCompleteAddressString(Latitude, Longitude);

    }
}
