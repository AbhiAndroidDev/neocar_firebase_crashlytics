package com.neo.cars.app.font;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by parna on 6/3/18.
 */


public class CustomTitilliumTextViewSemiBold extends AppCompatTextView{

    public CustomTitilliumTextViewSemiBold(Context context) {
        super(context);
    }

    public CustomTitilliumTextViewSemiBold(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomTitilliumTextViewSemiBold(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        if(!isInEditMode()){
            //------ Font------//
            Typeface typeface1 = Typeface.createFromAsset(getContext().getAssets(), "fonts/TitilliumWeb-Bold.ttf");
            //Typeface typeface2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/SaintAndrewdesKiwis.ttf");

            setTypeface(typeface1,   Typeface.NORMAL);
        }

    }

}
