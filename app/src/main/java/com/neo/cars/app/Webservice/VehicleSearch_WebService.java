package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.CityList_Interface;
import com.neo.cars.app.Interface.VehicleSearch_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.CityListModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleSearchModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 23/10/18.
 */

public class VehicleSearch_WebService {

    Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private VehicleSearchModel vehicleSearchModel;
    JSONObject jsonObjectCity;
    ArrayList<VehicleSearchModel> arrlistVehicle;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;


    public void  vehicleSearchWebservice (Activity context) {
        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);
        arrlistVehicle = new ArrayList<>();

        gson = new Gson();
        UserLoginDetails = new UserLoginDetailsModel();
        vehicleSearchModel = new VehicleSearchModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest citylistRequest = new StringRequest(Request.Method.POST, Urlstring.state_city_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

//                if(strStateId != null){
//                    params.put("state_id", strStateId);
//                }
//                params.put("user_id", UserLoginDetails.getId());

                new PrintClass("city params******getParams***"+params);
                return params;
            }



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        citylistRequest.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(context).add(citylistRequest);


    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try{

            jobj_main = new JSONObject(response);

//            strUserDeleted = jobj_main.optJSONObject("state_city_list").optString("user_deleted");
            Msg = jobj_main.optJSONObject("state_city_list").optString("message");
            Status = jobj_main.optJSONObject("state_city_list").optString("status");

            JSONArray jsonArrayDetails = jobj_main.optJSONObject("state_city_list").optJSONArray("details");
            for (int i=0; i<jsonArrayDetails.length(); i++){

                jsonObjectCity = jsonArrayDetails.optJSONObject(i);
                VehicleSearchModel vehicleSearchModel = new VehicleSearchModel();
                String isState = jsonObjectCity.optString("is_state");
                vehicleSearchModel.setIsState(isState);
                vehicleSearchModel.setId(jsonObjectCity.optString("id"));
                vehicleSearchModel.setName(jsonObjectCity.optString("name"));

                arrlistVehicle.add(vehicleSearchModel);
            }

            Log.d("d", "Vehiclelistmodel size:"+ arrlistVehicle.size());
            Log.e("d", "Statelistmodel" + arrlistVehicle);


        }catch (Exception e) {
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }

        if (Status.equals(StaticClass.SuccessResult)) {
            ((VehicleSearch_Interface)mcontext).vehicleSearch(arrlistVehicle);

        }
    }

}
