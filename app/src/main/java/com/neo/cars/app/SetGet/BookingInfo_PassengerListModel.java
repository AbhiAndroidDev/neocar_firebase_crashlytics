package com.neo.cars.app.SetGet;

/**
 * Created by joydeep on 7/5/18.
 */

public class BookingInfo_PassengerListModel {

    public String getPassenger_name() {
        return passenger_name;
    }

    public void setPassenger_name(String passenger_name) {
        this.passenger_name = passenger_name;
    }

    public String getPassenger_contact() {
        return passenger_contact;
    }

    public void setPassenger_contact(String passenger_contact) {
        this.passenger_contact = passenger_contact;
    }

    String passenger_name,passenger_contact;


}
