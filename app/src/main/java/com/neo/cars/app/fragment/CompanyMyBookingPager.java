package com.neo.cars.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class CompanyMyBookingPager extends FragmentStatePagerAdapter {

    private int tabCount;
    private boolean upcommingFlag=false;

    public CompanyMyBookingPager(FragmentManager fm, int tabCount,boolean upcommingFlag) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
        this.upcommingFlag=upcommingFlag;
    }

    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                CompanyOngoingBookingFragment tab1 = new CompanyOngoingBookingFragment();
                return tab1;

            case 1:
                CompanyUpcomingBookingFragment tab2 = new CompanyUpcomingBookingFragment();
                Bundle bundle = new Bundle();
                bundle.putBoolean("upcommingFlag", upcommingFlag);
                tab2.setArguments(bundle);
                return tab2;

            case 2:
                CompanyPastBookingFragment tab3 = new CompanyPastBookingFragment();
                return tab3;

            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}
