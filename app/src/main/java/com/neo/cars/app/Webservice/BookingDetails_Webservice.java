package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.MyBookingDetails_Interface;
import com.neo.cars.app.Interface.Profile_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.BookingInfo_PassengerListModel;
import com.neo.cars.app.SetGet.BookingInformationModel;
import com.neo.cars.app.SetGet.DriverDataModel;
import com.neo.cars.app.SetGet.MyVehicleBookingModel;
import com.neo.cars.app.SetGet.UserDocumentModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleGalleryModel;
import com.neo.cars.app.SetGet.VehicleInformationModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by joydeep on 7/5/18.
 */

public class BookingDetails_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strShowDriver="", strShowDriverMessage="", strUserDeleted="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;


    public void BookingDetails(Activity context,final String Sbooking_id) {

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);


        showProgressDialog();

        StringRequest loginRequest = new StringRequest(Request.Method.POST, Urlstring.booking_details,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        new PrintClass("******Response***"+response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("booking_id",Sbooking_id);
                params.put("user_id", UserLoginDetails.getId());

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        loginRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(loginRequest);

    }




    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        BookingInformationModel bookingInformationModel = new BookingInformationModel();
        DriverDataModel driverdataModel=new DriverDataModel();
        VehicleInformationModel vehicleInformationModel=new VehicleInformationModel();

        try {
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("booking_details").optString("user_deleted");
            Msg = jobj_main.optJSONObject("booking_details").optString("message");
            Status= jobj_main.optJSONObject("booking_details").optString("status");
            strShowDriver = jobj_main.optJSONObject("booking_details").optString("show_driver");
            strShowDriverMessage = jobj_main.optJSONObject("booking_details").optString("show_driver_message");

            JSONObject details=jobj_main.optJSONObject("booking_details").optJSONObject("details");

            //**********booking_information

            JSONObject booking_information=details.optJSONObject("booking_information");

            bookingInformationModel.setBooking_id(booking_information.optString("booking_id"));
            bookingInformationModel.setUser_id(booking_information.optString("user_id"));
            bookingInformationModel.setVehicle_id(booking_information.optString("vehicle_id"));
            bookingInformationModel.setDriver_id(booking_information.optString("driver_id"));
            bookingInformationModel.setToll_parking_charge(booking_information.optString("toll_parking_charge"));
            bookingInformationModel.setBooking_date(booking_information.optString("booking_date"));
            bookingInformationModel.setStart_time(booking_information.optString("start_time"));
            bookingInformationModel.setEnd_time(booking_information.optString("end_time"));
            bookingInformationModel.setTotal_passenger(booking_information.optString("total_passenger"));
            bookingInformationModel.setDuration(booking_information.optString("duration"));
            bookingInformationModel.setPickup_location(booking_information.optString("pickup_location"));
            bookingInformationModel.setDrop_location(booking_information.optString("drop_location"));
            bookingInformationModel.setTotal_amount(booking_information.optString("total_amount"));
            bookingInformationModel.setAddl_request(booking_information.optString("addl_request"));
            bookingInformationModel.setAddl_info(booking_information.optString("addl_info"));
            bookingInformationModel.setExpected_route(booking_information.optString("expected_route"));
            bookingInformationModel.setBooking_purpose(booking_information.optString("booking_purpose"));
            bookingInformationModel.setVehicle_city(booking_information.optString("vehicle_city"));

            bookingInformationModel.setCancel_by(booking_information.optString("cancel_by"));
            bookingInformationModel.setCancel_reason(booking_information.optString("cancel_reason"));
            bookingInformationModel.setCancel_charge(booking_information.optString("cancel_charge"));
            bookingInformationModel.setRefundable_amount(booking_information.optString("refundable_amount"));
            bookingInformationModel.setBooking_status(booking_information.optString("booking_status"));

            bookingInformationModel.setCustomer_sex(booking_information.optString("customer_sex"));
            bookingInformationModel.setCustomer_dob(booking_information.optString("customer_dob"));
            bookingInformationModel.setCustomer_address(booking_information.optString("customer_address"));
            bookingInformationModel.setCustomer_name(booking_information.optString("customer_name"));
            bookingInformationModel.setCustomer_mail(booking_information.optString("customer_mail"));
            bookingInformationModel.setCustomer_contact(booking_information.optString("customer_contact"));
            bookingInformationModel.setCost(booking_information.optString("net_payable_amount"));

            JSONArray Ja_passList=booking_information.getJSONArray("passenger_list");

            ArrayList<BookingInfo_PassengerListModel> BIPL=new ArrayList<>();
            for(int i=0;i<Ja_passList.length();i++){
                BookingInfo_PassengerListModel bookinginopassengerlist=new BookingInfo_PassengerListModel();
                bookinginopassengerlist.setPassenger_name(Ja_passList.getJSONObject(i).optString("passenger_name"));
                bookinginopassengerlist.setPassenger_contact(Ja_passList.getJSONObject(i).optString("passenger_contact"));
                BIPL.add(bookinginopassengerlist);
            }

            bookingInformationModel.setPassengerList(BIPL);


            //**********Driver_information**********************

            JSONObject Jo_driver_information=details.optJSONObject("driver_information");

            driverdataModel.setName(Jo_driver_information.optString("driver_name"));
            driverdataModel.setContact_no(Jo_driver_information.optString("driver_contact"));
            driverdataModel.setAddress(Jo_driver_information.optString("driver_address"));
            driverdataModel.setDriver_email(Jo_driver_information.optString("driver_email"));
            driverdataModel.setDob(Jo_driver_information.optString("driver_dob"));
            driverdataModel.setGender(Jo_driver_information.optString("driver_gender"));
            driverdataModel.setNationality(Jo_driver_information.optString("driver_nationality"));
            driverdataModel.setIn_employment_since(Jo_driver_information.optString("driver_in_employment_since"));
            driverdataModel.setFathers_name(Jo_driver_information.optString("driver_fathers_name"));
            driverdataModel.setMarital_status(Jo_driver_information.optString("driver_marital_status"));
            driverdataModel.setAdditional_info(Jo_driver_information.optString("driver_additional_info"));
            driverdataModel.setRecent_photograph(Jo_driver_information.optString("recent_photograph"));

            JSONArray ja_DocList=Jo_driver_information.getJSONArray("driver_document_list");
            ArrayList<UserDocumentModel> arr_UserDocumentModel=new ArrayList<>();
            for(int i=0;i<ja_DocList.length();i++){
                UserDocumentModel userdocumentModel=new UserDocumentModel();
                userdocumentModel.setDocument_type(ja_DocList.getJSONObject(i).optString("document_type"));
                userdocumentModel.setDocument_file(ja_DocList.getJSONObject(i).optString("document_file"));
                userdocumentModel.setIs_approved(ja_DocList.getJSONObject(i).optString("is_approved"));
                arr_UserDocumentModel.add(userdocumentModel);
            }

            driverdataModel.setArr_UserDocumentModel(arr_UserDocumentModel);


            //************ vehicle_information*****************

            JSONObject Jo_vehicle_information=details.optJSONObject("vehicle_information");

            vehicleInformationModel.setVehicle_make(Jo_vehicle_information.optString("vehicle_make"));
            vehicleInformationModel.setVehicle_model(Jo_vehicle_information.optString("vehicle_model"));
            vehicleInformationModel.setVehicle_type(Jo_vehicle_information.optString("vehicle_type_name"));
            vehicleInformationModel.setVehicle_year(Jo_vehicle_information.optString("vehicle_year"));
            vehicleInformationModel.setVehicle_km_travelled(Jo_vehicle_information.optString("vehicle_km_travelled"));
            vehicleInformationModel.setHourly_rate(Jo_vehicle_information.optString("hourly_rate"));
            vehicleInformationModel.setVehicle_special_info(Jo_vehicle_information.optString("vehicle_special_info"));
            vehicleInformationModel.setVehicle_reg_number(Jo_vehicle_information.optString("vehicle_reg_number"));
            vehicleInformationModel.setLicense_plate_no(Jo_vehicle_information.optString("license_plate_no"));
            vehicleInformationModel.setMax_luggage(Jo_vehicle_information.optString("max_luggage"));
            vehicleInformationModel.setMax_passenger(Jo_vehicle_information.optString("max_passenger"));

            JSONArray Ja_image_gallery=Jo_vehicle_information.getJSONArray("image_gallery");
            ArrayList<VehicleGalleryModel> arr_VehicleGalleryModel=new ArrayList<>();

            for(int i=0;i<Ja_image_gallery.length();i++){
                VehicleGalleryModel vehiclegalleryModel=new VehicleGalleryModel();
                vehiclegalleryModel.setImage_file(Ja_image_gallery.getJSONObject(i).optString("vehicle_image"));
                vehiclegalleryModel.setIs_default(Ja_image_gallery.getJSONObject(i).optString("is_default"));
                arr_VehicleGalleryModel.add(vehiclegalleryModel);
            }
            vehicleInformationModel.setImage_gallery(arr_VehicleGalleryModel);

        }catch(Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }

        //new CustomToast(mcontext, Msg);

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                ((MyBookingDetails_Interface)mcontext).OnMyBookingDetails(bookingInformationModel,driverdataModel,vehicleInformationModel, strShowDriver, strShowDriverMessage);
            }

        }
    }
}
