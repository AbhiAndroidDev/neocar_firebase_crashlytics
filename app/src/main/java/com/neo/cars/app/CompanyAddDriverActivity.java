package com.neo.cars.app;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.Interface.DriverAdd_interface;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.CommonUtility;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.NoDefaultSpinner;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.CompanyDriverAdd_Webservice;
import com.neo.cars.app.dialog.BottomSheetDialogPositiveNegative;
import com.neo.cars.app.dialog.BottomSheetSingleDialog;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.dialog.YearMonthPickerDialog;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

//import me.iwf.photopicker.PhotoPicker;


public class CompanyAddDriverActivity extends RootActivity implements CallBackButtonClick, DriverAdd_interface {

    private Toolbar toolbar;
    private Context context;
    private Activity activity;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private RelativeLayout rlBackLayout;
    private SharedPrefUserDetails sharedPref;
    private ConnectionDetector cd;
    private RelativeLayout rlSpouseChildrenLayout;
    private CircularImageViewBorder civDriverPic;
    private ImageView ivDrivingLicense, ivDrivingLicenseBack, ivAdhaarCard, ivAdhaarCardBack, ivContactIconDriverInfo;
    private NoDefaultSpinner spnrGender, spnrMaritalStatus;
    private CustomEditTextTitilliumWebRegular etSpouseName, etChildrenName, etName,  etAddress, etFathersname, etMobNo, etAddl;
    private CustomButtonTitilliumSemibold btnProfileSave;
    private CustomTextviewTitilliumBold tvDrivingLicenseextview, tvDrivingLicenseextviewBack, tvAdhaarcardTextview, tvAdhaarcardTextviewBack;
    private CustomTextviewTitilliumWebRegular etDOB, etEmploymntSince, etNationality;

    private String strImage="", userChoosenTask="", imageFilePath="", strDriverPic="", strSelectedDriverPic="", strGender="", strSelectedGender="M", strSelectedMarritalStatus="",  strMaritalStatus="",
            strDriverName="", strDriverDob="", strDriverAddress="", strDriverNationality="", strDriverEmploymentSince="", strDriverFathersName="",
            strDriverMobNo="", strDriverSpouseName="", strDriverChildrenName="", strDriverAdditionalInfo="",
            strDrivingLicensePic="", strDrivingLicensePicBack="", strSelectedDrivingLicensePic="", strSelectedDrivingLicensePicBack="",
            strAdhaarcardpic="", strAdhaarcardpicBack="", strSelectedAdhaarCardPic="", strSelectedAdhaarCardPicBack="", strDriverFirstName="", strDriverLastName="";

    private File file, fileDriverPic, fileDrivingLicensePic, fileDrivingLicensePicBack, fileAdhaarcard, fileAdhaarcardBack;
    private Calendar mcalendar;
    private int day,month,year;

    private static final int REQUEST_IMAGE_CAPTURE = 105;
    private static final int PICK_IMAGE_REQUEST = 908;
    private static final int NationalityListRequestCode = 402;
    private static final int REQUEST_CODE_PICK_CONTACTS = 252;
    private String[] state = { StaticClass.DriverGenderMale, StaticClass.DriverGenderFemale};
    private String[] maritalStatus = { StaticClass.DriverMarried, StaticClass.DriverSingle} ;
    private ArrayAdapter<String> arrayAdapterGender, arrayAdapterMaritalStatus;
    private Bundle bundle;
    private CustomEditTextTitilliumWebRegular etFirstName, etLastName;
    private CustomTextviewTitilliumBold tvFirstNameheader, tvLastNameheader;
    private CustomTitilliumTextViewSemiBold tvSexHeader, tvAddressHeader, tvNationalityHeader,
            tvEmplHeader, tvFathersnameHeader, tvMobNoHeader, tvMaritalStatusHeader, tvSpouseNameHeader, tvDOBHeader;

    private Uri uriContact;
    private String contactID;     // contacts unique ID
    private int transitionflag = StaticClass.transitionflagNext;
    private ProgressDialog dialog;
    private Uri imageUri;

    private BottomViewCompany bottomViewCompany = new BottomViewCompany();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_add_driver);
        initialize();
        listener();

        new AnalyticsClass(CompanyAddDriverActivity.this);
    }

    private void initialize() {
        sharedPref = new SharedPrefUserDetails(this);
        activity = CompanyAddDriverActivity.this;
        context = CompanyAddDriverActivity.this;
        cd = new ConnectionDetector(this);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        rlBackLayout = findViewById(R.id.rlBackLayout);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvDetails));

        mcalendar = Calendar.getInstance();
        day=mcalendar.get(Calendar.DAY_OF_MONTH);
        year=mcalendar.get(Calendar.YEAR);
        month=mcalendar.get(Calendar.MONTH);

        tvFirstNameheader = findViewById(R.id.tvFirstNameheader);
        tvLastNameheader = findViewById(R.id.tvLastNameheader);
        tvSexHeader = findViewById(R.id.tvSexHeader);
        tvAddressHeader = findViewById(R.id.tvAddressHeader);
        tvNationalityHeader = findViewById(R.id.tvNationalityHeader);
        tvEmplHeader = findViewById(R.id.tvEmplHeader);
        tvFathersnameHeader = findViewById(R.id.tvFathersnameHeader);
        tvMobNoHeader = findViewById(R.id.tvMobNoHeader);
        tvMaritalStatusHeader = findViewById(R.id.tvMaritalStatusHeader);
        tvSpouseNameHeader = findViewById(R.id.tvSpouseNameHeader);
        tvDOBHeader = findViewById(R.id.tvDOBHeader);

        civDriverPic = findViewById(R.id.civDriverPic);
        ivDrivingLicense = findViewById(R.id.ivDrivingLicense);
        ivDrivingLicenseBack = findViewById(R.id.ivDrivingLicenseBack);
        ivAdhaarCard = findViewById(R.id.ivAdhaarCard);
        ivAdhaarCardBack = findViewById(R.id.ivAdhaarCardBack);
        //ivContactIconDriverInfo = findViewById(R.id.ivContactIconDriverInfo);
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etDOB = findViewById(R.id.etDOB);
        etAddress =  findViewById(R.id.etAddress);
        etNationality = findViewById(R.id.etNationality);
        etEmploymntSince = findViewById(R.id.etEmploymntSince);
        etFathersname =  findViewById(R.id.etFathersname);
        etMobNo =  findViewById(R.id.etMobNo);
        etAddl =  findViewById(R.id.etAddl);
        spnrGender = findViewById(R.id.spnrGender);
        spnrMaritalStatus = findViewById(R.id.spnrMaritalStatus);
        rlSpouseChildrenLayout = findViewById(R.id.rlSpouseChildrenLayout);
        etSpouseName = findViewById(R.id.etSpouseName);
        etChildrenName = findViewById(R.id.etChildrenName);
        btnProfileSave = findViewById(R.id.btnProfileSave);
        tvDrivingLicenseextview = findViewById(R.id.tvDrivingLicenseextview);
        tvDrivingLicenseextviewBack = findViewById(R.id.tvDrivingLicenseextviewBack);
        tvAdhaarcardTextview = findViewById(R.id.tvAdhaarcardTextview);
        tvAdhaarcardTextviewBack = findViewById(R.id.tvAdhaarcardTextviewBack);

        //Gender value
        arrayAdapterGender = new ArrayAdapter<String>(activity, R.layout.countryitem, state);
        arrayAdapterGender.setDropDownViewResource(R.layout.simpledropdownitem);
        spnrGender.setPrompt(getResources().getString(R.string.tvPleaseSelectGender));
        spnrGender.setAdapter(arrayAdapterGender);
        spnrGender.setSelection(0);

        spnrGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                spnrGender.setSelection(position);
                strGender = (String) spnrGender.getSelectedItem();
                Log.d("d", "Selected Gender: "+strGender);

                if (StaticClass.DriverGenderMale.equalsIgnoreCase(strGender)){
                    strSelectedGender = "M";
                }else if (StaticClass.DriverGenderFemale.equalsIgnoreCase(strGender)){
                    strSelectedGender = "F";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        //Marital status value
        arrayAdapterMaritalStatus = new ArrayAdapter<String>(activity, R.layout.countryitem, maritalStatus);
        arrayAdapterMaritalStatus.setDropDownViewResource(R.layout.simpledropdownitem);
        spnrMaritalStatus.setPrompt(getResources().getString(R.string.tvPleaseselectyourmaritalstatus));
        spnrMaritalStatus.setAdapter(arrayAdapterMaritalStatus);

        spnrMaritalStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long arg3) {
                spnrMaritalStatus.setSelection(position);
                strMaritalStatus = (String) spnrMaritalStatus.getSelectedItem();
                Log.d("d", "Selected marital status: " + strMaritalStatus);

                if (StaticClass.DriverMarried.equalsIgnoreCase(strMaritalStatus)){
                    Log.d("d", "Married status::");
                    strSelectedMarritalStatus = "Y";
                    Log.d("d", "View marital status1: " + strSelectedMarritalStatus);
                    rlSpouseChildrenLayout.setVisibility(View.VISIBLE);

                }else if (StaticClass.DriverSingle.equalsIgnoreCase(strMaritalStatus)){
                    Log.d("d", "Unmarried status::");
                    strSelectedMarritalStatus = "N";
                    Log.d("d", "View marital status2: " + strSelectedMarritalStatus);
                    rlSpouseChildrenLayout.setVisibility(View.GONE);

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        etNationality.setText("India");
    }

    private void listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(activity,
                        activity.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                        activity.getResources().getString(R.string.yes),
                        activity.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();

                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
            }
        });

        btnProfileSave.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                etFirstName.setError(null);
                etLastName.setError(null);
                etAddress.setError(null);
                etNationality.setError(null);
                etEmploymntSince.setError(null);
                etFathersname.setError(null);

                boolean cancel = false;
                View focusView = null;

                strDriverFirstName = etFirstName.getText().toString().trim();
                strDriverLastName = etLastName.getText().toString().trim();
                strDriverDob = etDOB.getText().toString().trim();
                strDriverAddress = etAddress.getText().toString().trim();
                strDriverNationality = etNationality.getText().toString().trim();
                strDriverEmploymentSince = etEmploymntSince.getText().toString().trim();
                strDriverFathersName = etFathersname.getText().toString().trim();
                strDriverMobNo = etMobNo.getText().toString().trim();
                strDriverSpouseName = etSpouseName.getText().toString().trim();
                strDriverChildrenName = etChildrenName.getText().toString().trim();
                strDriverAdditionalInfo = etAddl.getText().toString().trim();

                //Driver Mob no validation
                if (TextUtils.isEmpty(strDriverMobNo)) {
                    if (!(new Emailvalidation().phone_validation(strDriverMobNo))) {
                        etMobNo.setError(getString(R.string.txt_message_mobile_no));
                        focusView = etMobNo;
                        cancel = true;
                    }
                }

                if (TextUtils.isEmpty(strDriverFirstName)) {
                    etFirstName.setError(getString(R.string.error_field_required));
                    focusView = etFirstName;
                    cancel = true;

                } else if (TextUtils.isEmpty(strDriverLastName)){
                    etLastName.setError(getString(R.string.error_field_required));
                    focusView = etLastName;
                    cancel = true;

                }

                else if (TextUtils.isEmpty(strDriverAddress)){
                    etAddress.setError(getString(R.string.error_field_required));
                    focusView = etAddress;
                    cancel = true;

                }else if (TextUtils.isEmpty(strDriverNationality)){
                    etNationality.setError(getString(R.string.error_field_required));
                    focusView = etNationality;
                    cancel = true;

                }else if (TextUtils.isEmpty(strDriverEmploymentSince)){
                    etEmploymntSince.setError(getString(R.string.error_field_required));
                    focusView = etEmploymntSince;
                    cancel = true;

                }else if (TextUtils.isEmpty(strDriverFathersName)){
                    etFathersname.setError(getString(R.string.error_field_required));
                    focusView = etFathersname;
                    cancel = true;

                }else if (TextUtils.isEmpty(strDriverMobNo)){
                    etMobNo.setError(getString(R.string.error_field_required));
                    focusView = etMobNo;
                    cancel = true;

                }else if (cancel) {
                    // form field with an error.
                    focusView.requestFocus();

                }else if (TextUtils.isEmpty(strMaritalStatus)){

                    new CustomToast(activity, getResources().getString(R.string.msgmaritalstatus));

                } else {

                   if (fileDriverPic == null){
                        new CustomToast(activity, getResources().getString(R.string.addDriverPic));

                    }
//                    else if (fileAdhaarcard == null){
//                        new CustomToast(activity, getResources().getString(R.string.upload_adhaar_card));
//
//                    }else if (fileAdhaarcardBack == null){
//                       new CustomToast(activity, getResources().getString(R.string.upload_adhaar_card));
//
//                   }
                   else if (fileDrivingLicensePic == null){
                        new CustomToast(activity, getResources().getString(R.string.upload_driving_licence));

                    }else if (fileDrivingLicensePicBack == null){
                       new CustomToast(activity, getResources().getString(R.string.upload_driving_licence));

                   }
                    else{

                        if(cd.isConnectingToInternet()){
                            new CompanyDriverAdd_Webservice().DriverAdd(activity, strDriverFirstName, strDriverLastName,  strSelectedGender, strDriverAddress,
                                    strDriverNationality, strDriverEmploymentSince, strDriverFathersName, strDriverMobNo, strSelectedMarritalStatus,
                                    strDriverSpouseName, strDriverChildrenName, strDriverAdditionalInfo, fileDriverPic, strDriverDob,
                                    fileAdhaarcard, fileAdhaarcardBack, fileDrivingLicensePic, fileDrivingLicensePicBack, "");

                        }else{
                            Intent i = new Intent(activity, NetworkNotAvailable.class);
                            startActivity(i);
                        }
                    }
                }
            }
        });

        civDriverPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "DriverPic";
                selectDriverPic(getResources().getString(R.string.msgUploadDriverPic), strImage);
            }
        });

        etDOB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DobDialog();
            }
        });

        tvDrivingLicenseextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "DrivingLicense";
                selectImage(getResources().getString(R.string.msgUpldDrivingLicense), strImage);
            }
        });

        tvDrivingLicenseextviewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "DrivingLicenseBack";
                selectImage(getResources().getString(R.string.msgUpldDrivingLicense), strImage);

            }
        });

        tvAdhaarcardTextview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "AdhaarCard";
                selectImage(getResources().getString(R.string.msgUploadAdhaarCard), strImage);
            }
        });

        tvAdhaarcardTextviewBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                strImage = "AdhaarCardBack";
                selectImage(getResources().getString(R.string.msgUploadAdhaarCard), strImage);
            }
        });

        etEmploymntSince.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                inEmployementSince();
            }
        });

        etNationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent stateintent = new Intent(context, NationalityListActivity.class);
                startActivityForResult(stateintent, NationalityListRequestCode);
            }
        });
    }


    public void DobDialog(){

        etDOB.setInputType(InputType.TYPE_NULL);
        etDOB.setTextIsSelectable(true);

        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Log.d("d", "monthOfYear:" +monthOfYear);
                etDOB.setText(year + "-" + (monthOfYear+1) + "-" +dayOfMonth );
                Log.d("d", "String Date::"+etDOB.getText().toString().trim());

            }
        };
        DatePickerDialog dpDialog = new DatePickerDialog(context, listener, year, month, day);
        dpDialog.getDatePicker().setMaxDate(mcalendar.getTimeInMillis());
        dpDialog.show();

    }

    public void inEmployementSince(){

        Calendar calendar = Calendar.getInstance();
        calendar.set(calendar.get(Calendar.YEAR),01,01);

        YearMonthPickerDialog yearMonthPickerDialog = new YearMonthPickerDialog(activity,
                calendar,
                new YearMonthPickerDialog.OnDateSetListener() {
                    @Override
                    public void onYearMonthSet(int year) {
                        System.out.println("**YearMonthPickerDialog**"+year);
                        etEmploymntSince.setText(String.valueOf(year));
                    }
                });

        yearMonthPickerDialog.show();
    }

    public void selectDriverPic (final String strMessage, String strImage){

        BottomSheetSingleDialog bsd = new BottomSheetSingleDialog(context,
                activity,
                CompanyAddDriverActivity.this,
                strMessage,
                CompanyAddDriverActivity.this.getResources().getString(R.string.Camera));
    }

    public void selectImage(final String strMessage, String filetype) {

        BottomSheetDialogPositiveNegative bsd = new BottomSheetDialogPositiveNegative(context,
                activity,
                CompanyAddDriverActivity.this,
                strMessage,
                CompanyAddDriverActivity.this.getResources().getString(R.string.Camera),
                CompanyAddDriverActivity.this.getResources().getString(R.string.Gallery));

    }

    //take photo through camera
    private void takePhoto() {

        imageFilePath = ImageUtils.getFile().getAbsolutePath();
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        imageUri = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", ImageUtils.getFile());// return content:///..
        //imageUri=Uri.fromFile(getFile()); // returns file:///...
        Log.d("@ file uri :", imageUri.toString());
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //API >24
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
    }

    //select image from gallery
    private void mCallPhotoGallary() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_PICK);
        startActivityForResult(Intent.createChooser(intent, "Select a photo"), PICK_IMAGE_REQUEST);

//        try {
//            PhotoPicker.builder()
//                    .setPhotoCount(1)
//                    .setShowCamera(true)
//                    .setShowGif(false)
//                    .setPreviewEnabled(false)
//                    .start(this, PICK_IMAGE_REQUEST);
//
//        } catch (Exception e) {
//            //To catch unknown Null pointer exception inside library
//            e.printStackTrace();
//        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        sharedPref.putBottomViewCompany(StaticClass.Menu_DriverLists_company);
        bottomViewCompany = new BottomViewCompany();
        bottomViewCompany.BottomViewCompany(CompanyAddDriverActivity.this, StaticClass.Menu_DriverLists_company);


    }

    @Override
    public void onBackPressed() {
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(activity,
                activity.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                activity.getResources().getString(R.string.yes),
                activity.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {

            switch (requestCode) {

                case REQUEST_IMAGE_CAPTURE:
                    try {
                        dialog = new ProgressDialog(context);
                        try {

                            dialog.setMessage("Image processing...");
                            dialog.show();
                            final Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try
                                    {
                                        Bitmap fullImage = MediaStore.Images.Media.getBitmap(getContentResolver(),imageUri);

                                        InputStream input = context.getContentResolver().openInputStream(imageUri);
                                        ExifInterface ei;
                                        if (Build.VERSION.SDK_INT > 23)
                                            ei = new ExifInterface(input);
                                        else
                                            ei = new ExifInterface(imageUri.getPath());

                                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

                                        switch (orientation) {
                                            case ExifInterface.ORIENTATION_ROTATE_90:
                                                fullImage = rotateImage(fullImage, 90);
                                                break;
                                            case ExifInterface.ORIENTATION_ROTATE_180:
                                                fullImage = rotateImage(fullImage, 180);
                                                break;
                                            case ExifInterface.ORIENTATION_ROTATE_270:
                                                fullImage = rotateImage(fullImage, 270);
                                                break;
                                            default:
                                                break;
                                        }

                                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                                        fullImage.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                                        byte[] byteArray = bytes.toByteArray();
                                        Bitmap scaledBitmap=ImageUtils.decodeSampledBitmapFromResource(byteArray,400,400);
                                        file = ImageUtils.saveImage(scaledBitmap, context);

                                        if (file != null) {
                                            if (strImage == "DriverPic"){
                                                fileDriverPic = file;
                                                Log.d("d", "Camera Driver pic file:" + fileDriverPic);
                                                Picasso.get().load(fileDriverPic).resize(200, 200).into(civDriverPic);

                                            }
                                            else if (strImage == "DrivingLicense"){
                                                fileDrivingLicensePic = file;
                                                Log.d("d", "Camera Driving license pic file: "+fileDrivingLicensePic);
                                                Picasso.get().load(fileDrivingLicensePic).resize(200, 200).into(ivDrivingLicense);

                                            }

                                            else if (strImage == "DrivingLicenseBack"){
                                                fileDrivingLicensePicBack = file;
                                                Log.d("d", "Camera Driving license pic file: "+fileDrivingLicensePicBack);
                                                Picasso.get().load(fileDrivingLicensePicBack).resize(200, 200).into(ivDrivingLicenseBack);

                                            }

                                            else if (strImage == "AdhaarCard"){
                                                fileAdhaarcard = file;
                                                Log.d("d", "Camera Adhaar card pic file: "+fileAdhaarcard);
                                                Picasso.get().load(fileAdhaarcard).resize(200, 200).into(ivAdhaarCard);
                                            }

                                            else if (strImage == "AdhaarCardBack"){
                                                fileAdhaarcardBack = file;
                                                Log.d("d", "Camera Adhaar card pic file: "+fileAdhaarcardBack);
                                                Picasso.get().load(fileAdhaarcardBack).resize(200, 200).into(ivAdhaarCardBack);
                                            }
                                        }

                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        Toast.makeText(context, "Failed saving!", Toast.LENGTH_SHORT).show();
                                    }
                                    finally{
                                        dialog.dismiss();
                                    }
                                }
                            }, 4000);  // 4 sec to allow processing of image captured


                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(context, "Failed camera!", Toast.LENGTH_SHORT).show();
                        }
                    }catch (NullPointerException e){
                        e.printStackTrace();
                        Toast.makeText(context, "NULL PATH", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case PICK_IMAGE_REQUEST:

                    try{
//                        ArrayList<String> photos = data.getStringArrayListExtra(PhotoPicker.KEY_SELECTED_PHOTOS);

                        BufferedInputStream bufferedInputStream;
                        Bitmap bmp;

                        if(data != null) {
                            InputStream inputStream = context.getContentResolver().openInputStream(Objects.requireNonNull(data.getData()));
                            //Now you can do whatever you want with your inpustream, save it as file, upload to a server, decode a bitmap...

                            bufferedInputStream = new BufferedInputStream(inputStream);
                            bmp = BitmapFactory.decodeStream(bufferedInputStream);

                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            bmp.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                            byte[] byteArray = bytes.toByteArray();
                            Bitmap scaledBitmap = ImageUtils.decodeSampledBitmapFromResource(byteArray, 400, 400);
                            file = ImageUtils.saveImageGallery(scaledBitmap, context);
                        }


                        if(strImage.equals("DriverPic")){
//                            strDriverPic = "file://" + photos.get(0);
//                            Log.d("d", "Driver pic path : " + strDriverPic);
//                            strSelectedDriverPic = photos.get(0);
//                            Log.d("d", "Driver pic to be uploaded: " + strSelectedDriverPic);
                            fileDriverPic = file;
                            Log.d("d", "Driver pic Image File::" + fileDriverPic);
                            Picasso.get().load(file).resize(200, 200).into(civDriverPic);


                        }

                        else if (strImage.equals("DrivingLicense")){
//                            strDrivingLicensePic = "file://" + photos.get(0);
//                            Log.d("d", "Driving license pic path : " + strDrivingLicensePic);
//                            strSelectedDrivingLicensePic = photos.get(0);
//                            Log.d("d", "Driving license pic to be uploaded: " + strSelectedDrivingLicensePic);
                            fileDrivingLicensePic = file;
                            Log.d("d", "Driving license Image file: "+fileDrivingLicensePic);
                            Picasso.get().load(file).resize(200, 200).into(ivDrivingLicense);

                        }

                        else if (strImage.equals("DrivingLicenseBack")){
//                            strDrivingLicensePicBack = "file://" + photos.get(0);
//                            Log.d("d", "Driving license pic path : " + strDrivingLicensePicBack);
//                            strSelectedDrivingLicensePicBack = photos.get(0);
//                            Log.d("d", "Driving license pic to be uploaded: " + strSelectedDrivingLicensePicBack);
                            fileDrivingLicensePicBack = file;
                            Log.d("d", "Driving license Image file: "+fileDrivingLicensePicBack);
                            Picasso.get().load(file).resize(200, 200).into(ivDrivingLicenseBack);
                        }


                        else if (strImage.equals("AdhaarCard")){
//                            strAdhaarcardpic = "file://" + photos.get(0);
//                            Log.d("d", "Adhaar card pic path : " + strAdhaarcardpic);
//                            strSelectedAdhaarCardPic = photos.get(0);
//                            Log.d("d", "Adhaar card pic to be uploaded: " + strSelectedAdhaarCardPic);
                            fileAdhaarcard = file;
                            Log.d("d", "Adhaar card Image file: "+fileAdhaarcard);
                            Picasso.get().load(file).resize(200, 200).into(ivAdhaarCard);
                        }

                        else if (strImage.equals("AdhaarCardBack")){
//                            strAdhaarcardpicBack = "file://" + photos.get(0);
//                            Log.d("d", "Adhaar card pic path : " + strAdhaarcardpicBack);
//                            strSelectedAdhaarCardPicBack = photos.get(0);
//                            Log.d("d", "Adhaar card pic to be uploaded: " + strSelectedAdhaarCardPicBack);
                            fileAdhaarcardBack = file;
                            Log.d("d", "Adhaar card Image file: "+fileAdhaarcardBack);
                            Picasso.get().load(file).resize(200, 200).into(ivAdhaarCardBack);

                        }

                    }catch (Exception e){
                        e.printStackTrace();
                    }
                    break;

                case NationalityListRequestCode:

                    if (!data.getExtras().getString("Nationality").equals("")) {
                        etNationality.setText(data.getExtras().getString("Nationality"));
                    }

                    break;

                case REQUEST_CODE_PICK_CONTACTS:
                    Log.d("**Response:**", "Response: " + data.toString());
                    uriContact = data.getData();

                    getContactNumber(context, data);
                    break;
            }
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }

    public void getContactNumber(final Context context, Intent data) {
        String selectedNumber = "";
        Cursor cursor = null;
        String phoneNumber = "", primaryMobile = "";

        List<String> allNumbers = new ArrayList<>();
        int contactIdColumnId, phoneColumnID, nameColumnID;
        try {
            Uri result = data.getData();
            String id = result.getLastPathSegment();
            cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", new String[]{id}, null);
            contactIdColumnId = cursor.getColumnIndex(ContactsContract.Data.RAW_CONTACT_ID);
            phoneColumnID = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA);
            nameColumnID = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);

            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {
                    String idContactBook = cursor.getString(contactIdColumnId);
                    String displayName = cursor.getString(nameColumnID);
                    phoneNumber = cursor.getString(phoneColumnID);
                    Log.e("Phone", phoneNumber);

                    if (phoneNumber.length() == 0)
                        continue;

                    int type = cursor.getInt(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
                    if (type == ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE && primaryMobile.equals(""))
                        primaryMobile = phoneNumber;
                    allNumbers.add(phoneNumber);
                    allNumbers.size();
                    cursor.moveToNext();
                }
            } else {
                // no results actions
            }
        } catch (Exception e) {
            // error actions
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            if (allNumbers.size() > 1 && !allNumbers.isEmpty()) {
                final CharSequence[] items = allNumbers.toArray(new String[allNumbers.size()]);
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
                builder.setTitle("Choose a number");
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int item) {

                        String selectedNumber = items[item].toString().replaceAll("[^0-9\\+]", "");
                        Log.d("numberOnClick", selectedNumber);

                        if (selectedNumber != null && selectedNumber.matches("^([+][9][1]|[9][1]|[0]){0,1}([6-9]{1})([0-9]{9})$")) {
                            etMobNo.setText(selectedNumber);
                            etMobNo.requestFocus();

                        } else {
                            new CustomToast(CompanyAddDriverActivity.this, getResources().getString(R.string.txt_message_mobile_no));
                        }
                    }
                });
                android.app.AlertDialog alert = builder.create();
                alert.show();

            } else {
                selectedNumber = phoneNumber.replaceAll("[^0-9\\+]", "");
                Log.d("number", selectedNumber);
                if (selectedNumber != null && selectedNumber.matches("^([+][9][1]|[9][1]|[0]){0,1}([6-9]{1})([0-9]{9})$")) {
                    etMobNo.setText(selectedNumber);
                    etMobNo.requestFocus();
                } else {
                    new CustomToast(CompanyAddDriverActivity.this, getResources().getString(R.string.txt_message_mobile_no));
                }
            }
        }
    }

    @Override
    public void onButtonClick(String strButtonText) {
        System.out.println("****strButtonText***"+strButtonText);

//        boolean result = CommonUtility.checkPermission(context);

        if (strButtonText.equals(activity.getResources().getString(R.string.Camera))) {
            new PrintClass("Alert Camera");
            userChoosenTask = "Camera";
//            if (result) {

                Dexter.withActivity(this).withPermissions( Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener()
                        {
                            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if(report.areAllPermissionsGranted()){

                                    takePhoto();
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // permission is denied permenantly, navigate user to app settings
                                    showAlert();
                                }
                            }
                            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                            {/* ... */

                                token.continuePermissionRequest();
                            }
                        }).check();
//            }

        } else if (strButtonText.equals(activity.getResources().getString(R.string.Gallery))) {
            new PrintClass("Alert Gallery");
            userChoosenTask = "Gallery";
//            if (result) {

                Dexter.withActivity(this).withPermissions( Manifest.permission.READ_EXTERNAL_STORAGE )
                        .withListener(new MultiplePermissionsListener()
                        {
                            @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if(report.areAllPermissionsGranted()){

                                    mCallPhotoGallary();
                                }

                                // check for permanent denial of any permission
                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    // permission is denied permenantly, navigate user to app settings
                                    showAlert();
                                }
                            }
                            @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                            {/* ... */

                                token.continuePermissionRequest();
                            }
                        }).check();
//            }
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CompanyAddDriverActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void driverAddInterface(String status, String msg) {
        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(this,
                msg ,
                getResources().getString(R.string.btn_ok),
                "");
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialogYESNO.dismiss();
                ImageUtils.deleteImageGallery();
                finish();

            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }
}
