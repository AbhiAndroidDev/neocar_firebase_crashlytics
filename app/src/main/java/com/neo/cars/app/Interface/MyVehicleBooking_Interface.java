package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.MyVehicleBookingModel;

import java.util.ArrayList;

/**
 * Created by joydeep on 4/5/18.
 */

public interface MyVehicleBooking_Interface {

    public void onMyVehicleBooking(ArrayList<MyVehicleBookingModel> arrlistVehicle);
}
