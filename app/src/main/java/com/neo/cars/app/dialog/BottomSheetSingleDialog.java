package com.neo.cars.app.dialog;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.R;

/**
 * Created by parna on 7/6/18.
 */

public class BottomSheetSingleDialog {

    private Context mContext;
    private Activity mActivity;
    private String mStrMsg, mStrPositive;
    private CallBackButtonClick buttonClick=null;

    public BottomSheetSingleDialog(Context context, Activity activity,
                                             String strMsg, String strPositive) {

        this.mContext = context;
        this.mActivity = activity;
        this.mStrMsg = strMsg;
        this.mStrPositive = strPositive;

        // call bottom sheet dialog
        callBottomSheetDialog();
    }

    public BottomSheetSingleDialog(Context context, Activity activity,
                                             CallBackButtonClick buttonClick,
                                             String strMsg,
                                             String strPositive) {
        this.mContext = context;
        this.mActivity = activity;
        this.buttonClick = buttonClick;
        this.mStrMsg = strMsg;
        this.mStrPositive = strPositive;

        // call bottom sheet dialog
        callBottomSheetDialog();
    }

    private void callBottomSheetDialog() {
        final BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(mContext);
        View sheetView = mActivity.getLayoutInflater().inflate(R.layout.bottom_sheet_single_dialog, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        TextView tvMsg;
        Button  btPositive;

        tvMsg = sheetView.findViewById(R.id.tvMsg);
        btPositive = sheetView.findViewById(R.id.btPositive);

        tvMsg.setText(mStrMsg);
        btPositive.setText(mStrPositive);

        btPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                if (buttonClick == null) {
                    ((CallBackButtonClick) mActivity).onButtonClick(mStrPositive);
                }else {
                    buttonClick.onButtonClick(mStrPositive);
                }
            }
        });
    }
}
