package com.neo.cars.app.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by parna on 8/3/18.
 */

public class CouponPager extends FragmentStatePagerAdapter {


    //integer to count number of tabs
    private int tabCount;

    //Constructor to the class
    public CouponPager(FragmentManager fm, int tabCount) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
    }

    //Overriding method getItem
    @Override
    public Fragment getItem(int position) {
        //Returning the current tabs
        switch (position) {
            case 0:
                ViewCouponFragment tab1 = new ViewCouponFragment();
                return tab1;

            case 1:
                RedeemCouponFragment tab2 = new RedeemCouponFragment();
                return tab2;


            default:
                return null;
        }
    }

    //Overriden method getCount to get the number of tabs
    @Override
    public int getCount() {
        return tabCount;
    }
}
