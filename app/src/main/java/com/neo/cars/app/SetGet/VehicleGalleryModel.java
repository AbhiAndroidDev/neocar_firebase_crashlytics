package com.neo.cars.app.SetGet;

/**
 * Created by parna on 23/4/18.
 */

public class VehicleGalleryModel {

    String id;
    String user_vehicle_id;
    String image_file;
    String is_default;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_vehicle_id() {
        return user_vehicle_id;
    }

    public void setUser_vehicle_id(String user_vehicle_id) {
        this.user_vehicle_id = user_vehicle_id;
    }

    public String getImage_file() {
        return image_file;
    }

    public void setImage_file(String image_file) {
        this.image_file = image_file;
    }

    public String getIs_default() {
        return is_default;
    }

    public void setIs_default(String is_default) {
        this.is_default = is_default;
    }
}
