package com.neo.cars.app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.WelcomeEmailWebService;

public class EmailConfirmationActivity extends AppCompatActivity {

    private Button btnContinue;
    private TextView tvPreEmailId;
    private LinearLayout ll_EditEmail;
    private String userType = "";
    private SharedPrefUserDetails sharedPref;
    private Gson gson;
    private UserLoginDetailsModel userLoginDetails;
    private ConnectionDetector connectionDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_confirmation);
        new AnalyticsClass(EmailConfirmationActivity.this);
        initialize();
        listener();
    }

    private void initialize() {

        gson = new Gson();
        userLoginDetails=new UserLoginDetailsModel();
        sharedPref = new SharedPrefUserDetails(this);
        connectionDetector = new ConnectionDetector(this);
        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        userLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        btnContinue = findViewById(R.id.btnContinue);
        tvPreEmailId = findViewById(R.id.tvPreEmailId);
        ll_EditEmail = findViewById(R.id.ll_EditEmail);

        userType = userLoginDetails.getUser_type();

    }

    private void listener(){
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (connectionDetector.isConnectingToInternet()){

                    sharedPref.setUserloginStatus();

                    (new WelcomeEmailWebService()).welcomeEmailWebservice(EmailConfirmationActivity.this);

                    if (userType.equalsIgnoreCase("U")) {
                        sharedPref.setUserloginStatus();
                        sharedPref.putBottomView(StaticClass.Menu_Explore);
//                        Intent invitefriendsIntent = new Intent(EmailConfirmationActivity.this, HomeProfileActivity.class);
                        Intent invitefriendsIntent = new Intent(EmailConfirmationActivity.this, SearchActivity.class);
                        startActivity(invitefriendsIntent);
                        finish();

                    } else if (userType.equalsIgnoreCase("C")) {
                        sharedPref.setUserloginStatus();
                        Intent invitefriendsIntent = new Intent(EmailConfirmationActivity.this, CompanyHomeProfileActivity.class);
                        startActivity(invitefriendsIntent);
                        finish();
                    }

                }else {
                    startActivity(new Intent(EmailConfirmationActivity.this, NetworkNotAvailable.class));
                }
            }
        });

        ll_EditEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(EmailConfirmationActivity.this, EditEmailActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!TextUtils.isEmpty(sharedPref.getEmail())){
            tvPreEmailId.setText(sharedPref.getEmail());
        }else {

            tvPreEmailId.setText(userLoginDetails.getEmail());

        }
    }
}
