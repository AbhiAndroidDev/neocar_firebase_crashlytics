package com.neo.cars.app.View;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.R;
import com.neo.cars.app.RegistrationActivity;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.Emailvalidation;
import com.neo.cars.app.Utils.MessageText;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by parna on 10/5/18.
 */

public class AddStopOverAdapterBooking {

    @SuppressLint("InflateParams")

    ArrayList<String> arrNamebooking = new ArrayList<String>();
    ArrayList<String> arrMobileNoBooking = new ArrayList<String>();

    LayoutInflater inflater;
    Activity context;
    LinearLayout addstopover_listview;
    private boolean checkFiledFlag=true;

    private static final int REQUEST_CODE_PICK_CONTACTS = 105;

    public AddStopOverAdapterBooking (Activity context, LinearLayout addstopoverlistview){
        this.context = context;
        inflater = LayoutInflater.from(this.context);
        addstopover_listview=addstopoverlistview;

        arrNamebooking = new ArrayList<String>();
        arrMobileNoBooking = new ArrayList<String>();

        arrNamebooking = StaticClass.getAl_NameBooking();
        arrMobileNoBooking= StaticClass.getAl_MobileNoBooking();

        addstopover_listview.removeAllViews();
        for(int i=0;i<arrNamebooking.size();i++){
            addstopover_listview.addView(addView2(i));
        }
    }

    public View addView2(final int position) {

        View v;
        v = LayoutInflater.from(context).inflate(R.layout.addmore_name_mobno, null);

        CustomEditTextTitilliumWebRegular tvAddName = v.findViewById(R.id.tvAddName);
        CustomEditTextTitilliumWebRegular tvAddMobNo = v.findViewById(R.id.tvAddMobNo);

        //ImageView ivContactIconAddMore = v.findViewById(R.id.ivContactIconAddMore);

        tvAddName.setText(arrNamebooking.get(position));
        if(!TextUtils.isEmpty(arrMobileNoBooking.get(position)) && new Emailvalidation().phone_validation(arrMobileNoBooking.get(position).trim())) {
            tvAddMobNo.setText(arrMobileNoBooking.get(position));
        }




        tvAddName.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(count==0){
                    arrNamebooking.set(position, "");
                }else {
                    arrNamebooking.set(position, s.toString());
                }
                StaticClass.setAl_NameBooking(arrNamebooking);
            }
        });

        tvAddMobNo.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if(count==0){
                    arrMobileNoBooking.set(position, "");
                }else {
//                    if(!TextUtils.isEmpty(s.toString()) && new Emailvalidation().phone_validation(s.toString())) {
                        arrMobileNoBooking.set(position, s.toString());
//                    }
                }

                StaticClass.setAl_MobileNoBooking(arrMobileNoBooking);
            }
        });

        return v;
    }


    public void showAlert(){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setTitle("Permissions Required")
                .setMessage("You have forcefully denied some of the required permissions " +
                        "for this action. Please open settings, go to permissions and allow them.")
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", context.getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }
}
