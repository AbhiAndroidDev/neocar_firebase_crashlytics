package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;

/**
 * Created by parna on 10/5/18.
 */

public interface ProcessBooking_interface {

    public void processBooking(VehicleTypeModel vehicleTypeModel);
}
