package com.neo.cars.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.neo.cars.app.Adapter.CompanyOngoingBookingAdapter;
import com.neo.cars.app.Interface.CompanyBookingList_Interface;
import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.CompanyBookingListModel;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.CompanyBookingList_Webservice;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;

public class CompanyOngoingBookingFragment extends Fragment implements CompanyBookingList_Interface
        ,SwipeRefreshLayout.OnRefreshListener {

    private View mView;
    private Context context;
    private RecyclerView rcvMyBooking;
    private LinearLayoutManager layoutManagerVertical;
    private CompanyOngoingBookingAdapter ongoingBookingAdapter;
    private ArrayList<CompanyBookingListModel> myBookingModelList = new ArrayList<>();
    private SwipeRefreshLayout swipeRefreshLayout;
    private CustomTitilliumTextViewSemiBold tvNoTripsFound;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        try {
            mView = inflater.inflate(R.layout.fragment_upcoming_mybooking, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        Initialize();
        Listener();
        refreshList();

        return mView;
    }

    @Override
    public void onRefresh(){
        swipeRefreshLayout.setRefreshing(true);
        refreshList();
    }

    private void  refreshList(){
        if (NetWorkStatus.isNetworkAvailable(getActivity())) {
            new CompanyBookingList_Webservice().CompanyBookingList(getActivity(), StaticClass.MyVehicleOngoing, CompanyOngoingBookingFragment.this);

        } else {
            StaticClass.MyBookingListFlag=true;
            Intent i = new Intent(getActivity(), NetworkNotAvailable.class);
            startActivity(i);
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    private void Initialize(){

        context = getActivity();
        swipeRefreshLayout = mView.findViewById(R.id.refresh);
        rcvMyBooking = mView.findViewById(R.id.rcvMyBooking);
        tvNoTripsFound = mView.findViewById(R.id.tvNoTripsFound);
    }

    private void Listener(){
        swipeRefreshLayout.setOnRefreshListener(CompanyOngoingBookingFragment.this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(StaticClass.BottomProfileCompany){
            getActivity().finish();
        }
    }

    @Override
    public void companyBookingList(ArrayList<CompanyBookingListModel> bookingList) {
        myBookingModelList = bookingList;

        if(myBookingModelList.size() > 0){
            ongoingBookingAdapter = new CompanyOngoingBookingAdapter(context, myBookingModelList);
            layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rcvMyBooking.setLayoutManager(layoutManagerVertical);
            rcvMyBooking.setItemAnimator(new DefaultItemAnimator());
            rcvMyBooking.setHasFixedSize(true);
            rcvMyBooking.setAdapter(ongoingBookingAdapter);

        }else {
            rcvMyBooking.setVisibility(View.GONE);
            tvNoTripsFound.setVisibility(View.VISIBLE);
        }
    }
}
