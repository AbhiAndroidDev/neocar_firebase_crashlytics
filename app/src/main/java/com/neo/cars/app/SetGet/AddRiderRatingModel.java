package com.neo.cars.app.SetGet;

/**
 * Created by parna on 13/11/18.
 */

public class AddRiderRatingModel {

    String riview_by_entity, riview_by_id, booking_id, rider_rating, comment, rider_id, review_id;

    public String getRiview_by_entity() {
        return riview_by_entity;
    }

    public void setRiview_by_entity(String riview_by_entity) {
        this.riview_by_entity = riview_by_entity;
    }

    public String getRiview_by_id() {
        return riview_by_id;
    }

    public void setRiview_by_id(String riview_by_id) {
        this.riview_by_id = riview_by_id;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getRider_rating() {
        return rider_rating;
    }

    public void setRider_rating(String rider_rating) {
        this.rider_rating = rider_rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRider_id() {
        return rider_id;
    }

    public void setRider_id(String rider_id) {
        this.rider_id = rider_id;
    }

    public String getReview_id() {
        return review_id;
    }

    public void setReview_id(String review_id) {
        this.review_id = review_id;
    }
}
