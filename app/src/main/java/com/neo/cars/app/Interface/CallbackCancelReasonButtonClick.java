package com.neo.cars.app.Interface;

public interface CallbackCancelReasonButtonClick {

    void onCancelReasonSubmitButtonClick(String mStrPositive, String mBookingFlag, String mBookingType, String mCancelReason);
}
