package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.ViewUserProfile;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 17/7/18.
 */

public class EditProfileOTP_webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;

    private String st_mobile="",otp="", unique_id="";

    public void EditProfileOTP(Activity context, final String mobile) {

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        st_mobile=mobile;


        sharedPref = new SharedPrefUserDetails(mcontext);


        showProgressDialog();

        StringRequest loginRequest = new StringRequest(Request.Method.POST, Urlstring.get_login_otp,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("mobile_no",st_mobile);



                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        loginRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(loginRequest);

    }




    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        try {
            jobj_main = new JSONObject(response);


            Msg = jobj_main.optJSONObject("get_login_otp").optString("message");
            Status= jobj_main.optJSONObject("get_login_otp").optString("status");

            JSONObject details=jobj_main.optJSONObject("get_login_otp").optJSONObject("details");

            if (Status.equals(StaticClass.SuccessResult)) {
                st_mobile=details.optString("mobile_no");
                otp=details.optString("otp");

            }else if (Status.equals("0")){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("get_login_otp").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);

            }
        }catch(Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }

       // new CustomToast(mcontext, Msg);

        if (Status.equals(StaticClass.SuccessResult)) {
           // ((OtpVerification_Interface)mcontext).OtpVerification(st_mobile,otp);

            Intent i = new Intent(mcontext, ViewUserProfile.class);
            mcontext.startActivity(i);
            mcontext.finish();

        }
    }

}
