package com.neo.cars.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.neo.cars.app.Adapter.MyVehicleBokingListAdapter;
import com.neo.cars.app.Adapter.VehicleListAdapter;
import com.neo.cars.app.Interface.MyVehicleBooking_Interface;
import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.MyVehicleBookingModel;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.MyVehicleBooking_webservice;
import com.neo.cars.app.Webservice.UserVehicleList_Webservice;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;

/**
 * Created by parna on 12/3/18.
 */

public class MyVehicleBookingFragment extends Fragment implements MyVehicleBooking_Interface,
        SwipeRefreshLayout.OnRefreshListener{

    private View mView;
    private Context context;
    private RecyclerView rcvMyVehicle;
    private LinearLayoutManager layoutManagerVertical;
    private ArrayList<MyVehicleBookingModel> myBookingModelList = new ArrayList<>();
    private int transitionflag = StaticClass.transitionflagNext;
    private ConnectionDetector cd;
    private CustomTitilliumTextViewSemiBold tvNoVehicleFound;
    private MyVehicleBokingListAdapter myvehicleBokingListAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        try {
            mView = inflater.inflate(R.layout.fragment_my_vehicle_booking, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        Initialize();
        Listener();
        refreshList();

        return mView;
    }

    @Override
    public void onRefresh(){
        swipeRefreshLayout.setRefreshing(true);
        refreshList();
    }

    private void  refreshList(){
        //fetch user vehicle list
        if (NetWorkStatus.isNetworkAvailable(getActivity())) {
            new MyVehicleBooking_webservice().MyVehicleBooking(getActivity(), MyVehicleBookingFragment.this);

        } else {
            StaticClass.MyVehicleAddUpdate=true;
            Intent i = new Intent(getActivity(), NetworkNotAvailable.class);
            startActivity(i);
        }

        swipeRefreshLayout.setRefreshing(false);
    }

    private void Initialize(){

        context = getActivity();
        cd = new ConnectionDetector(getActivity());

        swipeRefreshLayout = mView.findViewById(R.id.refresh);
        swipeRefreshLayout.setOnRefreshListener(this);

        rcvMyVehicle = mView.findViewById(R.id.rcvMyVehicle);
        tvNoVehicleFound = mView.findViewById(R.id.tvNoVehicleFound);
    }

    private void Listener(){

    }

    @Override
    public void onMyVehicleBooking(ArrayList<MyVehicleBookingModel> arrlistVehicle) {
        myBookingModelList=arrlistVehicle;

        if(myBookingModelList.size() > 0){
            myvehicleBokingListAdapter = new MyVehicleBokingListAdapter(context,myBookingModelList);
            layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rcvMyVehicle.setLayoutManager(layoutManagerVertical);
            rcvMyVehicle.setItemAnimator(new DefaultItemAnimator());
            rcvMyVehicle.setHasFixedSize(true);
            rcvMyVehicle.setAdapter(myvehicleBokingListAdapter);

        }else{
            rcvMyVehicle.setVisibility(View.GONE);
            tvNoVehicleFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        StaticClass.BottomProfile = false;

        refreshList();
    }
}
