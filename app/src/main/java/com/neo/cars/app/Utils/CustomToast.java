package com.neo.cars.app.Utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.neo.cars.app.R;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;


/**
 * Created by rajesh on 19/8/16.
 */
public class CustomToast {

    public CustomToast(Activity mcontext, String msg) {


        final CustomAlertDialogOKCancel alertDialogYESNO = new CustomAlertDialogOKCancel(mcontext,
                msg,
                mcontext.getResources().getString(R.string.btn_ok),
                "");
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();

    }
}
