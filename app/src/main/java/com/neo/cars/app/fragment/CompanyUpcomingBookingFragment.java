package com.neo.cars.app.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neo.cars.app.Adapter.CompanyUpcomingBookingAdapter;
import com.neo.cars.app.Interface.CompanyBookingList_Interface;
import com.neo.cars.app.Interface.VehicleUnAvailability_Interface;
import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.CompanyBookingListModel;
import com.neo.cars.app.SetGet.VehicleUnAvailabilityModel;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.CompanyBookingList_Webservice;
import com.neo.cars.app.Webservice.UpcomingBookingDateWebService;
import com.neo.cars.app.calendar.CalendarAdapterA;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

public class CompanyUpcomingBookingFragment extends Fragment implements CompanyBookingList_Interface
        ,SwipeRefreshLayout.OnRefreshListener, VehicleUnAvailability_Interface {

    private View mView;
    private Context context;
    private Activity activity;
    private RecyclerView rcvMyBooking;
    private LinearLayoutManager layoutManagerVertical;
    private CompanyUpcomingBookingAdapter upcomingBookingAdapter;
    private List<CompanyBookingListModel> myBookingModelList = new ArrayList<>();
    private SwipeRefreshLayout swipeRefreshLayout;
    private CustomTitilliumTextViewSemiBold tvNoTripsFound;
    private ImageView fab;

    //Calendar used
    private RelativeLayout rel_upcoming_rl;
    private LinearLayout calendar_ll;
    boolean isClickedFB = false;

    private String strYearMonth = "";
    private ConnectionDetector cd;
    boolean upcommingCalenderFlag=false;

    //added later to replace previous calendar
    public GregorianCalendar month, itemmonth;// calendar instances.

    public CalendarAdapterA adapter;// adapter instance

    private ArrayList<String> eventList = new ArrayList<>();

    private GridView gridview;
    private TextView title;
    private ImageView previous,next;
    private int rightClickCount = 0;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        try {
            mView = inflater.inflate(R.layout.fragment_upcoming_mybooking, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        if(getArguments() != null){
            upcommingCalenderFlag = getArguments().getBoolean("upcommingFlag");
            Log.d("d", "Get upcommingFlag: "+upcommingCalenderFlag);
        }

        Initialize();
        Listener();
        refreshList();

        strYearMonth = getCurrentDate2();
        callWebService(strYearMonth);

        return mView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    //added later to replace previous calendar by apna java
    private void setCalendarView() {

        month = (GregorianCalendar) GregorianCalendar.getInstance();
        itemmonth = (GregorianCalendar) month.clone();
        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

        adapter = new CalendarAdapterA(getActivity(), month, eventList);
        gridview.setAdapter(adapter);

        adapter.refreshDays();
        adapter.notifyDataSetChanged();

        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rightClickCount--;
                disablePreviousButton(false);
                disableNextButton(false);
                setPreviousMonth();
                refreshCalendar();
                if (rightClickCount == 0){
                    disablePreviousButton(true);
                }
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rightClickCount++;
                if (rightClickCount >= 5){
                    disableNextButton(true);
                }
                disablePreviousButton(false);
                setNextMonth();
                refreshCalendar();
            }
        });

        if (rightClickCount == 0){
            disablePreviousButton(true);
        }

    }


    protected void setNextMonth() {

        if (month.get(GregorianCalendar.MONTH) == month
                .getActualMaximum(GregorianCalendar.MONTH)) {
            month.set((month.get(GregorianCalendar.YEAR) + 1),
                    month.getActualMinimum(GregorianCalendar.MONTH), 1);
        } else {
            month.set(GregorianCalendar.MONTH,
                    month.get(GregorianCalendar.MONTH) + 1);
        }

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM", Locale.US);
        strYearMonth = df.format(month.getTime());
        Log.d("strYearMonth", strYearMonth);

        callWebService(strYearMonth);


    }

    protected void setPreviousMonth() {
        if (month.get(GregorianCalendar.MONTH) == month
                .getActualMinimum(GregorianCalendar.MONTH)) {
            month.set((month.get(GregorianCalendar.YEAR) - 1),
                    month.getActualMaximum(GregorianCalendar.MONTH), 1);
        } else {
            month.set(GregorianCalendar.MONTH,
                    month.get(GregorianCalendar.MONTH) - 1);
        }

    }


    public void disablePreviousButton(boolean isConditionMatched){

        if(isConditionMatched){
            previous.setClickable(false);
            previous.setEnabled(false);
//            previous.setVisibility(View.INVISIBLE);
            previous.setBackgroundResource(R.drawable.ic_arrow_left_calender_dselect);
        }else {
            previous.setClickable(true);
            previous.setEnabled(true);
            previous.setBackgroundResource(R.drawable.ic_arrow_right_calender_select_1);
//            previous.setVisibility(View.VISIBLE);
        }

    }

    //created by miths to disable/hide next button when 4th month from current month shows

    public void disableNextButton(boolean isConditionMatched){

        if(isConditionMatched){
            next.setClickable(false);
            next.setEnabled(false);
            next.setBackgroundResource(R.drawable.ic_arrow_right_calender_dselect);
//            next.setVisibility(View.INVISIBLE);
        }else {
            next.setClickable(true);
            next.setEnabled(true);
            next.setBackgroundResource(R.drawable.ic_arrow_right_calender_select);
//            next.setVisibility(View.VISIBLE);
        }

    }

    public void refreshCalendar() {
        adapter = new CalendarAdapterA(getActivity(), month, eventList);
        gridview.setAdapter(adapter);

        adapter.refreshDays();
        adapter.notifyDataSetChanged();

        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));

    }

    private void callWebService(String strYearMonth) {

        if (NetWorkStatus.isNetworkAvailable(getActivity())) {
            new UpcomingBookingDateWebService().getUpcomingDate(getContext(), getActivity(), strYearMonth, CompanyUpcomingBookingFragment.this);
        } else {
            Intent i = new Intent(getActivity(), NetworkNotAvailable.class);
            startActivity(i);
        }
    }

    @Override
    public void onRefresh(){
        swipeRefreshLayout.setRefreshing(true);
        refreshList();
    }

    private void  refreshList(){
        if (NetWorkStatus.isNetworkAvailable(getActivity())) {
            new CompanyBookingList_Webservice().CompanyBookingList(getActivity(),StaticClass.MyVehicleUpcoming, CompanyUpcomingBookingFragment.this);

        } else {
            StaticClass.MyBookingListFlag=true;
            Intent i = new Intent(getActivity(), NetworkNotAvailable.class);
            startActivity(i);
        }

        swipeRefreshLayout.setRefreshing(false);

    }

    private void Initialize() {

        activity = getActivity();
        swipeRefreshLayout = mView.findViewById(R.id.refresh);
        rcvMyBooking = mView.findViewById(R.id.rcvMyBooking);
        tvNoTripsFound = mView.findViewById(R.id.tvNoTripsFound);

        //added later for calendar
        fab = mView.findViewById(R.id.fab);
        rel_upcoming_rl = mView.findViewById(R.id.rel_upcoming_rl);
        calendar_ll = mView.findViewById(R.id.calendar_ll);

        cd = new ConnectionDetector(getActivity());
        context = getContext();

        gridview = mView.findViewById(R.id.calendar_grid);
        title = mView.findViewById(R.id.calendar_date_display);
        previous = mView.findViewById(R.id.calendar_prev_button);
        next = mView.findViewById(R.id.calendar_next_button);

        if(upcommingCalenderFlag) {
            rel_upcoming_rl.setVisibility(View.GONE);
            calendar_ll.setVisibility(View.VISIBLE);
            fab.setVisibility(View.VISIBLE);
            isClickedFB = true;
            fab.setBackgroundResource(R.drawable.list_icon);
        }

        setCalendarView();

    }

    private void Listener(){
        swipeRefreshLayout.setOnRefreshListener(CompanyUpcomingBookingFragment.this);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isClickedFB) {
                    rel_upcoming_rl.setVisibility(View.GONE);
                    calendar_ll.setVisibility(View.VISIBLE);
                    isClickedFB = true;
                    fab.setBackgroundResource(R.drawable.list_icon);
                    refreshList();
                }else {
                    rel_upcoming_rl.setVisibility(View.VISIBLE);
                    calendar_ll.setVisibility(View.GONE);
                    isClickedFB = false;
                    fab.setBackgroundResource(R.drawable.calendar);
                    refreshList();
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();
    }

    public String getCurrentDate2() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM");
        //strYearMonth used to send as params in webservice
        strYearMonth = df.format(c);
        return strYearMonth;

    }

    @Override
    public void unAvailableDate(ArrayList<VehicleUnAvailabilityModel> arrListVehicleUnAvailabilityModel) {

        for (int i = 0; i < arrListVehicleUnAvailabilityModel.size(); i++){
            String strUnAvailableDate = arrListVehicleUnAvailabilityModel.get(i).getUnavailable_date();
            setUnAvailableDateOnCalendar(strUnAvailableDate);
        }

        adapter = new CalendarAdapterA(getActivity(), month, eventList);
        gridview.setAdapter(adapter);

    }

    public void setUnAvailableDateOnCalendar(String strUnAvailableDate){

            Log.d("strUnAvailableDate", strUnAvailableDate);
            eventList.add(strUnAvailableDate);

    }

    @Override
    public void companyBookingList(ArrayList<CompanyBookingListModel> bookingList) {
        myBookingModelList = bookingList;
        System.out.println("****UpcomingBookingFragment***"+bookingList.size());

        if(myBookingModelList.size() > 0){
            fab.setVisibility(View.VISIBLE);
            upcomingBookingAdapter = new CompanyUpcomingBookingAdapter(context,myBookingModelList);
            layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rcvMyBooking.setLayoutManager(layoutManagerVertical);
            rcvMyBooking.setItemAnimator(new DefaultItemAnimator());
            rcvMyBooking.setHasFixedSize(true);
            rcvMyBooking.setAdapter(upcomingBookingAdapter);

        }else {
            rcvMyBooking.setVisibility(View.GONE);
            tvNoTripsFound.setVisibility(View.VISIBLE);

            if (isClickedFB){
                fab.setVisibility(View.VISIBLE);
            }else {
                fab.setVisibility(View.GONE);
            }
        }
    }
}
