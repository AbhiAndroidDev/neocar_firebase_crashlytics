package com.neo.cars.app.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.neo.cars.app.Interface.CallBackButtonClick;
import com.neo.cars.app.R;
import com.neo.cars.app.Utils.StaticClass;

/**
 * Created by joydeep on 2/2/18.
 */

public class CenterDialogPositiveNegative {

    private Context mContext;
    private Activity mActivity;
    private String mStrMsg, mStrNegative, mStrPositive;

    public CenterDialogPositiveNegative(Context context, Activity activity, String strMsg, String strNegative, String strPositive) {
        this.mContext = context;
        this.mActivity = activity;

        this.mStrMsg = strMsg;
        this.mStrNegative = strNegative;
        this.mStrPositive = strPositive;

        // call bottom sheet dialog
        callBottomSheetDialog();
    }

    private void callBottomSheetDialog() {
        final Dialog mBottomSheetDialog = new Dialog(mContext);
        View sheetView = mActivity.getLayoutInflater().inflate(R.layout.bottom_sheet_dialog_positive_negative, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        TextView tvMsg;
        Button btNegative, btPositive;

        tvMsg = sheetView.findViewById(R.id.tvMsg);
        btNegative = sheetView.findViewById(R.id.btNegative);
        btPositive = sheetView.findViewById(R.id.btPositive);

        tvMsg.setText(mStrMsg);
        btNegative.setText(mStrNegative);
        btPositive.setText(mStrPositive);

        btNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                ((CallBackButtonClick)mActivity).onButtonClick(StaticClass.alertyes);
            }
        });

        btPositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
                ((CallBackButtonClick)mActivity).onButtonClick(StaticClass.alertno);
            }
        });
    }

}
