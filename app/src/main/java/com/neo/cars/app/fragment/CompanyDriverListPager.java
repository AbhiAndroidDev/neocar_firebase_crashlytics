package com.neo.cars.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by parna on 12/11/18.
 */

public class CompanyDriverListPager extends FragmentStatePagerAdapter {

    private int tabCount;
    private String driverId="";
    private Bundle bundle=null;

    public CompanyDriverListPager(FragmentManager fm, int tabCount, String driverId) {
        super(fm);
        this.tabCount = tabCount;
        this.driverId = driverId;
    }

    public CompanyDriverListPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                MoreDriverInfoFragment tab1 = new MoreDriverInfoFragment();
                bundle = new Bundle();
                bundle.putString("driverId", driverId);
                tab1.setArguments(bundle);
                return tab1;

            case 1:
                DriverAvailabilityInfofragment tab2 = new DriverAvailabilityInfofragment();
                bundle = new Bundle();
                bundle.putString("driverId", driverId);
                tab2.setArguments(bundle);
                return tab2;

            default:
                return null;

        }

    }
    @Override
    public int getCount() {
        return tabCount;
    }
}
