package com.neo.cars.app;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.GetApiAccessToken_Webservice;
import com.neo.cars.app.db.DataInsert;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;

//import com.facebook.CallbackManager;
//import com.facebook.FacebookCallback;
//import com.facebook.FacebookException;
//import com.facebook.FacebookSdk;
//import com.facebook.GraphRequest;
//import com.facebook.GraphResponse;
//import com.facebook.login.LoginManager;
//import com.facebook.login.LoginResult;
//import com.twitter.sdk.android.core.Callback;
//import com.twitter.sdk.android.core.Result;
//import com.twitter.sdk.android.core.TwitterException;
//import com.twitter.sdk.android.core.TwitterSession;
//import com.twitter.sdk.android.core.identity.TwitterAuthClient;


/**
 * Created by parna on 2/3/18.
 */

public class LoginStepOneActivity extends RootActivity implements GoogleApiClient.OnConnectionFailedListener {

    private Button btnLogin;
    private CustomButtonTitilliumSemibold btnCreateAccount;
    private CustomTextviewTitilliumWebRegular tvCreateCompanyAccount;
    private TextView tvByLoggingIntoText_termsservices,tvByLoggingIntoText_privacypolicy;
    private ImageButton btnFacebookLogin, btnTwitterLogin, btnGooglePlusLogin;
    //private CheckBox cb_terms;
//    private CallbackManager callbackManager;

    private String strAccessToken, strPhNo="", strEmail="", strName="", strFname="", strLname="", strId="", fbProfilePicUrl="", strRegId="", gplusPersonName = "", gplusPersonPhotoUrl = "",gplusEmail = "", gplusId = "";
//    private GraphRequest request;
    private HashMap<String, String> hashMap;
    private Bundle parameters;
    private GoogleApiClient mGoogleApiClient;
    private Context context;
    private SharedPrefUserDetails sharedPref;

//    private TwitterAuthClient mTwitterAuthClient;
    private int transitionflag = StaticClass.transitionflagNext;

    private static final int RC_SIGN_IN = 007;

    //request parameter
    private static final String TAG_LOGIN_TYPE = "login_type";
    private static final String TAG_FACEBOOK_ID = "facebook_id";
    private static final String TAG_GPLUS_ID = "gplus_id";
    private static final String TAG_USER_PROFILE_PHOTO = "user_profile_photo";
    private static final String TAG_DEVICE_REG_ID = "device_reg_id";
    private static final String TAG_DEVICE_TYPE = "device_type";

    //response parameter
    private static final String TAG_EMAIL = "email";
    private static final String TAG_NAME = "name";

    private boolean isLocationEnableCancelled = false, isPermissionDenied  = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_step_one);

        context = LoginStepOneActivity.this;
        sharedPref = new SharedPrefUserDetails(LoginStepOneActivity.this);

//        mTwitterAuthClient = new TwitterAuthClient();

        new AnalyticsClass(LoginStepOneActivity.this);

        Initialize();
        Listener();
        new DataInsert().isDataInsert(LoginStepOneActivity.this);

        callAllAppPermission();

        //Google Plus Login
//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();

//        mGoogleApiClient = new GoogleApiClient.Builder(this)
//                .enableAutoManage(this, this)
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();

        //Facebook Login
//        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                strAccessToken = loginResult.getAccessToken().getToken();
//                request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new   GraphRequest.GraphJSONObjectCallback() {
//
//                    @Override
//                    public void onCompleted(JSONObject object, GraphResponse graphResponse) {
//                        Log.e("FACEBOOK JSON", object.toString());
//
//                        try {
//                            strEmail = object.optString("email");
//                            strFname = object.optString("first_name");
//                            strLname = object.optString("last_name");
//
//                            strId = object.optString("id");
//                            if (object.has("picture")) {
//                                fbProfilePicUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");
//                                fbProfilePicUrl = fbProfilePicUrl.replace("\\", "");
//                                Log.e("FACEBOOK PicUrl : ", fbProfilePicUrl);
//                            }
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//
//                        hashMap = new HashMap<String, String>();
//                        hashMap.put(TAG_LOGIN_TYPE, "FB");
//                        hashMap.put(TAG_EMAIL, strEmail);
//                        hashMap.put(TAG_NAME, strFname + " " + strLname);
//                        hashMap.put(TAG_FACEBOOK_ID, strId);
//                        hashMap.put(TAG_USER_PROFILE_PHOTO, fbProfilePicUrl);
//                        hashMap.put(TAG_DEVICE_REG_ID, strRegId);
//                        hashMap.put(TAG_DEVICE_TYPE, "A");
//
//                        //** call the webservice to fetch login details and navigate to the required page **//
//                        // new LoginUsingSocialMedia().sendUserData(hashMap);
//
//                        new SocialLogin_Webservice().SocialLogin(LoginStepOneActivity.this, StaticClass.FacebookLogin, strId, strEmail, "", strFname, fbProfilePicUrl);
//
//                    }
//                });
//                parameters = new Bundle();
//                parameters.putString("fields", "email,first_name,last_name,id,picture.type(large)");
//                request.setParameters(parameters);
//                request.executeAsync();
//
//                Log.e("FACEBOOK RESULT", loginResult.toString());
//
//            }
//
//
//            @Override
//            public void onCancel() {
//                new CustomToast(LoginStepOneActivity.this, getResources().getString(R.string.error_login_cancel));
//            }
//
//            @Override
//            public void onError(FacebookException exception) {
//                Log.e("FACEBOOK EXCEPTION", exception.toString());
//                new CustomToast(LoginStepOneActivity.this, exception.getMessage());
//            }
//        });
    }

    private void callAllAppPermission() {

        checkAllPermission();

    }

    private void checkAllPermission() {
        Dexter.withActivity(LoginStepOneActivity.this).withPermissions(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener()
                {
                    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
                    @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if(report.areAllPermissionsGranted()){

                            Log.d("AllPermission","Enable");

                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                            showAlert();
                        }
                    }
                    @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                    {/* ... */

                        token.continuePermissionRequest();
                    }
                }).check();
    }


    public void showAlertLocation(){

        if(!StaticClass.isLocationPermissionCanceled) {


            final android.support.v7.app.AlertDialog.Builder alertDialogBuilder = new android.support.v7.app.AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Permissions Required")
                    .setMessage("You have forcefully denied some of the required permissions " +
                            "for this action. Please open settings, go to permissions and allow them.")
                    .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                    Uri.fromParts("package", getPackageName(), null));
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    })
                    .setCancelable(false)
                    .create()
                    .show();
        }
    }

    private void Initialize() {
        btnLogin = findViewById(R.id.btnLogin);
        btnCreateAccount = findViewById(R.id.btnCreateAccount);
        tvByLoggingIntoText_termsservices = findViewById(R.id.tvByLoggingIntoText_termsservices);
       // cb_terms= findViewById(R.id.cb_terms);
        tvByLoggingIntoText_privacypolicy= findViewById(R.id.tvByLoggingIntoText_privacypolicy);

        tvCreateCompanyAccount = findViewById(R.id.tvCreateCompanyAccount);
        tvCreateCompanyAccount.setText(Html.fromHtml("<u>"+getResources().getString(R.string.createCompanyAccount)+"</u>"));

        btnFacebookLogin = findViewById(R.id.btnFacebookLogin);
        btnTwitterLogin = findViewById(R.id.btnTwitterLogin);
        btnGooglePlusLogin = findViewById(R.id.btnGooglePlusLogin);

        //Facebook keyhash fetch
//        FacebookSdk.sdkInitialize(getApplicationContext());
//        callbackManager = CallbackManager.Factory.create();
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.neo.cars.app", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void Listener(){
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent loginIntent = new Intent(LoginStepOneActivity.this, LoginStepTwoActivity.class);
                startActivity(loginIntent);
                finish();

               /* if(cb_terms.isChecked()) {
                    Intent loginIntent = new Intent(LoginStepOneActivity.this, LoginStepTwoActivity.class);
                    startActivity(loginIntent);
                    finish();
                }else{
                    new CustomToast(LoginStepOneActivity.this, getResources().getString(R.string.Pleaseaccepttheprivacypolicytoprocess));

                }*/
            }
        });

        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent loginIntent = new Intent(LoginStepOneActivity.this, RegistrationActivity.class);
                startActivity(loginIntent);
                finish();
//                PermissionForUserRegistration();
               /* if(cb_terms.isChecked()) {
                Intent loginIntent = new Intent(LoginStepOneActivity.this, RegistrationActivity.class);
                startActivity(loginIntent);
                finish();
                }else{
                    new CustomToast(LoginStepOneActivity.this, getResources().getString(R.string.Pleaseaccepttheprivacypolicytoprocess));
                }*/
            }
        });

        tvCreateCompanyAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent loginIntent = new Intent(LoginStepOneActivity.this, CompanyRegistrationActivity.class);
                startActivity(loginIntent);
//                PermissionForCompRegistration();
               /* if(cb_terms.isChecked()) {
                Intent loginIntent = new Intent(LoginStepOneActivity.this, CompanyRegistrationActivity.class);
                startActivity(loginIntent);
                }else{
                    new CustomToast(LoginStepOneActivity.this, getResources().getString(R.string.Pleaseaccepttheprivacypolicytoprocess));
                }*/
            }
        });


        tvByLoggingIntoText_termsservices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent termsIntent = new Intent(LoginStepOneActivity.this, CmsPageActivity.class);
                termsIntent.putExtra("cms", StaticClass.TermsCondition);
                startActivity(termsIntent);
            }
        });


        tvByLoggingIntoText_privacypolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent termsIntent = new Intent(LoginStepOneActivity.this, CmsPageActivity.class);
                termsIntent.putExtra("cms", StaticClass.PrivacyPolicy);
                startActivity(termsIntent);
            }
        });

        //Facebook login
//        btnFacebookLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (NetWorkStatus.isNetworkAvailable(LoginStepOneActivity.this)){
//                    if (isPackageInstalled(LoginStepOneActivity.this, "com.facebook")) {
//                        LoginManager.getInstance().logInWithReadPermissions(LoginStepOneActivity.this, Arrays.asList("public_profile", "email"));
//                    }else {
//
//                        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(LoginStepOneActivity.this,
//                                "Please install Facebook App",
//                                getResources().getString(R.string.btn_ok),
//                                "");
//                        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                alertDialogYESNO.dismiss();
//
//                                //to open play store twitter link if twitter app is not installed
//
//                                final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//                                try {
//                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.facebook.katana")));
//                                } catch (android.content.ActivityNotFoundException anfe) {
//                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.facebook.katana")));
//                                }
//                            }
//                        });
//
//                        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
//                            @Override
//                            public void onClick(View v) {
//                                alertDialogYESNO.dismiss();
//                            }
//                        });
//
//                        alertDialogYESNO.show();
//                    }
//                }else {
//                    startActivity(new Intent(LoginStepOneActivity.this, NetworkNotAvailable.class));
//                }
//
//            }
//        });


        //Twitter login
//        btnTwitterLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (isPackageInstalled(LoginStepOneActivity.this, "com.twitter")) {
//                    mTwitterAuthClient.authorize(LoginStepOneActivity.this, new Callback<TwitterSession>() {
//                        @Override
//                        public void success(final Result<TwitterSession> twitterSessionResult) {
//                            // Success
//                            //  requestEmailAddress(getApplicationContext(), twitterSessionResult.data);
//
//                            new TwitterAuthClient().requestEmail(twitterSessionResult.data, new Callback<String>() {
//                                @Override
//                                public void success(Result<String> result) {
//                                    Log.d("d", "Twitter data fetch: " + result.data);
//
//                                    new PrintClass("Email: " + result.data +
//                                            "\nId: " + twitterSessionResult.data.getId() +
//                                            "\nUsername: " + twitterSessionResult.data.getUserName() +
//                                            "\nAuthToken: " + twitterSessionResult.data.getAuthToken() +
//                                            "\nUserId: " + twitterSessionResult.data.getUserId());
//
//                                    strId = Long.toString(twitterSessionResult.data.getId());
//                                    strEmail = result.data;
//                                    strName = twitterSessionResult.data.getUserName();
//                                    Log.d("d", "Twitter name: " + strName);
//
//                                    new SocialLogin_Webservice().SocialLogin(LoginStepOneActivity.this, StaticClass.TwitterLogin, strId, strEmail, "", strName, "");
//                                }
//
//                                @Override
//                                public void failure(TwitterException exception) {
//                                    new CustomToast(LoginStepOneActivity.this, exception.getMessage());
//                                }
//                            });
//
//                        }
//
//                        @Override
//                        public void failure(TwitterException exception) {
//                            exception.printStackTrace();
//                        }
//                    });
//                }else {
//
////                    new CustomToast(LoginStepOneActivity.this, "Please install Twitter App");
//
//                    final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(LoginStepOneActivity.this,
//                            "Please install Twitter App",
//                            getResources().getString(R.string.btn_ok),
//                            "");
//                    alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alertDialogYESNO.dismiss();
//
//                            //to open play store twitter link if twitter app is not installed
//
//                            final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//                            try {
//                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.twitter.android")));
//                            } catch (android.content.ActivityNotFoundException anfe) {
//                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "com.twitter.android")));
//                            }
//
//
//                        }
//                    });
//
//                    alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            alertDialogYESNO.dismiss();
//                        }
//                    });
//
//                    alertDialogYESNO.show();
//                }
//            }
//        });

        //Google plus login
//        btnGooglePlusLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (NetWorkStatus.isNetworkAvailable(LoginStepOneActivity.this)){
////                    if (isPackageInstalled(context, "com.google.android")){
//                    signIn();
////                    }
//
//                }else{
//                    startActivity(new Intent(LoginStepOneActivity.this, NetworkNotAvailable.class));
//                }
//            }
//        });
    }

//    private boolean isPackageInstalled(Context context, String packagename) {
//
//        PackageManager pkManager = context.getPackageManager();
//        List<ApplicationInfo> packages = pkManager.getInstalledApplications(PackageManager.GET_META_DATA);
//
//        for(ApplicationInfo info : packages){
//            Log.d("**getPkgInfo**", info.packageName);
//            if (info.packageName.contains(packagename)){
//                return true;
//            }
//        }
//        return false;
//
//    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(LoginStepOneActivity.this, StaticClass.transitionflagNext);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new OnPauseSlider(LoginStepOneActivity.this, StaticClass.transitionflagBack);

    }

//    @Override
//    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//
//    }


    // twitter profile info
//    private static void requestEmailAddress(final Context context, TwitterSession session) {
//        new TwitterAuthClient().requestEmail(session, new Callback<String>() {
//            @Override
//            public void success(Result<String> result) {
//                Toast.makeText(context, result.data, Toast.LENGTH_SHORT).show();
//                Log.d("d", "Twitter data fetch: "+ result.data);
//            }
//
//            @Override
//            public void failure(TwitterException exception) {
//                Toast.makeText(context, exception.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

//    private void signIn() {
//        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//        startActivityForResult(signInIntent, RC_SIGN_IN);
//    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (callbackManager.onActivityResult(requestCode, resultCode, data)) {
//            return;
//        }

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
//        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);
//
//        }else {
//
//            System.out.println("requestCode****1******" + requestCode);
//
//            callbackManager.onActivityResult(requestCode, resultCode, data);
//
//            // Twitter result
//            mTwitterAuthClient.onActivityResult(requestCode, resultCode, data);
//
//        }
    }


//    private void handleSignInResult(GoogleSignInResult result) {
//        Log.e("GPLUS RESULT", result.toString());
//        if (result.isSuccess()) {
//            // Signed in successfully, show authenticated UI.
//            GoogleSignInAccount acct = result.getSignInAccount();
//
//            gplusPersonName = acct.getDisplayName();
//            Log.e("gplusPersonName : ", "" + gplusPersonName);
//            if (acct.getPhotoUrl() != null)
//                gplusPersonPhotoUrl = acct.getPhotoUrl().toString();
//            gplusPersonPhotoUrl = gplusPersonPhotoUrl.replace("\\", "");
//            Log.e("gplusPersonPhotoUrl : ", "" + gplusPersonPhotoUrl);
//            gplusEmail = acct.getEmail();
//            gplusId = acct.getId();
//
//            hashMap = new HashMap<String, String>();
//            hashMap.put(TAG_LOGIN_TYPE, "GP");
//            hashMap.put(TAG_EMAIL, gplusEmail);
//            hashMap.put(TAG_NAME, gplusPersonName);
//            hashMap.put(TAG_GPLUS_ID, gplusId);
//            hashMap.put(TAG_USER_PROFILE_PHOTO, gplusPersonPhotoUrl);
//            hashMap.put(TAG_DEVICE_REG_ID, strRegId);
//            hashMap.put(TAG_DEVICE_TYPE, "A");
//
//            // new LoginUsingSocialMedia().sendUserData(hashMap);
//            new SocialLogin_Webservice().SocialLogin(LoginStepOneActivity.this, StaticClass.GoogleLogin, gplusId, gplusEmail, "", gplusPersonName, gplusPersonPhotoUrl );
//
//        } else {
//            new CustomToast(LoginStepOneActivity.this, getResources().getString(R.string.error_login_failed));
//        }
//    }

    @Override
    protected void onResume() {
        super.onResume();

        if (NetWorkStatus.isNetworkAvailable(LoginStepOneActivity.this)) {
            if(sharedPref.getKEY_Access_Token().equals("")) {
                new GetApiAccessToken_Webservice().GetApiAccessToken(LoginStepOneActivity.this);
            }
        } else {
            startActivity(new Intent(LoginStepOneActivity.this, NetworkNotAvailable.class));

        }

        if (sharedPref.getLoginStatus()){
            finish();
        }
    }


    private void showAlertToEnable() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(context);
        dialog.setMessage("Please enable your device location.");
        dialog.setCancelable(false);
        dialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                context.startActivity(myIntent);


                //get gps
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                // TODO Auto-generated method stub
//                    finish();
                isLocationEnableCancelled = true;
            }
        });
        dialog.show();
    }
}
