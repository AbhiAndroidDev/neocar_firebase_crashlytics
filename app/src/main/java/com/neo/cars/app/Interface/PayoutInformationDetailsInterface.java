package com.neo.cars.app.Interface;

/**
 * Created by parna on 31/5/18.
 */

public interface PayoutInformationDetailsInterface {

    public void onPayoutInformationDetails(String jsonObjDetails);

}
