package com.neo.cars.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.bumptech.glide.Glide;
import com.neo.cars.app.BookACarActivity;
import com.neo.cars.app.Interface.ExploreViewMore_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.ExploreModel;
import com.neo.cars.app.SetGet.ExploreVehicleListModel;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by parna on 13/3/18.
 */

public class ExploreCarAdapter extends RecyclerView.Adapter<ExploreCarAdapter.MyViewHolder>  {

    private List<ExploreModel> exploreModelList;
    private Context mContext;
    private ArrayList<ExploreVehicleListModel> arr_listOfVehicle;
    private String strStartDate = "", strDuration = "", strPickUpTime = "";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CustomTextviewTitilliumWebRegular tvExploreCityName;
        CustomButtonTitilliumSemibold btnShowMore;
        LinearLayout llCar, ll_view_all;
        ImageButton btnViewAll;


        public MyViewHolder(View view) {
            super(view);
            tvExploreCityName = view.findViewById(R.id.tvExploreCityName);
            btnShowMore = view.findViewById(R.id.btnShowMore);
            llCar = view.findViewById(R.id.llCar);
            ll_view_all = view.findViewById(R.id.ll_view_all);
            btnViewAll = view.findViewById(R.id.btnViewAll);

        }
    }

    public ExploreCarAdapter(Context mContext, List<ExploreModel> myExploreModelList, String strStartDate, String strDuration, String strPickUpTime) {
        exploreModelList = myExploreModelList;
        this.mContext = mContext;
        this.strStartDate = strStartDate;
        this.strDuration = strDuration;
        this.strPickUpTime = strPickUpTime;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.explore_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,final int position) {

        //set text values
        holder.tvExploreCityName.setText(exploreModelList.get(position).getCity_name());

        String strShowMore = exploreModelList.get(position).getHas_more_vehicle();
        Log.d("d", "Show more btn::"+strShowMore);
        if ("Y".equals(exploreModelList.get(position).getHas_more_vehicle())){
            holder.btnShowMore.setVisibility(View.GONE);

        }else if ("N".equals(exploreModelList.get(position).getHas_more_vehicle())){
            holder.btnShowMore.setVisibility(View.GONE);
        }

        arr_listOfVehicle= exploreModelList.get(position).getArr_exploreVehicleListModel();
        int listSize = 0;
        if(arr_listOfVehicle.size() >2){
            listSize=2;
        }else{
            listSize=arr_listOfVehicle.size();
        }

        holder.llCar.removeAllViews();
        for (int i=0;i<listSize;i++){
            holder.llCar.addView(addVehicalImage(i, arr_listOfVehicle));
        }

        holder.ll_view_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              String  CityId=exploreModelList.get(position).getCity_id();
                String  CityName=exploreModelList.get(position).getCity_name();
                Log.d("d", "**City Name**"+CityName);
                Log.d("d", "**City Id**"+CityId);
                ((ExploreViewMore_Interface)mContext).onExploreViewMore(CityId,CityName);
            }
        });
    }

    @Override
    public int getItemCount() {
        return exploreModelList.size();
    }

    private View addVehicalImage(final int pos, final ArrayList<ExploreVehicleListModel> listOfVehical ){
        final View view = LayoutInflater.from(mContext).inflate(R.layout.activity_explore_car_image,null);
        final ImageView ivCarOne = view.findViewById(R.id.ivCarOne);
        CustomTextviewTitilliumBold tvCarDescription = view.findViewById(R.id.tvCarDescription);
        final RelativeLayout rlCarLayout = view.findViewById(R.id.rlCarLayout);


        Transformation transformation1 = new Transformation() {

            @Override
            public Bitmap transform(Bitmap source) {
                Bitmap result=null;
                try {
                    int targetWidth = 0;
                    int targetHeight = 0;
                    try {
                        targetWidth = ivCarOne.getWidth();
                        if(targetWidth<1){
                            targetWidth=100;
                        }
                        double aspectRatio = (double) source.getHeight()/(double) source.getWidth() ;
                        targetHeight = (int) (targetWidth * aspectRatio);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if(targetHeight<1){
                        targetHeight=100;
                    }

                    result= Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                    if (result != source) {
                        source.recycle();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return result;
            }

            @Override
            public String key() {
                return "transformation" + " desiredWidth";
            }
        };


        if (!TextUtils.isEmpty(listOfVehical.get(pos).getDescription())){
            tvCarDescription.setText(listOfVehical.get(pos).getDescription());
        }
        if(!TextUtils.isEmpty(listOfVehical.get(pos).getVehicle_image())){

            Glide.with(mContext)
                    .load(listOfVehical.get(pos).getVehicle_image())
                    .into(ivCarOne);

//            Picasso.get()
//                    .load(listOfVehical.get(pos).getVehicle_image())
////                    .memoryPolicy(MemoryPolicy.NO_CACHE, MemoryPolicy.NO_STORE)
////                    .networkPolicy(NetworkPolicy.NO_CACHE, NetworkPolicy.NO_STORE)
//                    .into(ivCarOne);
        }

        rlCarLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //move to car details page for book now option
                Log.d("d", "**** Tap on rlCarLayout *****");
                Intent bookacarintent = new Intent(mContext, BookACarActivity.class);
                bookacarintent.putExtra("VehicleId", listOfVehical.get(pos).getId());
                bookacarintent.putExtra("IsComingFrom", StaticClass.ExploreCar);

                bookacarintent.putExtra("FromDate", strStartDate );
                Log.d("strStartDate*", strStartDate);
                bookacarintent.putExtra("Duration", strDuration );
                Log.d("strDuration*", strDuration);
                bookacarintent.putExtra("PickUpTime", strPickUpTime );
                Log.d("strPickUpTime*", strPickUpTime);
                Log.d("d", "Vehicle id:: " + listOfVehical.get(pos).getId());
                mContext.startActivity(bookacarintent);
            }
        });


        return view;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


}
