package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.VehicleListInterface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleDetailsModel;
import com.neo.cars.app.SetGet.VehicleListModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchedVehicleListApi {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="", strTotalRecord="", strPageLimit="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private Gson gson;

    private ArrayList<VehicleDetailsModel> arr_VehicleDetailsModel ;
    private ArrayList<VehicleListModel> arrListvehicleModel ;
    private UserLoginDetailsModel UserLoginDetails;
    private int transitionflag = StaticClass.transitionflagNext;

    public void SearchedVehicleList(Activity context, final String device_id, final String state_id, final String city_id, final String user_id,
                                   final String start_date, final String duration, final String pickup_time, final String trip_cost_range,
                                   final String vehicle_type, final String vehicle_brand, final String passenger_capacity, final String luggage_capacity,
                                   final String overtime_available, final String sort_by, final String search_flag, final String page_no,
                                   final String is_luxury_status){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);
        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        showProgressDialog();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        StringRequest exploreVehicleListREquest = new StringRequest(Request.Method.POST, Urlstring.searched_vehicle_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response***", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                String userId = UserLoginDetails.getId();

                if (device_id != null) {
                    params.put("device_id", device_id);
                }

                if(state_id != null){
                    params.put("state_id", state_id);
                }else{
                    params.put("state_id", "");
                }

                if(city_id != null){
                    params.put("city_id", city_id);
                }else{
                    params.put("city_id", "");
                }

//                if (userId != null) {
                    params.put("user_id", UserLoginDetails.getId());
//                }


                if(start_date != null){
                    params.put("start_date", start_date);
                }
                else {
                    params.put("start_date", "");
                }

                if(duration != null){
                    params.put("duration", duration);
                }else{
                    params.put("duration", "");
                }

                if(pickup_time != null){
                    params.put("pickup_time", pickup_time);
                }else{
                    params.put("pickup_time", "");
                }


                if(trip_cost_range != null){
                    params.put("trip_cost_range", trip_cost_range);
                }else {
                    params.put("trip_cost_range", "");
                }

                if(vehicle_type != null){
                    params.put("vehicle_type", vehicle_type);
                }else{
                    params.put("vehicle_type", "");
                }

                if(vehicle_brand != null){
                    params.put("vehicle_brand", vehicle_brand);
                }else{
                    params.put("vehicle_brand", "");
                }

                if(passenger_capacity != null){
                    params.put("passenger_capacity", passenger_capacity);
                }else{
                    params.put("passenger_capacity", "");
                }

                if (luggage_capacity != null){
                    params.put("luggage_capacity", luggage_capacity);
                }else {
                    params.put("luggage_capacity", "");
                }

                if(overtime_available != null){
                    params.put("overtime_available", overtime_available);
                }else{
                    params.put("overtime_available", "");
                }

                if(sort_by != null){
                    params.put("sort_by", sort_by);
                }else{
                    params.put("sort_by", "");
                }

                if(search_flag != null){
                    params.put("search_flag", search_flag);
                }else{
                    params.put("search_flag", "");
                }

                if(!TextUtils.isEmpty(page_no)){
                    params.put("page_no", page_no);
                }else{
                    params.put("page_no", "");
                }

                if(is_luxury_status != null){
                    params.put("is_luxury_status", is_luxury_status);
                }else{
                    params.put("is_luxury_status", "");
                }


//                if(strCityId == null || strCityId.equalsIgnoreCase("")){
//                    params.put("page_no", Integer.toString(pageCount));
//                }else{
//                    params.put("city_page_no", Integer.toString(pageCount));
//                }


                new PrintClass("explore vehicle list params******getParams***"+params);
                return params;
            }



            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        exploreVehicleListREquest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(exploreVehicleListREquest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){


        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("vehicle_list").optString("user_deleted");
            Msg = jobj_main.optJSONObject("vehicle_list").optString("message");
            Status= jobj_main.optJSONObject("vehicle_list").optString("status");
            strTotalRecord = jobj_main.optJSONObject("vehicle_list").optString("total_record");
            strPageLimit = jobj_main.optJSONObject("vehicle_list").optString("page_limit");

            JSONObject details = jobj_main.optJSONObject("vehicle_list").optJSONObject("details");

            JSONArray vehicle = jobj_main.optJSONObject("vehicle_list").optJSONObject("details").optJSONArray("vehicle");

            if (Status.equals(StaticClass.SuccessResult)){

                arr_VehicleDetailsModel = new ArrayList<>();
                arrListvehicleModel=new ArrayList<>();

                VehicleDetailsModel vehicleDetailsModel = new VehicleDetailsModel();

                vehicleDetailsModel.setCity_id(details.optString("city_id"));
                vehicleDetailsModel.setCity_name(details.optString("city_name"));
                vehicleDetailsModel.setState_id(details.optString("state_id"));
                vehicleDetailsModel.setState_name(details.optString("state_name"));

                for (int i=0; i < vehicle.length(); i++){

                    JSONObject jobjDetails = vehicle.getJSONObject(i);

                    VehicleListModel vehicleListModel = new VehicleListModel();
                    vehicleListModel.setVehicle_id(jobjDetails.optString("vehicle_id"));
                    vehicleListModel.setTotal_trip_cost(jobjDetails.optString("total_trip_cost"));
                    vehicleListModel.setVehicle_make_id(jobjDetails.optString("vehicle_make_id"));
                    vehicleListModel.setVehicle_make_name(jobjDetails.optString("vehicle_make_name"));
                    vehicleListModel.setVehicle_model_id(jobjDetails.optString("vehicle_model_id"));
                    vehicleListModel.setVehicle_model_name(jobjDetails.optString("vehicle_model_name"));
                    vehicleListModel.setVehicle_type_id(jobjDetails.optString("vehicle_type_id"));
                    vehicleListModel.setVehicle_type_name(jobjDetails.optString("vehicle_type_name"));
                    vehicleListModel.setIs_luxury(jobjDetails.optString("is_luxury"));
                    vehicleListModel.setMax_luggage(jobjDetails.optString("max_luggage"));
                    vehicleListModel.setMax_passenger(jobjDetails.optString("max_passenger"));
                    vehicleListModel.setOvertime_available(jobjDetails.optString("overtime_available"));
                    vehicleListModel.setAvg_vehicle_rating(jobjDetails.optString("neo_rating"));
                    vehicleListModel.setImage_file(jobjDetails.optString("image_file"));

                    arrListvehicleModel.add(vehicleListModel);
                }
                vehicleDetailsModel.setArr_exploreVehicleListModel(arrListvehicleModel);

                arr_VehicleDetailsModel.add(vehicleDetailsModel);
            }else{
                new CustomToast(mcontext, Msg);
            }

        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)){
                ((VehicleListInterface)mcontext).VehicleList(strPageLimit, strTotalRecord, arr_VehicleDetailsModel);
            }
        }
    }
}
