package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.neo.cars.app.Interface.RatingReviewList_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.RatingReviewAllListModel;
import com.neo.cars.app.SetGet.RatingReviewListModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.ViewRatingActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewRatingWebService {

    Activity mcontext;
    String struser_id;
    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;
    private String Status = "0", Msg = "", strUserDeleted="";
    JSONObject details;
    RatingReviewListModel ratingReviewListModel;
    ArrayList<RatingReviewListModel> arrListratingReviewListModel;
    private int transitionflag = StaticClass.transitionflagNext;

    public void ViewRatingList(ViewRatingActivity ratingDetailsActivity, final String passenger_id) {

        mcontext = ratingDetailsActivity;
        sharedPref = new SharedPrefUserDetails(mcontext);

        StringRequest ratingReviewRequest = new StringRequest(Request.Method.POST, Urlstring.rider_review_rating_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d(":: Response details:: ", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("user_id", sharedPref.getUserid());
                params.put("user_id", passenger_id);
                params.put("page_no", "");

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        ratingReviewRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(mcontext).add(ratingReviewRequest);

    }


    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        ratingReviewListModel = new RatingReviewListModel();
        arrListratingReviewListModel = new ArrayList<RatingReviewListModel>();

        try {
            jobj_main = new JSONObject(response);
            strUserDeleted = jobj_main.optJSONObject("rider_review_rating_list").optString("user_deleted");
            Msg = jobj_main.optJSONObject("rider_review_rating_list").optString("message");
            Status = jobj_main.optJSONObject("rider_review_rating_list").optString("status");
            details = jobj_main.optJSONObject("rider_review_rating_list").optJSONObject("details");


            JSONObject jsonObject = details.getJSONObject("average_rider_ratting");
            ratingReviewListModel.setAverage_rating(jsonObject.optString("average_rating"));
            ratingReviewListModel.setCount_rating(jsonObject.optString("count_rating"));
            ratingReviewListModel.setCount_review(jsonObject.optString("count_review"));

            String countTotal = details.optString("count_total_record");

            JSONArray jsonArray = details.getJSONArray("all_review_list");
            JSONObject object;

            RatingReviewAllListModel ratingReviewAllListModel;
            ArrayList<RatingReviewAllListModel> arrListRatingReviewAllListModel = new ArrayList<>();

            for (int i = 0; i < jsonArray.length(); i++){
                object = jsonArray.getJSONObject(i);
                ratingReviewAllListModel = new RatingReviewAllListModel();
                ratingReviewAllListModel.setReview_id(object.optString("review_id"));
                ratingReviewAllListModel.setRating(object.optString("rating"));
                ratingReviewAllListModel.setComment(object.optString("comment"));
                ratingReviewAllListModel.setReview_id(object.optString("riview_by_id"));
                ratingReviewAllListModel.setSend_anonymously(object.optString("send_anonymously"));
                ratingReviewAllListModel.setRiview_by_name(object.optString("riview_by_name"));
                ratingReviewAllListModel.setUpdated_at(object.optString("updated_at"));
                arrListRatingReviewAllListModel.add(ratingReviewAllListModel);
            }

            ratingReviewListModel.setRatingReviewAllListModels(arrListRatingReviewAllListModel);

            if (Status.equals(StaticClass.SuccessResult)) {

                ((RatingReviewList_Interface) mcontext).onRatingReviewList(ratingReviewListModel);

            } else if (Status.equals(StaticClass.ErrorResult)) {
                details = jobj_main.optJSONObject("rider_review_rating_list").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length() > 0) {
                    Msg = jarrError.getString(0);
                }
                new CustomToast(mcontext, Msg);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        if ("Y".equalsIgnoreCase(strUserDeleted)) {
            Log.d("d", "***strUserDeleted***" + strUserDeleted);
            StaticClass.isLoginFalg = true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        } else {
            Log.d("d", "***Status 12***" + StaticClass.SuccessResult);
            if (Status.equals(StaticClass.SuccessResult)) {
                Log.d("d", "***Status 34***" + StaticClass.SuccessResult);
                ((RatingReviewList_Interface) mcontext).onRatingReviewList(ratingReviewListModel);
//                mcontext.finish();

            }

        }
    }


    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext ,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
