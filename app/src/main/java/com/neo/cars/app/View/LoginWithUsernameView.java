package com.neo.cars.app.View;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neo.cars.app.ForgotPassword;
import com.neo.cars.app.HomeProfileActivity;
import com.neo.cars.app.InviteFriendsActivity;
import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.RegistrationActivity;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.UserLogin_Webservice;

/**
 * Created by joydeep on 2/3/18.
 */

public class LoginWithUsernameView {

    View v;
    Activity mcontext;
    Context ab;
    EditText etUsername,etPassword;
    //added by tb
    ImageView iv_password_showhide,iv_password_showhide2;
    RelativeLayout iv_password_showhide_rl;
    Button btnGo;
    TextView tvForgotPassword;
    boolean VISIBLE_PASSWORD = false;

    public View LoginWithUsernameView(Activity context){
        mcontext = context;
        ab = context;

        try {
            v = LayoutInflater.from(mcontext).inflate(R.layout.login_with_username, null);
        }catch (Exception e){
            e.printStackTrace();
        }

        Initialize();
        Listener();
        return v;

    }

    private void Initialize() {
        etUsername=v.findViewById(R.id.etUsername);
        etPassword=v.findViewById(R.id.etPassword);
        iv_password_showhide=v.findViewById(R.id.iv_password_showhide);
        iv_password_showhide2=v.findViewById(R.id.iv_password_showhide2);
        btnGo=v.findViewById(R.id.btnGo);
        tvForgotPassword=v.findViewById(R.id.tvForgotPassword);

        //added by tb
        iv_password_showhide_rl=v.findViewById(R.id.iv_password_showhide_rl);

        iv_password_showhide.setVisibility(View.VISIBLE);//cross
        iv_password_showhide2.setVisibility(View.GONE);//eye

      /*  etUsername.setText("demo@cr.com");
        etPassword.setText("123456");*/

    }

    private void Listener(){

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean cancel = false;
                View focusView = null;

                String sUsername=etUsername.getText().toString().trim();
                String sPassword=etPassword.getText().toString().trim();

                if (TextUtils.isEmpty(sUsername)) {
                    etUsername.setError(mcontext.getString(R.string.error_field_required));
                    etUsername.requestFocus();
                    //focusView = etUsername;
                    // cancel = true;

                } else if (TextUtils.isEmpty(sPassword)){
                    etPassword.setError(mcontext.getString(R.string.error_field_required));
                    etPassword.requestFocus();
                    //focusView = etPassword;
                    // cancel = true;

                }else{
                    if (NetWorkStatus.isNetworkAvailable(mcontext)) {
                        new UserLogin_Webservice().userLoginWebservice(mcontext, sUsername, sPassword);
                    }else {
                        mcontext.startActivity(new Intent(mcontext, NetworkNotAvailable.class));
                    }

                }
            }
        });

        iv_password_showhide_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(VISIBLE_PASSWORD){
                    VISIBLE_PASSWORD=false;
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    //iv_password_showhide.setImageResource(R.drawable.show_password_icon_2);
                    iv_password_showhide.setVisibility(View.VISIBLE);
                    iv_password_showhide2.setVisibility(View.GONE);

                }else{
                    VISIBLE_PASSWORD=true;
                    etPassword.setInputType(InputType.TYPE_CLASS_TEXT );
                    //iv_password_showhide.setImageResource(R.drawable.show_password_icon);
                    iv_password_showhide.setVisibility(View.GONE);
                    iv_password_showhide2.setVisibility(View.VISIBLE);
                }

            }
        });


        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent forgotPassIntent = new Intent(mcontext, ForgotPassword.class);
                mcontext.startActivity(forgotPassIntent);

            }
        });
    }
}
