package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.VehicleUnAvailabilityDetailsModel;

public interface VehicleAddEditAvailability_Interface {

    void vehicleAddEditAvailability(VehicleUnAvailabilityDetailsModel vehicleUnAvailabilityModel, String status, String msg);
}
