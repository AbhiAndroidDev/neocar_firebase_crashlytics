package com.neo.cars.app.SetGet;

/**
 * Created by parna on 13/9/18.
 */

public class AddRatingModel {

    String booking_id;
    String riview_by_entity;
    String riview_by_id;
    String car_rating;
    String driver_rating;
    String comment;
    String vehicle_id;
    String driver_id;
    String vehicle_review_id;
    String driver_review_id;

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getRiview_by_entity() {
        return riview_by_entity;
    }

    public void setRiview_by_entity(String riview_by_entity) {
        this.riview_by_entity = riview_by_entity;
    }

    public String getRiview_by_id() {
        return riview_by_id;
    }

    public void setRiview_by_id(String riview_by_id) {
        this.riview_by_id = riview_by_id;
    }

    public String getCar_rating() {
        return car_rating;
    }

    public void setCar_rating(String car_rating) {
        this.car_rating = car_rating;
    }

    public String getDriver_rating() {
        return driver_rating;
    }

    public void setDriver_rating(String driver_rating) {
        this.driver_rating = driver_rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getVehicle_review_id() {
        return vehicle_review_id;
    }

    public void setVehicle_review_id(String vehicle_review_id) {
        this.vehicle_review_id = vehicle_review_id;
    }

    public String getDriver_review_id() {
        return driver_review_id;
    }

    public void setDriver_review_id(String driver_review_id) {
        this.driver_review_id = driver_review_id;
    }
}
