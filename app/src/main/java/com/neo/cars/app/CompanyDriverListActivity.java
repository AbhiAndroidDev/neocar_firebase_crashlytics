package com.neo.cars.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.neo.cars.app.Adapter.CompanyDriverListAdapter;
import com.neo.cars.app.Interface.CompanyDriverList_interface;
import com.neo.cars.app.SetGet.CompanyDriverListModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.Webservice.CompanyDriverList_Webservice;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.util.ArrayList;

public class CompanyDriverListActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener,
        CompanyDriverList_interface{

    private Toolbar toolbar;
    private Context context;
    private Activity activity;
    private RecyclerView rcvDriverList;

    private LinearLayoutManager layoutManagerVertical;
    private CompanyDriverListAdapter companyDriverListAdapter;
    private ImageView ib_add_vehicle;
    private int transitionflag = StaticClass.transitionflagNext;
    private ConnectionDetector cd;
    private CustomTitilliumTextViewSemiBold tvNoVehicleFound;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, rightTopBarText;
    private String strCanAddVehicle="Y";
    private SwipeRefreshLayout swipeRefreshLayout;
    private int selectionPosition=0;
    private RelativeLayout rlAddLayout, rlBackLayout;
    private SharedPrefUserDetails sharedPref;

    private BottomViewCompany bottomViewCompany = new BottomViewCompany();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_driver_list);

        new AnalyticsClass(CompanyDriverListActivity.this);
        Initialize();
        Listener();

        refreshList();
    }

    private void Initialize(){

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        sharedPref = new SharedPrefUserDetails(this);

        activity = CompanyDriverListActivity.this;
        context = CompanyDriverListActivity.this;
        cd = new ConnectionDetector(activity);

        swipeRefreshLayout = findViewById(R.id.refreshLayoutC);
        swipeRefreshLayout.setOnRefreshListener(this);

        rcvDriverList = findViewById(R.id.rcvDriverList);
        tvNoVehicleFound = findViewById(R.id.tvNoVehicleFound);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvDriverTitle));

        rightTopBarText = findViewById(R.id.rightTopBarText);
        rightTopBarText.setVisibility(View.VISIBLE);

        rlAddLayout = findViewById(R.id.rlAddLayout);
        rlAddLayout.setVisibility(View.VISIBLE);

        rlBackLayout = findViewById(R.id.rlBackLayout);

    }

    private void Listener() {
        rlAddLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(StaticClass.canAddVehicle.equalsIgnoreCase("Y")){
                    transitionflag=StaticClass.transitionflagNext;
                    Intent addvehicledriverintent = new Intent(CompanyDriverListActivity.this, CompanyAddDriverActivity.class);
                    startActivity(addvehicledriverintent);

                }else{
                    new CustomToast(CompanyDriverListActivity.this, getResources().getString(R.string.strCanAddVehicle));

                }
            }
        });

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(true);
        selectionPosition=0;
        refreshList();
    }

    private void  refreshList(){
        //fetch user vehicle list
        if (NetWorkStatus.isNetworkAvailable(activity)) {
            new CompanyDriverList_Webservice().companyDriverList(activity, CompanyDriverListActivity.this);

        } else {
            Intent i = new Intent(activity, NetworkNotAvailable.class);
            startActivity(i);
        }
        swipeRefreshLayout.setRefreshing(false);

    }

    @Override
    public void onResume() {
        super.onResume();
        refreshList();

        sharedPref.putBottomViewCompany(StaticClass.Menu_DriverLists_company);
        bottomViewCompany = new BottomViewCompany();
        bottomViewCompany.BottomViewCompany(CompanyDriverListActivity.this, StaticClass.Menu_DriverLists_company);

    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyDriverListActivity.this, transitionflag);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void companyDriverList(ArrayList<CompanyDriverListModel> arrlistDriver) {
        Log.d("d", "arrlistVehicle.size():::"+arrlistDriver.size());


        if(arrlistDriver.size() > 0){
            companyDriverListAdapter = new CompanyDriverListAdapter(context, CompanyDriverListActivity.this, arrlistDriver);
            layoutManagerVertical = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
            rcvDriverList.setLayoutManager(layoutManagerVertical);
            rcvDriverList.setItemAnimator(new DefaultItemAnimator());
            rcvDriverList.setHasFixedSize(true);
            rcvDriverList.setAdapter(companyDriverListAdapter);
            rcvDriverList.getLayoutManager().scrollToPosition(selectionPosition);

            rcvDriverList.setVisibility(View.VISIBLE);
            tvNoVehicleFound.setVisibility(View.GONE);

        }else if (arrlistDriver.size() == 0 || arrlistDriver == null){
            rcvDriverList.setVisibility(View.GONE);
            tvNoVehicleFound.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CompanyDriverListActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
