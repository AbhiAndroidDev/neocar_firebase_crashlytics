package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.neo.cars.app.Interface.PayoutInformationDetailsInterface;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CircularImageViewBorder;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CropCircleTransformation;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.PayoutInfoDetailsWebservice;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by parna on 21/5/18.
 */

public class PayoutInformationDetailsActivity extends AppCompatActivity implements PayoutInformationDetailsInterface{

    private Context context;
    private ConnectionDetector cd;
    private CustomTextviewTitilliumWebRegular tvTotalAmount, tvDiscount, tvPayableAmnt, tvAppliedCouponCode, tvPaymentStatus,
            tvDriverName, tvBookingRefrenceId, tvBookingStatus, tvCity, tvDropLoc, tvDuration, tv_toolbar_title, tvYourAmount;
    private CircularImageViewBorder civDriverPic;
    private Toolbar toolbar;
    private RelativeLayout rlBackLayout;
    private LinearLayout llAppliedCouponCode, llYourAmount;
    private int transitionflag = StaticClass.transitionflagNext;

    private Bundle bundle;
    private String strBookingId="";
    private JSONObject details=null;

    private String strTotalAmount="",strDiscount="",strpaybleAmount="",strAppliedCouponcode="",strpaymentStatus="", strVoucherCode = "", strYourAmount = "";
    private String strname="",strEmail="",strPhonenumber="",strPickupLocation="",strDropLocation="",strDuration="",strImage="", strCity = "", strBookingStatus = "";
    private BottomView bottomview = new BottomView();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payout_information_details);

        bundle = getIntent().getExtras();
        if(bundle != null){

            strBookingId = bundle.getString("BookingId");
            Log.d("d", "**BookingId**"+strBookingId);

        }
        context = this;
        cd = new ConnectionDetector(this);

        if(cd.isConnectingToInternet()){
            new PayoutInfoDetailsWebservice().PayoutInformationDetails(PayoutInformationDetailsActivity.this, strBookingId);

        }else{
            Intent i = new Intent(PayoutInformationDetailsActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }

        new AnalyticsClass(PayoutInformationDetailsActivity.this);
        Initialize();
        Listener();

    }

    private void Initialize(){

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.payoutinfoheader));

        tvTotalAmount = findViewById(R.id.tvTotalAmount);
        tvDiscount = findViewById(R.id.tvDiscount);
        tvPayableAmnt = findViewById(R.id.tvPayableAmnt);
        tvYourAmount = findViewById(R.id.tvYourAmount);
        tvAppliedCouponCode = findViewById(R.id.tvAppliedCouponCode);
        tvPaymentStatus = findViewById(R.id.tvPaymentStatus);
        tvDriverName = findViewById(R.id.tvDriverName);
        tvBookingRefrenceId = findViewById(R.id.tvBookingRefrenceId);
        tvBookingStatus = findViewById(R.id.tvBookingStatus);
        tvCity = findViewById(R.id.tvCity);
        tvDropLoc = findViewById(R.id.tvDropLoc);
        tvDuration = findViewById(R.id.tvDuration);
        civDriverPic = findViewById(R.id.civDriverPic);
        rlBackLayout = findViewById(R.id.rlBackLayout);
        llAppliedCouponCode = findViewById(R.id.llAppliedCouponCode);
        llYourAmount = findViewById(R.id.llYourAmount);
        llAppliedCouponCode.setVisibility(View.VISIBLE);
        llYourAmount.setVisibility(View.VISIBLE);

        bottomview.BottomView(PayoutInformationDetailsActivity.this, StaticClass.Menu_profile);

    }

    private void Listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
    }

    @Override
    public void onPayoutInformationDetails(String jsonObjDetails) {

        try {
            details=new JSONObject(jsonObjDetails);

            strTotalAmount=details.optString("total_amount");
            strDiscount=details.optString("discount_amount");
            strpaybleAmount=details.optString("net_payable_amount");
            strYourAmount=details.optString("vehicle_owner_amount");
            strVoucherCode=details.optString("voucher_code");
            strpaymentStatus=details.optString("payment_status");
            strname=details.optString("customer_name");
            strEmail=details.optString("customer_mail");
            strPhonenumber=details.optString("customer_contact");
            strPickupLocation=details.optString("pickup_location");
            strDropLocation=details.optString("drop_location");
            strDuration=details.getString("duration");
            strImage=details.optString("customer_image");
            strCity=details.optString("vehicle_city");
            strBookingStatus = details.optString("booking_status");


            tvTotalAmount.setText(getResources().getString(R.string.Rs)+" "+strTotalAmount);
            if (!TextUtils.isEmpty(strDiscount))
            tvDiscount.setText(getResources().getString(R.string.Rs)+" "+strDiscount);
            else tvDiscount.setText("N/A");
            tvPayableAmnt.setText(getResources().getString(R.string.Rs)+" "+strpaybleAmount);
            if (!TextUtils.isEmpty(strYourAmount) && !strYourAmount.equalsIgnoreCase("NA"))
            tvYourAmount.setText(getResources().getString(R.string.Rs)+" "+strYourAmount);
            else tvYourAmount.setText("N/A");
            if (!TextUtils.isEmpty(strVoucherCode))
            tvAppliedCouponCode.setText(strVoucherCode);
            else tvAppliedCouponCode.setText("N/A");
            if (!TextUtils.isEmpty(strpaymentStatus))
            tvPaymentStatus.setText(strpaymentStatus);
            else tvPaymentStatus.setText("N/A");
            tvDriverName.setText(strname);
            tvBookingRefrenceId.setText(strBookingId);
            if (strBookingStatus.equalsIgnoreCase("2")) {
                tvBookingStatus.setText("Cancel");
            }else{
                tvBookingStatus.setText("Completed");
            }
            tvCity.setText(strCity);
            tvDropLoc.setText(strDropLocation);
            tvDuration.setText(strDuration+" hrs");

            Picasso.get().load(strImage).transform(new CropCircleTransformation()).into(civDriverPic);


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) PayoutInformationDetailsActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(PayoutInformationDetailsActivity.this, transitionflag);
    }
}
