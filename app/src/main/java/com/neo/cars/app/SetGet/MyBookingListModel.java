package com.neo.cars.app.SetGet;

/**
 * Created by joydeep on 4/5/18.
 */

public class MyBookingListModel {

    String booking_id;
    String customer_id;
    String parent_booking_id;
    String vehicle_id;
    String driver_id;
    String booking_date;
    String start_time;
    String end_time;
    String total_passenger;
    String duration;
    String total_amount;
    String pickup_location;
    String drop_location;
    String vehicle_image;
    String review_added;
    String vehicle_rating;
    String driver_rating;
    String status;
    String is_overtime;
    String overtime_ongoing;

    public String getOvertime_ongoing() {
        return overtime_ongoing;
    }

    public void setOvertime_ongoing(String overtime_ongoing) {
        this.overtime_ongoing = overtime_ongoing;
    }

    public String getIs_overtime() {
        return is_overtime;
    }

    public void setIs_overtime(String is_overtime) {
        this.is_overtime = is_overtime;
    }

    public String getReview_added() {
        return review_added;
    }

    public void setReview_added(String review_added) {
        this.review_added = review_added;
    }

    public String getVehicle_rating() {
        return vehicle_rating;
    }

    public void setVehicle_rating(String vehicle_rating) {
        this.vehicle_rating = vehicle_rating;
    }

    public String getDriver_rating() {
        return driver_rating;
    }

    public void setDriver_rating(String driver_rating) {
        this.driver_rating = driver_rating;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getParent_booking_id() {
        return parent_booking_id;
    }

    public void setParent_booking_id(String parent_booking_id) {
        this.parent_booking_id = parent_booking_id;
    }

    public String getVehicle_id() {
        return vehicle_id;
    }

    public void setVehicle_id(String vehicle_id) {
        this.vehicle_id = vehicle_id;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getTotal_passenger() {
        return total_passenger;
    }

    public void setTotal_passenger(String total_passenger) {
        this.total_passenger = total_passenger;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getPickup_location() {
        return pickup_location;
    }

    public void setPickup_location(String pickup_location) {
        this.pickup_location = pickup_location;
    }

    public String getDrop_location() {
        return drop_location;
    }

    public void setDrop_location(String drop_location) {
        this.drop_location = drop_location;
    }

    public String getVehicle_image() {
        return vehicle_image;
    }

    public void setVehicle_image(String vehicle_image) {
        this.vehicle_image = vehicle_image;
    }




}
