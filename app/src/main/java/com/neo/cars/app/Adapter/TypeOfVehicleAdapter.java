package com.neo.cars.app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.neo.cars.app.Interface.TripCostSelectInterface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.TypeOfVehicleModel;
import java.util.ArrayList;

public class TypeOfVehicleAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<TypeOfVehicleModel> arrlistVehicleType;
    private int selectedPosition = 0;
    private TripCostSelectInterface selectInterface;
    private LayoutInflater inflter;
    private TextView textView;

    public TypeOfVehicleAdapter(Context context, TripCostSelectInterface _selectInterface,  ArrayList<TypeOfVehicleModel> _arrlistVehicleType,
                                int _selectedPosition) {
        this.mContext = context;
        this.arrlistVehicleType = _arrlistVehicleType;
        this.selectedPosition = _selectedPosition;
        this.selectInterface = _selectInterface;

        inflter = (LayoutInflater.from(mContext));
    }

    @Override
    public int getCount() {
        return arrlistVehicleType.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View view = inflter.inflate(R.layout.item_gridview, null);

        textView = view.findViewById(R.id.tv_item_name);

//        final TextView textView;
//
//        if (convertView == null) {
//
//            textView = new TextView(mContext);
//            textView.setLayoutParams(new GridView.LayoutParams(250, 100));
//            textView.setPadding(5, 5, 5, 5);
//            textView.setBackgroundResource(R.drawable.base);
//            textView.setGravity(Gravity.CENTER);
//
//        }else{
//            textView = (TextView) convertView;
//        }


        textView.setText(arrlistVehicleType.get(position).getName());

        if(selectedPosition == position){
            textView.setBackgroundResource(R.drawable.base_yellow);
        }
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setBackgroundResource(R.drawable.base_yellow);
                selectInterface.setTripCost(position, "vehicle_type", 0);
            }
        });

        return view;
    }
}
