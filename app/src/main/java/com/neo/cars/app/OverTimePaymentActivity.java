package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.neo.cars.app.PaytmIntegration.Constants;
import com.neo.cars.app.PaytmIntegration.Paytm;
import com.neo.cars.app.SetGet.PassengerModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CommonUtility;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.OvertimeBeforeRedirectPayTmApi;
import com.neo.cars.app.Webservice.Process_Overtime_Booking_webservice;
import com.neo.cars.app.Webservice.Urlstring;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONException;
import org.json.JSONObject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by parna on 30/5/18.
 */

public class OverTimePaymentActivity extends AppCompatActivity implements PaytmPaymentTransactionCallback {

    private Context context;
    private ConnectionDetector cd;

    private RelativeLayout rlBackLayout;

    private Toolbar toolbar;
    private TextView tvOvertimePaymentDate, tvOvertimePaymentDuration, tvOvertimePaymentStartTime, tvOvertimePaymentReasonextension;
    private TextView tvOvertimePaymentAddtionalInformation, tvOvertimePaymentBaggage;
    private TextView tvOvertimePaymentAmount, tvOvertimeTotalAmount, tvOvertimeNightCharge, tv_toolbar_title, tvParkingCharges;
    private Button btntvOvertimePaymentMakePayment;
    private LinearLayout linearAddPassengers;

    private String strbookingdate="",strDuration="",strStartTime="", strEndTime="", strReasonforextension="", strexpectedReleaseTime ="";
    private String strAdditionalInformation="",strNoofBaggage="",strTotalAmount="", strNightCharge = "", strNetPayableAmount = "",strparking_charge = "",
            strBookingId="",strExpectedRoute="",addl_passenger="", vehicle_city_name = "";

    private ArrayList<PassengerModel> arrlistPassenger;

    private int transitionflag = StaticClass.transitionflagNext;
    private Bundle bundle;
    private SharedPrefUserDetails prefs;

    private static final String PAYTM_GENERATE_CHECK_SUM = Urlstring.Basepathmain+"paytmApp/generateChecksum.php";
    private Paytm paytm;
    private String customerid = "CUST123456", amount = "1.00";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_overtimepayment);

        prefs = new SharedPrefUserDetails(this);

        bundle = getIntent().getExtras();
        if(bundle != null){

            strBookingId= bundle.getString("BookingId");
            strExpectedRoute=bundle.getString("ExpectedRoute");
            strbookingdate = bundle.getString("Date");
            strDuration = bundle.getString("Duration");
            strStartTime = bundle.getString("StartTime");
            strEndTime = bundle.getString("EndTime");

            strReasonforextension = bundle.getString("ReasonExtension");
            strexpectedReleaseTime = bundle.getString("ExpectedReleaseTime");
            strAdditionalInformation = bundle.getString("AdditionalInformation");
            strNoofBaggage = bundle.getString("NoofBaggage");
            strTotalAmount=bundle.getString("TotalAmount");
            strNightCharge=bundle.getString("NightCharge");
            strNetPayableAmount=bundle.getString("NetPayableAmount");
            strparking_charge=bundle.getString("parking_charge");
            arrlistPassenger = bundle.getParcelableArrayList("PassengerList");
            addl_passenger=bundle.getString("addl_passenger");
            vehicle_city_name=bundle.getString("vehicle_city_name");


            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                final Date dateObj = sdf.parse(strStartTime);
                System.out.println(dateObj);
                System.out.println(new SimpleDateFormat("h:mm a").format(dateObj));

                strStartTime = new SimpleDateFormat("h:mm a").format(dateObj);
                prefs.setStartTime(strStartTime);
            } catch (final ParseException e) {
                e.printStackTrace();
            }

            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                final Date dateObj = sdf.parse(strEndTime);
                System.out.println(dateObj);
                System.out.println(new SimpleDateFormat("h:mm a").format(dateObj));

                strEndTime = new SimpleDateFormat("h:mm a").format(dateObj);
                prefs.setEndTime(strEndTime);
            } catch (final ParseException e) {
                e.printStackTrace();
            }

            Log.e("strbookingdate",strbookingdate);
            Log.e("strStartTime",strStartTime);
            Log.e("strEndTime",strEndTime);
            Log.e("strDuration",strDuration);
            Log.e("strTotalAmount",strTotalAmount);
            Log.e("addl_passenger",addl_passenger);
        }

        new AnalyticsClass(OverTimePaymentActivity.this);

        initialize();
        listener();
    }

    private void initialize() {

        context = this;
        cd = new ConnectionDetector(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title =  findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.booknow));

        tvOvertimePaymentDate = findViewById(R.id.tvOvertimePaymentDate);
        tvOvertimePaymentDuration = findViewById(R.id.tvOvertimePaymentDuration);
        tvOvertimePaymentStartTime = findViewById(R.id.tvOvertimePaymentStartTime);
        tvOvertimePaymentReasonextension = findViewById(R.id.tvOvertimePaymentReasonextension);
        tvOvertimePaymentAddtionalInformation = findViewById(R.id.tvOvertimePaymentAddtionalInformation);
        tvOvertimePaymentBaggage = findViewById(R.id.tvOvertimePaymentBaggage);

        tvOvertimeTotalAmount = findViewById(R.id.tvOvertimeTotalAmount);
        tvOvertimeNightCharge = findViewById(R.id.tvOvertimeNightCharge);
        tvOvertimePaymentAmount = findViewById(R.id.tvOvertimeNetPayableAmount);
        tvParkingCharges = findViewById(R.id.tvParkingCharges);

        linearAddPassengers = findViewById(R.id.linearAddPassengers);
        btntvOvertimePaymentMakePayment = findViewById(R.id.btntvOvertimePaymentMakePayment);

        rlBackLayout = findViewById(R.id.rlBackLayout);

        tvOvertimePaymentDate.setText(strbookingdate);
        tvOvertimePaymentDuration.setText(strDuration+" hours");
        tvOvertimePaymentStartTime.setText(strStartTime);
        tvOvertimePaymentReasonextension.setText(strReasonforextension);
        tvOvertimePaymentAddtionalInformation.setText(strAdditionalInformation);
        tvOvertimePaymentBaggage.setText(strNoofBaggage);

        tvOvertimeTotalAmount.setText(getResources().getString(R.string.Rs)+strTotalAmount);
        tvOvertimeNightCharge.setText(getResources().getString(R.string.Rs)+strNightCharge);
        tvOvertimePaymentAmount.setText(getResources().getString(R.string.Rs)+strNetPayableAmount);
        tvParkingCharges.setText(strparking_charge);

        if (arrlistPassenger != null) {
            Log.d("d", "****arrlistPassenger size***" + arrlistPassenger.size());
            if (arrlistPassenger.size() > 0) {
                for (int i = 0; i < arrlistPassenger.size(); i++) {
                    //inflate layout
                    linearAddPassengers.addView(addView2(i));
                }
            } else {
            }
        }
    }

    public View addView2(final int position) {

        View v;
        v = LayoutInflater.from(this).inflate(R.layout.addmore_name_phno_inflate, null);

        CustomTextviewTitilliumWebRegular tvName = v.findViewById(R.id.tvName);
        CustomTextviewTitilliumWebRegular tvMobNo = v.findViewById(R.id.tvMobNo);

        tvName.setText(arrlistPassenger.get(position).getPassenger_name());
        tvMobNo.setText(arrlistPassenger.get(position).getPassenger_contact());

        return v;
    }

    private void listener(){

        btntvOvertimePaymentMakePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(cd.isConnectingToInternet()){
                    new OvertimeBeforeRedirectPayTmApi().overtimeBeforeRedirectPayTm(OverTimePaymentActivity.this, strBookingId, strDuration, "",
                            "", strNightCharge, strNetPayableAmount, strExpectedRoute, strReasonforextension, strexpectedReleaseTime,
                            strAdditionalInformation, strNoofBaggage, addl_passenger, vehicle_city_name);
                }else{
                    Intent i = new Intent(OverTimePaymentActivity.this, NetworkNotAvailable.class);
                    startActivity(i);
                }

                String strtimeformat=new CommonUtility(OverTimePaymentActivity.this).timeformat(strStartTime);
                Log.e("Stringtimeformat",strtimeformat);

                if(cd.isConnectingToInternet()){

                    //paytm payment gateway calling

                    paytm = new Paytm(
                            Constants.M_ID,
                            Constants.CHANNEL_ID,
//                            amount,
                            strNetPayableAmount,
                            Constants.WEBSITE,
                            Constants.CALLBACK_URL,
                            Constants.INDUSTRY_TYPE_ID,
                            customerid
                    );

                    //calling the method generateCheckSum() which will generate the paytm checksum for payment
                    generateCheckSum(paytm);


                }else{
                    Intent i = new Intent(OverTimePaymentActivity.this, NetworkNotAvailable.class);
                    startActivity(i);
                }
            }
        });

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(OverTimePaymentActivity.this,
                        OverTimePaymentActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                        OverTimePaymentActivity.this.getResources().getString(R.string.yes),
                        OverTimePaymentActivity.this.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();
                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
            }
        });
    }

    private void generateCheckSum(final Paytm paytm){

        StringRequest request = new StringRequest(Request.Method.POST, PAYTM_GENERATE_CHECK_SUM, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    System.out.println(jsonObject.toString(3));

                    initializePaytmPayment(jsonObject.getString("CHECKSUMHASH"), paytm);

                } catch (JSONException e) {

                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();

                param.put("MID", paytm.getmId());
                param.put("ORDER_ID", paytm.getOrderId());
                param.put("CUST_ID", paytm.getCustId());
                param.put("CHANNEL_ID", paytm.getChannelId());
                param.put("TXN_AMOUNT", paytm.getTxnAmount());
                param.put("WEBSITE", paytm.getWebsite());
                param.put("CALLBACK_URL", paytm.getCallBackUrl() + paytm.getOrderId());
                param.put("INDUSTRY_TYPE_ID", paytm.getIndustryTypeId());

                if (prefs.getMobile() !=null ){
                    param.put("MERC_UNQ_REF", prefs.getMobile()+"_"+paytm.getCustId());
                }
                else{
                    param.put("MERC_UNQ_REF", prefs.getUserid()+"_"+paytm.getCustId());
                }

                System.out.println("***generateCheckSum param : " + param.toString());

                return param;
            }
        };

        request.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(OverTimePaymentActivity.this).add(request);
    }


    private void initializePaytmPayment(String checksumHash, Paytm paytm) {

        //getting paytm service
        PaytmPGService Service = PaytmPGService.getStagingService("");

        //use this when using for production
        //PaytmPGService Service = PaytmPGService.getProductionService();

        //creating a hashmap and adding all the values required
        HashMap<String, String> paramMap = new HashMap<>();
        paramMap.put("MID", Constants.M_ID);
        paramMap.put("ORDER_ID", paytm.getOrderId());
        paramMap.put("CUST_ID", paytm.getCustId());
        paramMap.put("CHANNEL_ID", paytm.getChannelId());
        paramMap.put("TXN_AMOUNT", paytm.getTxnAmount());
        paramMap.put("WEBSITE", paytm.getWebsite());
        paramMap.put("CALLBACK_URL", paytm.getCallBackUrl() + paytm.getOrderId());
        paramMap.put("CHECKSUMHASH", checksumHash);
        paramMap.put("INDUSTRY_TYPE_ID", paytm.getIndustryTypeId());

        if (prefs.getMobile() !=null ){
            paramMap.put("MERC_UNQ_REF", prefs.getMobile()+"_"+paytm.getCustId());
        }
        else{
            paramMap.put("MERC_UNQ_REF", prefs.getUserid()+"_"+paytm.getCustId());
        }
       // paramMap.put("MERC_UNQ_REF", prefs.getMobile()+"_"+paytm.getCustId());


        //creating a paytm order object using the hashmap
        PaytmOrder order = new PaytmOrder(paramMap);

        //intializing the paytm service
        Service.initialize(order, null);

        //finally starting the payment transaction
        Service.startPaymentTransaction(this, true, true, this);

    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) OverTimePaymentActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }


    @Override
    public void onBackPressed() {

        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(OverTimePaymentActivity.this,
                OverTimePaymentActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                OverTimePaymentActivity.this.getResources().getString(R.string.yes),
                OverTimePaymentActivity.this.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();
    }

    @Override
    public void onTransactionResponse(Bundle bundle) {

        Log.d("paytmBundle", bundle.toString());

        String strStatus = bundle.getString("STATUS");
        Log.d("strStatus", strStatus);
        String strOrderId = bundle.getString("ORDERID");
        String strTransactionId = bundle.getString("TXNID");
        String strBankTransactionId = bundle.getString("BANKTXNID");
        String strTransactionInfo = "[{\"BANKTXNID\":\""+strBankTransactionId+"\",\"STATUS\":\""+strStatus+"\",\"TXNID\":\""+strOrderId+"\"}]";

//        "transaction_info": "[{\"BANKTXNID\":\"5556157\",\"STATUS\":\"TXN_SUCCESS\",\"TXNID\":\"20180921111212800110168824600022527\"}]",

        if(Objects.equals(strStatus, "TXN_SUCCESS")) {

            new Process_Overtime_Booking_webservice().Process_Overtime_Booking(OverTimePaymentActivity.this, strBookingId, strDuration, strTransactionId,
                    strTransactionInfo, strNightCharge, strNetPayableAmount, strExpectedRoute, strReasonforextension, strexpectedReleaseTime,
                    strAdditionalInformation, strNoofBaggage, addl_passenger, vehicle_city_name);
        }else {
            new CustomToast(OverTimePaymentActivity.this, strStatus);
        }

    }

    @Override
    public void networkNotAvailable() {
        Toast.makeText(this, "Network error", Toast.LENGTH_LONG).show();
    }

    @Override
    public void clientAuthenticationFailed(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void someUIErrorOccurred(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressedCancelTransaction() {
//        Toast.makeText(this, "Back Pressed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        Toast.makeText(this, s + bundle.toString(), Toast.LENGTH_LONG).show();
    }
}
