package com.neo.cars.app.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.neo.cars.app.CompanyPayoutInfoDetailsActivity;
import com.neo.cars.app.PayoutInformationDetailsActivity;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.PaymentInfoModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.font.CustomTextviewTitilliumBold;

import java.util.List;

/**
 * Created by joydeep on 18/5/18.
 */

public class PayoutInfoAdapter extends RecyclerView.Adapter<PayoutInfoAdapter.MyViewHolder> {

    private List<PaymentInfoModel> PaymentInfoList;

    private Context mContext;
    private SharedPrefUserDetails prefs;
    private String userType;


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_BookingDate,tv_pickuplocation,tv_droplocation,tv_Amount, tv_payment_settled_date, tv_refund, tv_Payable, tv_discount;
        private RelativeLayout paymentinfo_relative;
        private LinearLayout amount_linear, ll_payment_settled_date;
        private CustomTextviewTitilliumBold tvPickUpLocHeader, tvDropOffLoc;


        public MyViewHolder(View view) {
            super(view);
            tv_BookingDate = view.findViewById(R.id.tv_BookingDate);
            tv_pickuplocation = view.findViewById(R.id.tv_pickuplocation);
            tvPickUpLocHeader = view.findViewById(R.id.tvPickUpLocHeader);

            tv_droplocation = view.findViewById(R.id.tv_droplocation);
            tvDropOffLoc = view.findViewById(R.id.tvDropOffLoc);

            tv_Amount = view.findViewById(R.id.tv_Amount);
            tv_payment_settled_date = view.findViewById(R.id.tv_payment_settled_date);
            paymentinfo_relative=view.findViewById(R.id.paymentinfo_relative);
            ll_payment_settled_date = view.findViewById(R.id.ll_payment_settled_date);
            amount_linear = view.findViewById(R.id.amount_linear);
            amount_linear.setVisibility(View.VISIBLE);

        }
    }

    public PayoutInfoAdapter(List<PaymentInfoModel> myPaymentInfoList,Context context) {
        PaymentInfoList = myPaymentInfoList;
        mContext=context;

        prefs = new SharedPrefUserDetails(mContext);
        userType = prefs.getUserType();
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.payout_info_inflate, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tv_BookingDate.setText(PaymentInfoList.get(position).getBooking_date());
        holder.tv_pickuplocation.setText(PaymentInfoList.get(position).getBooking_id());
        holder.tv_droplocation.setText(PaymentInfoList.get(position).getPayment_status());


        if(PaymentInfoList.get(position).getTotal_amount().equalsIgnoreCase("null") ||
                PaymentInfoList.get(position).getTotal_amount().equalsIgnoreCase("NA")) {
            holder.tv_Amount.setText("N/A");

        }else {
            holder.tv_Amount.setText(mContext.getResources().getString(R.string.Rs)+" "+ PaymentInfoList.get(position).getTotal_amount());
        }

        holder.tv_payment_settled_date.setText(PaymentInfoList.get(position).getSettled_datetime());


        holder.paymentinfo_relative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bookingid=PaymentInfoList.get(position).getBooking_id();
                Intent intent = null;
                if (userType.equalsIgnoreCase("U")) {

                    intent = new Intent(mContext, PayoutInformationDetailsActivity.class);
                }else if (userType.equalsIgnoreCase("C")){
                    intent = new Intent(mContext, CompanyPayoutInfoDetailsActivity.class);
                }

                intent.putExtra("BookingId",bookingid);
                mContext.startActivity(intent);

            }
        });

    }


    @Override
    public int getItemCount() {
        return PaymentInfoList.size();

    }
}
