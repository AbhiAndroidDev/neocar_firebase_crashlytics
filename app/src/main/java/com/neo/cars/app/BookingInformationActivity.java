package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.neo.cars.app.Interface.CallbackCancelButtonClick;
import com.neo.cars.app.Interface.CallbackCancelReasonButtonClick;
import com.neo.cars.app.Interface.MyBookingDetails_Interface;
import com.neo.cars.app.SetGet.BookingInformationModel;
import com.neo.cars.app.SetGet.DriverDataModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleInformationModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.ImageUtils;
import com.neo.cars.app.Utils.MessageText;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomSheetCancelView;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.AcceptBooking_webservice;
import com.neo.cars.app.Webservice.BookingDetails_Webservice;
import com.neo.cars.app.Webservice.CancelBookingCarOwner_Webservice;
import com.neo.cars.app.Webservice.CancelBookingCarseeker_Webservice;
import com.neo.cars.app.Webservice.StopBooking_Webservice;
import com.neo.cars.app.dialog.BottomSheetCancel;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;
import com.neo.cars.app.fragment.BookingInfoPager;

/**
 * Created by parna on 15/3/18.
 */

public class BookingInformationActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener ,
        MyBookingDetails_Interface, CallbackCancelButtonClick, CallbackCancelReasonButtonClick {

    private TabLayout tabs;
    private ViewPager viewpager;
    private RelativeLayout rlBackLayout,ibMybookingAction,bookinginfoaction_dialog, bottom_cancel_relative;
    private TextView btnCancelRide;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title, tvProfile;
    private ImageButton ibNavigateMenu;
    private String booking_id="",booking_Type="", strBookingFlag="", strCarOwnerId="", strHaveOtherLocation="",
            strBookingStatus="", strCancelReason = "", strisovertime = "", strovertime_ongoing = "";
    private CustomTitilliumTextViewSemiBold tvReqOvertime, tvReqStopRide;
    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private SharedPrefUserDetails sharedPref;
    private CustomTitilliumTextViewSemiBold tvAcceptBooking;
    private int transitionflag = StaticClass.transitionflagNext;
    boolean ActionFlag=false;
    private BottomView bottomview =new BottomView();
    private ConnectionDetector cd;
    private Context context;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_information);

        StaticClass.continueExploring = false;

        booking_id=getIntent().getExtras().getString("booking_id");
        booking_Type=getIntent().getExtras().getString("booking_Type");
        strBookingFlag = getIntent().getExtras().getString("booking_flag");
        strHaveOtherLocation = getIntent().getExtras().getString("have_other_location");
        strBookingStatus = getIntent().getExtras().getString("booking_status");
        strisovertime = getIntent().getExtras().getString("is_overtime");
        strovertime_ongoing = getIntent().getExtras().getString("overtime_ongoing");

        System.out.println("booking_id: "+booking_id);
        Log.d("d", "Booking flag::"+strBookingFlag);
        Log.d("d", "***booking_Type***::"+booking_Type);
        Log.d("d", "***have_other_location***::"+strHaveOtherLocation);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        sharedPref = new SharedPrefUserDetails(BookingInformationActivity.this);

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);
        strCarOwnerId = UserLoginDetails.getId();

        new AnalyticsClass(BookingInformationActivity.this);

        Initialize();
        Listener();
    }

    private void Initialize(){

        context = this;

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        cd = new ConnectionDetector(this);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvDetails));
        rlBackLayout=findViewById(R.id.rlBackLayout);
        ibMybookingAction=findViewById(R.id.ibMybookingAction);
        bookinginfoaction_dialog=findViewById(R.id.bookinginfoaction_dialog);
        bottom_cancel_relative=findViewById(R.id.bottom_cancel_relative);

        tvReqOvertime= findViewById(R.id.tvReqOvertime);
        tvReqStopRide= findViewById(R.id.tvReqStopRide);

        btnCancelRide= findViewById(R.id.btnCancelRide);
        tvAcceptBooking =  findViewById(R.id.tvAcceptBooking);


        if(booking_Type.equals(StaticClass.MyVehicleOngoing)){
            ibMybookingAction.setVisibility(View.VISIBLE);
            tvReqStopRide.setVisibility(View.VISIBLE);

            btnCancelRide.setVisibility(View.GONE);
            tvAcceptBooking.setVisibility(View.GONE);

            if (strisovertime.equalsIgnoreCase("Y")){
                tvReqOvertime.setVisibility(View.GONE);

            }else if(strisovertime.equalsIgnoreCase("N")) {

                tvReqOvertime.setVisibility(View.VISIBLE);
            }

            if(strovertime_ongoing.equalsIgnoreCase("Y")){
                ibMybookingAction.setVisibility(View.GONE);
                tvReqStopRide.setVisibility(View.GONE);
            }

        }else if(booking_Type.equals(StaticClass.MyVehicleUpcoming)){

            ibMybookingAction.setVisibility(View.VISIBLE);
            tvAcceptBooking.setVisibility(View.GONE);
            btnCancelRide.setVisibility(View.VISIBLE);
            tvReqOvertime.setVisibility(View.GONE);
            tvReqStopRide.setVisibility(View.GONE);

        }else if(booking_Type.equalsIgnoreCase(StaticClass.MyVehicleBooking)){


            if(strBookingFlag.equalsIgnoreCase(StaticClass.MyVehicleUpcoming)) {


                ibMybookingAction.setVisibility(View.VISIBLE);
                btnCancelRide.setVisibility(View.VISIBLE);
                tvReqOvertime.setVisibility(View.GONE);
                tvReqStopRide.setVisibility(View.GONE);

                if (("Y".equalsIgnoreCase(strHaveOtherLocation) && ("0".equals(strBookingStatus)))){

                    tvAcceptBooking.setVisibility(View.VISIBLE); // it means for car owner
                }else{
                    tvAcceptBooking.setVisibility(View.GONE); // it means for car seeker(user)
                }

            }else{
                ibMybookingAction.setVisibility(View.GONE);
            }

        } else{
            ibMybookingAction.setVisibility(View.GONE);
        }

        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);

        viewpager = findViewById(R.id.viewpager);

        tabs = findViewById(R.id.tabs);

        //Adding the tabs using addTab() method

        tabs.addTab(tabs.newTab().setText("Booking Information"));
        tabs.addTab(tabs.newTab().setText("Driver Information"));
        tabs.addTab(tabs.newTab().setText("Vehicle Information"));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        bottomview.BottomView(BookingInformationActivity.this,StaticClass.Menu_profile);

        if (NetWorkStatus.isNetworkAvailable(BookingInformationActivity.this)) {
            new BookingDetails_Webservice().BookingDetails(BookingInformationActivity.this , booking_id);

        } else {
            Intent i = new Intent(BookingInformationActivity.this, NetworkNotAvailable.class);
            startActivity(i);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });


        ibMybookingAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ActionFlag){
                    bookinginfoaction_dialog.setVisibility(View.GONE);
                    ActionFlag=false;
                }else{
                    bookinginfoaction_dialog.setVisibility(View.VISIBLE);
                    ActionFlag=true;
                }
            }
        });

        tvReqOvertime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transitionflag = StaticClass.transitionflagNext;
                Intent reqOvertimeIntent = new Intent(BookingInformationActivity.this, RequestOvertimeActivity.class);
                reqOvertimeIntent.putExtra("bookingId", booking_id);
                startActivity(reqOvertimeIntent);
            }
        });

        tvReqStopRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                BottomSheetCancel bsd = new BottomSheetCancel(BookingInformationActivity.this,
                        BookingInformationActivity.this,
                        BookingInformationActivity.this,
                        BookingInformationActivity.this.getResources().getString(R.string.DoyouWantToStopthisBook),
                        BookingInformationActivity.this.getResources().getString(R.string.yes),
                        BookingInformationActivity.this.getResources().getString(R.string.no), "", "" );

            }
        });

        btnCancelRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              Log.d("d", "****BottomSheetCancel strBookingFlag ****" + strBookingFlag );
              Log.d("d", "***BottomSheetCancel strBookingType ***" + booking_Type);

                bottom_cancel_relative.addView(new BottomSheetCancelView().BottomSheetCancelView(BookingInformationActivity.this, BookingInformationActivity.this,
                        BookingInformationActivity.this,
                        BookingInformationActivity.this.getResources().getString(R.string.ReasonToCancelthisBook),
                        BookingInformationActivity.this.getResources().getString(R.string.cancel),
                        BookingInformationActivity.this.getResources().getString(R.string.Submit),
                        strBookingFlag, booking_Type));
            }
        });

        tvAcceptBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cd.isConnectingToInternet()){

                    new AcceptBooking_webservice().acceptBooking(BookingInformationActivity.this, booking_id);

                }else {
                    Intent nointernetintent = new Intent(BookingInformationActivity.this, NetworkNotAvailable.class);
                    startActivity(nointernetintent);
                }
            }
        });
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewpager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void OnMyBookingDetails(BookingInformationModel bookingInformationModel, DriverDataModel driverdataModel,
                                   VehicleInformationModel vehicleInformationModel, String strShowDriver, String strShowDriverMsg) {
        //Creating our pager adapter


        final BookingInfoPager adapter = new BookingInfoPager(getSupportFragmentManager(), tabs.getTabCount(),
                bookingInformationModel, driverdataModel, vehicleInformationModel, strShowDriver, strShowDriverMsg, StaticClass.USER, booking_Type);

        //Adding adapter to pager
        viewpager.setAdapter(adapter);

        //Adding onTabSelectedListener to swipe views
        tabs.setOnTabSelectedListener(this);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));

        if (booking_Type.equals(StaticClass.MyVehicleUpcoming) && !TextUtils.isEmpty(strShowDriverMsg)) {

            new CustomToast(this, strShowDriverMsg);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        new OnPauseSlider(BookingInformationActivity.this, transitionflag);
    }


    @Override
    public void onButtonClick(String strButtonText, String strBookingFlag, String booking_Type) {
        if (strButtonText.equals(getResources().getString(R.string.yes)) || strButtonText.equals(getResources().getString(R.string.Submit))) {

            if(booking_Type.equalsIgnoreCase(StaticClass.MyVehicleBooking)){

                if(strBookingFlag.equalsIgnoreCase("U"))
                {
                    Log.d("d", "Car owner cancel call");
                    if(NetWorkStatus.isNetworkAvailable(BookingInformationActivity.this)){
                        new CancelBookingCarOwner_Webservice().cancelBookingCarOwner(BookingInformationActivity.this, strCarOwnerId, booking_id, strCancelReason );

                    }else {
                        new CustomToast(BookingInformationActivity.this,MessageText.Network_not_availabl);

                    }
                }

            }else if (booking_Type.equalsIgnoreCase(StaticClass.MyVehicleUpcoming)){
                Log.d("d", "Car seeker cancel call");
                if (NetWorkStatus.isNetworkAvailable(BookingInformationActivity.this)) {
                    new CancelBookingCarseeker_Webservice().CancelBookingCarseeker(BookingInformationActivity.this, booking_id,strCancelReason);

                } else {
                    new CustomToast(BookingInformationActivity.this,MessageText.Network_not_availabl);
                }

        } else{
                Log.d("d", "Booking id::"+booking_id);

                //for Stop
                if (NetWorkStatus.isNetworkAvailable(BookingInformationActivity.this)){
                    new StopBooking_Webservice().stopBooking(BookingInformationActivity.this, booking_id);

                }else{
                    new CustomToast(BookingInformationActivity.this,MessageText.Network_not_availabl);
                }
            }


    }else if (strButtonText.equals(BookingInformationActivity.this.getResources().getString(R.string.no))) {

    }else if (strButtonText.equalsIgnoreCase("Cancel")){
            System.out.println("****Tap on cancel 1234**");
            try {
                InputMethodManager inputManager = (InputMethodManager) BookingInformationActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(new View(this).getWindowToken(), 0);
                bottom_cancel_relative.removeAllViews();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void onCancelReasonSubmitButtonClick(String mStrPositive, String mBookingFlag, String mBookingType, String mCancelReason) {

        this.strCancelReason = mCancelReason;
        Log.d("d", "Car seeker cancel call");
        if (NetWorkStatus.isNetworkAvailable(BookingInformationActivity.this)) {
            new CancelBookingCarseeker_Webservice().CancelBookingCarseeker(BookingInformationActivity.this, booking_id, mCancelReason);

        } else {
            new CustomToast(BookingInformationActivity.this,MessageText.Network_not_availabl);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfile){
            finish();
        }

        if(StaticClass.continueExploring){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }


    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) BookingInformationActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
