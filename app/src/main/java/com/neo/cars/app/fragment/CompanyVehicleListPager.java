package com.neo.cars.app.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

/**
 * Created by parna on 1/11/18.
 */

public class CompanyVehicleListPager extends FragmentStatePagerAdapter {

    private int tabCount;
    private String vehicleId="";
    private Bundle bundle=null;

    public CompanyVehicleListPager(FragmentManager fm, int tabCount, String svehicleId) {
        super(fm);
        //Initializing tab count
        this.tabCount= tabCount;
        vehicleId=svehicleId;
        Log.d("d", "vehicleId::"+vehicleId);
    }

    public CompanyVehicleListPager(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                MoreVehicleInfoFragment tab1 = new MoreVehicleInfoFragment();
                bundle = new Bundle();
                bundle.putString("vehicleId", vehicleId);
                tab1.setArguments(bundle);
                return tab1;

            case 1:
                VehicleAvailabilityInfofragment tab2 = new VehicleAvailabilityInfofragment();
                bundle = new Bundle();
                bundle.putString("vehicleId", vehicleId);
                tab2.setArguments(bundle);
                return tab2;

            default:
                return null;

        }

    }
    @Override
    public int getCount() {
        return tabCount;
    }
}
