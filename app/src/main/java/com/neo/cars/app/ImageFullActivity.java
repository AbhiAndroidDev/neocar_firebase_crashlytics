package com.neo.cars.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Utils.TouchImageView;
import com.squareup.picasso.Picasso;


public class ImageFullActivity extends AppCompatActivity {

//    private TouchImageView ivFull;
    private ImageView ivFull;
    private ImageButton imgBtClose;
    private String strImgUrl;
    private int transitionflag = StaticClass.transitionflagBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_full);

        new AnalyticsClass(ImageFullActivity.this);

        ivFull = findViewById(R.id.ivFull);
        imgBtClose = findViewById(R.id.imgBtClose);

        strImgUrl = getIntent().getStringExtra("imgUrl");
        Log.d("strImgUrls", strImgUrl);

        if (!TextUtils.isEmpty(strImgUrl)) {

            Glide.with(ImageFullActivity.this)
                    .load(strImgUrl)
                    .into(ivFull);

//            Picasso.get()
//                    .load(strImgUrl)
//                    .placeholder(R.drawable.vehicle_info_blank_image)
//                    .into(ivFull);
        }
        imgBtClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(0, R.anim.zoom_exit);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }
}
