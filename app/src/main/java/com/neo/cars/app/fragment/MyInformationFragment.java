package com.neo.cars.app.fragment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.BookingInformationModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.ViewRatingActivity;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.font.CustomTitilliumTextViewSemiBold;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by parna on 15/3/18.
 */

public class MyInformationFragment extends Fragment{

    private View mView;
    private Gson gson;
    private String bookingInformation;
    private BookingInformationModel bookingInformationModel;

    private TextView tvLocation, tvBookingRefId, tvCost,tvDate,tvDuration,tvStarttime,tvName,tvDOB,tvSex,tvAditionalPassengerHeader,
            tvnoofpasenger,tvPickLoc,tvDropLoc,tvAddlReq,tvAddlInfo,tvPurposeOfHire,
            tvExpectedTravelItinerary, tvParkingCost, tvParkingCostHeader;

    private ImageView tvAditionalPassengerArrow;
    private LinearLayout tvAditionalPassengerLinear;
    private boolean AddtionPasengerFlagvisible=true;

    private SharedPrefUserDetails prefs;

    private RelativeLayout rlCancelLayout;
    private CustomTitilliumTextViewSemiBold tvcancelInfoHeader, tvCancelledByHeader, tvCancelReasonHeader, tvCancellationChargeHeader,
            tvRefundableAmountHeader;

    private CustomTextviewTitilliumWebRegular tvCancelledBy, tvCancelReason, tvCancellationCharge, tvRefundableAmount;
    private CustomButtonTitilliumSemibold btn_View_Rating;
    private int transitionflag = StaticClass.transitionflagNext;
    final int PERMISSION_REQUEST_CODE = 111;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (mView != null) {
            ViewGroup parent = (ViewGroup) mView.getParent();
            if (parent != null)
                parent.removeView(mView);
        }
        try {
            mView = inflater.inflate(R.layout.fragment_my_information, container, false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        if(getArguments() != null){
            bookingInformation = getArguments().getString("bookingInformation");
            Log.d("d", "Get strvehicleId: "+bookingInformation);
        }

        gson = new Gson();
        bookingInformationModel=new BookingInformationModel();
        bookingInformationModel = gson.fromJson(bookingInformation, BookingInformationModel.class);

        Initialize();
        listener();
        SetData();

        return mView;

    }

    private void Initialize(){

        prefs = new SharedPrefUserDetails(getActivity());
        tvLocation =  mView.findViewById(R.id.tvLocation);
        tvBookingRefId =  mView.findViewById(R.id.tvBookingRefId);
        tvCost=  mView.findViewById(R.id.tvCost);

        tvParkingCost=  mView.findViewById(R.id.tvParkingCost);
        tvParkingCostHeader=  mView.findViewById(R.id.tvParkingCostHeader);

//        if (prefs.getUserType().equalsIgnoreCase("U")) {
            tvParkingCost.setVisibility(View.VISIBLE);
            tvParkingCostHeader.setVisibility(View.VISIBLE);
//        }
        tvDate =  mView.findViewById(R.id.tvDate);
        tvDuration=  mView.findViewById(R.id.tvDuration);

        tvStarttime=  mView.findViewById(R.id.tvStarttime);
        tvName=  mView.findViewById(R.id.tvName);
        tvDOB=  mView.findViewById(R.id.tvDOB);
        tvSex=  mView.findViewById(R.id.tvSex);
        tvAditionalPassengerHeader=  mView.findViewById(R.id.tvAditionalPassengerHeader);
        tvnoofpasenger=  mView.findViewById(R.id.tvnoofpasenger);
        tvPickLoc=  mView.findViewById(R.id.tvPickLoc);
        tvDropLoc=  mView.findViewById(R.id.tvDropLoc);
        tvAddlReq=  mView.findViewById(R.id.tvAddlReq);
        tvAddlInfo=  mView.findViewById(R.id.tvAddlInfo);
        tvPurposeOfHire=  mView.findViewById(R.id.tvPurposeOfHire);
        tvExpectedTravelItinerary=  mView.findViewById(R.id.tvExpectedTravelItinerary);
        tvAditionalPassengerLinear=mView.findViewById(R.id.tvAditionalPassengerLinear);
        tvAditionalPassengerArrow= mView.findViewById(R.id.tvAditionalPassengerArrow);

        rlCancelLayout = mView.findViewById(R.id.rlCancelLayout);
        tvcancelInfoHeader = mView.findViewById(R.id.tvcancelInfoHeader);
        tvCancelledByHeader = mView.findViewById(R.id.tvCancelledByHeader);
        tvCancelReasonHeader = mView.findViewById(R.id.tvCancelReasonHeader);
        tvCancellationChargeHeader = mView.findViewById(R.id.tvCancellationChargeHeader);
        tvRefundableAmountHeader = mView.findViewById(R.id.tvRefundableAmountHeader);

        tvCancelledBy = mView.findViewById(R.id.tvCancelledBy);
        tvCancelReason = mView.findViewById(R.id.tvCancelReason);
        tvCancellationCharge = mView.findViewById(R.id.tvCancellationCharge);
        tvRefundableAmount = mView.findViewById(R.id.tvRefundableAmount);

        btn_View_Rating = mView.findViewById(R.id.btn_View_Rating);

        if (prefs.getUserType().equalsIgnoreCase("C")){
            btn_View_Rating.setVisibility(View.VISIBLE);
        }


        tvAditionalPassengerArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AddtionPasengerFlagvisible){
                    AddtionPasengerFlagvisible=false;
                    tvAditionalPassengerArrow.setImageResource(R.drawable.filter_drop_arrow);
                    tvAditionalPassengerLinear.setVisibility(View.GONE);
                }else{
                    AddtionPasengerFlagvisible=true;
                    tvAditionalPassengerArrow.setImageResource(R.drawable.filter_drop_arrow_up);
                    tvAditionalPassengerLinear.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void listener() {

        btn_View_Rating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                transitionflag = StaticClass.transitionflagNext;
                Intent riderRatingIntent = new Intent(getActivity(), ViewRatingActivity.class);
                riderRatingIntent.putExtra("passenger_id", bookingInformationModel.getUser_id());
                startActivity(riderRatingIntent);

            }
        });
    }

    private void SetData(){

        if(!"".equals(bookingInformationModel.getVehicle_city()) && !"null".equals(bookingInformationModel.getVehicle_city())){
            tvLocation.setText(bookingInformationModel.getVehicle_city());
        }else{
            tvLocation.setText("N/A");
        }

        if (!TextUtils.isEmpty(bookingInformationModel.getBooking_id())){
            tvBookingRefId.setText(bookingInformationModel.getBooking_id());
        }else {
            tvBookingRefId.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getCost()) && !"null".equals(bookingInformationModel.getCost())){
            tvCost.setText(getActivity().getResources().getString(R.string.Rs)+" "+bookingInformationModel.getCost());
        }else{
            tvCost.setText("N/A");
        }


        if(!TextUtils.isEmpty(bookingInformationModel.getToll_parking_charge())){
            tvParkingCost.setText(bookingInformationModel.getToll_parking_charge());
        }else{
            tvParkingCost.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getBooking_date()) && !"null".equals(bookingInformationModel.getBooking_date())){
            tvDate.setText(bookingInformationModel.getBooking_date());
        }else{
            tvDate.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getDuration()) && !"null".equals(bookingInformationModel.getDuration())){
            tvDuration.setText(bookingInformationModel.getDuration()+" hrs");
        }else{
            tvDuration.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getStart_time()) && !"null".equals(bookingInformationModel.getStart_time())){
            try {
                final SimpleDateFormat sdf = new SimpleDateFormat("H:mm");
                final Date dateObj = sdf.parse(bookingInformationModel.getStart_time());
                tvStarttime.setText(new SimpleDateFormat("hh:mm a").format(dateObj));

                convert12(bookingInformationModel.getStart_time());
            } catch (final ParseException e) {
                e.printStackTrace();
            }
        }else{
            tvStarttime.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getCustomer_name()) && !"null".equals(bookingInformationModel.getCustomer_name())){
            tvName.setText(bookingInformationModel.getCustomer_name());
        }else{
            tvName.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getCustomer_dob()) && !"null".equals(bookingInformationModel.getCustomer_dob())){
            tvDOB.setText(bookingInformationModel.getCustomer_dob());
        }else{
            tvDOB.setText("N/A");
        }


        if(!"".equals(bookingInformationModel.getCustomer_sex()) && !"null".equals(bookingInformationModel.getCustomer_sex())){
            String sx=bookingInformationModel.getCustomer_sex();
            String sex="";
            if(sx.equalsIgnoreCase("M")){
                sex="Male";
            }else if(sx.equalsIgnoreCase("F")){
                sex="Female";
            }else if(sx.equalsIgnoreCase("O")){
                sex="Others";
            }
            tvSex.setText(sex);
        }else{
            tvSex.setText("Others");
        }

        if(!"".equals(bookingInformationModel.getTotal_passenger()) && !"null".equals(bookingInformationModel.getTotal_passenger())){
            tvnoofpasenger.setText(bookingInformationModel.getTotal_passenger());
        }else{
            tvnoofpasenger.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getPickup_location()) && !"null".equals(bookingInformationModel.getPickup_location())){
            tvPickLoc.setText(bookingInformationModel.getPickup_location());
        }else{
            tvPickLoc.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getDrop_location()) && !"null".equals(bookingInformationModel.getDrop_location())){
            tvDropLoc.setText(bookingInformationModel.getDrop_location());
        }else{
            tvDropLoc.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getAddl_request()) && !"null".equals(bookingInformationModel.getAddl_request())){
            tvAddlReq.setText(bookingInformationModel.getAddl_request());
        }else {
            tvAddlReq.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getAddl_info()) && !"null".equals(bookingInformationModel.getAddl_info())){
            tvAddlInfo.setText(bookingInformationModel.getAddl_info());
        }else{
            tvAddlInfo.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getBooking_purpose()) && !"null".equals(bookingInformationModel.getBooking_purpose())){
            tvPurposeOfHire.setText(bookingInformationModel.getBooking_purpose());
        }else {
            tvPurposeOfHire.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getExpected_route()) && !"null".equals(bookingInformationModel.getExpected_route())){
            tvExpectedTravelItinerary.setText(bookingInformationModel.getExpected_route());
        }else{
            tvExpectedTravelItinerary.setText("N/A");
        }

        if(!"".equals(bookingInformationModel.getPassengerList()) && !"null".equals(bookingInformationModel.getPassengerList())){
            tvAditionalPassengerHeader.setText(getActivity().getString(R.string.Additional_Passenger)+" ("+bookingInformationModel.getPassengerList().size()+")");
        }else{
            tvAditionalPassengerHeader.setText("N/A");

        }

        tvAditionalPassengerLinear.removeAllViews();
        for(int i=0;i<bookingInformationModel.getPassengerList().size();i++){
            tvAditionalPassengerLinear.addView(addView2(i));
        }

        //check booking status to make the cancellation field visible or not
        if (!"".equals(bookingInformationModel.getBooking_status()) && !"null".equals(bookingInformationModel.getBooking_status())){

            //Booking status = 2 for cancelled car booking. Then only show those 4 cancel data.
            if ("2".equals(bookingInformationModel.getBooking_status())){
                rlCancelLayout.setVisibility(View.VISIBLE);

                if (!"".equals(bookingInformationModel.getCancel_by()) && !"null".equals(bookingInformationModel.getCancel_by())){
                    tvCancelledBy.setText(bookingInformationModel.getCancel_by());
                }else {
                    tvCancelledBy.setText("N/A");
                }

                if (!"".equals(bookingInformationModel.getCancel_reason()) && !"null".equals(bookingInformationModel.getCancel_reason())){
                    tvCancelReason.setText(bookingInformationModel.getCancel_reason());
                }else {
                    tvCancelReason.setText("N/A");
                }

                if (!"".equals(bookingInformationModel.getCancel_charge()) && !"null".equals(bookingInformationModel.getCancel_charge())){
                    tvCancellationCharge.setText(getResources().getString(R.string.Rs)+" "+bookingInformationModel.getCancel_charge());
                }else {
                    tvCancellationCharge.setText("N/A");
                }

                if (!"".equals(bookingInformationModel.getRefundable_amount()) && !"null".equals(bookingInformationModel.getRefundable_amount())){
                    tvRefundableAmount.setText(getResources().getString(R.string.Rs)+" "+bookingInformationModel.getRefundable_amount());
                }else {
                    tvRefundableAmount.setText("N/A");
                }
            }
        }

    }


    public static void convert12(String str)
    {
// Get Hours
        int h1 = (int)str.charAt(0) - '0';
        int h2 = (int)str.charAt(1) - '0';

        int hh = h1 * 10 + h2;

        int m1 = (int) str.charAt(3) - '0';
        int m2 = (int) str.charAt(4) - '0';
        int mm = m1 * 10 + m2;

        // Finding out the Meridien of time
        // ie. AM or PM
        String Meridien;
        if (hh < 12) {
            Meridien = "AM";
        }
        else
            Meridien = "PM";

        hh %= 12;

        // Handle 00 and 12 case separately
        if (hh == 0) {
            System.out.print("12");

            // Printing minutes and seconds
            for (int i = 2; i < 8; ++i) {
                System.out.print(str.charAt(i));
            }
        }
        else {
            System.out.print(hh);
            // Printing minutes and seconds
            for (int i = 2; i < 8; ++i) {
                System.out.print(str.charAt(i));
            }
        }

        // After time is printed
        // cout Meridien
        System.out.println(" "+Meridien);
    }



    public View addView2(final int position) {

        View v;
        v = LayoutInflater.from(getActivity()).inflate(R.layout.addtionalpassenger_inflate, null);

        TextView tvPassengerName = v.findViewById(R.id.tvPassengerName);
        TextView tvPassengerContact = v.findViewById(R.id.tvPassengerContact);

        tvPassengerName.setText(bookingInformationModel.getPassengerList().get(position).getPassenger_name());
        tvPassengerContact.setText(bookingInformationModel.getPassengerList().get(position).getPassenger_contact());

        if(!TextUtils.isEmpty(bookingInformationModel.getPassengerList().get(position).getPassenger_contact())){

            tvPassengerContact.setTextColor(Color.parseColor("#1e65a6"));
            tvPassengerContact.setPaintFlags(tvPassengerContact.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
        }

        tvPassengerContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!TextUtils.isEmpty(bookingInformationModel.getPassengerList().get(position).getPassenger_contact())) {

                    contactPermission(bookingInformationModel.getPassengerList().get(position).getPassenger_contact());
//                    callMethod(bookingInformationModel.getPassengerList().get(position).getPassenger_contact());
                }
            }
        });

        return v;
    }


    private void contactPermission(final String contact_mobile){

        Dexter.withActivity(getActivity()).withPermissions( Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_PHONE_STATE)
                .withListener(new MultiplePermissionsListener()
                {
                    @RequiresApi(api = Build.VERSION_CODES.ECLAIR)
                    @Override public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if(report.areAllPermissionsGranted()){

                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + contact_mobile));
                            startActivity(intent);
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // permission is denied permenantly, navigate user to app settings
                            showAlert();
                        }
                    }
                    @Override public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
                    {/* ... */

                        token.continuePermissionRequest();
                    }
                }).check();
    }

    public void showAlert(){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setTitle("Permissions Required")
                .setMessage("You have forcefully denied some of the required permissions " +
                        "for this action. Please open settings, go to permissions and allow them.")
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                Uri.fromParts("package", getContext().getPackageName(), null));
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setCancelable(false)
                .create()
                .show();
    }

    private void callMethod(String passenger_contact) {
        if (Build.VERSION.SDK_INT >= 23) {
            // Marshmallow+
            if (!checkCallPhonePermission() || !checkReadStatePermission()) {
                requestPermission();
            } else {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + passenger_contact));
                startActivity(intent);
            }
        } else {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + passenger_contact));
            startActivity(intent);
        }

    }

    private boolean checkCallPhonePermission() {
        int result = ContextCompat.checkSelfPermission(mView.getContext(), Manifest.permission.CALL_PHONE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkReadStatePermission() {
        int result = ContextCompat.checkSelfPermission(mView.getContext(), Manifest.permission.READ_PHONE_STATE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.READ_PHONE_STATE}, PERMISSION_REQUEST_CODE);
    }
}
