package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomViewCompany;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;
import com.neo.cars.app.fragment.CompanyDriverListPager;

public class CompanyDriverDetailsActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    private TabLayout tabs;
    private ViewPager viewpager;
    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private ImageButton ibNavigateMenu, ibEditMenu;
    private CustomTextviewTitilliumWebRegular rightTopBarText;

    private int transitionflag = StaticClass.transitionflagNext;
    private RelativeLayout rlBackLayout, rlAddLayout;
    private String driverId="", strVehicleStatus="", strDriverId="";
    private BottomViewCompany bottomViewCompany = new BottomViewCompany();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_driver_details);

        StaticClass.isVehicleAvailabilitySave = false;

        new AnalyticsClass(CompanyDriverDetailsActivity.this);

        driverId=getIntent().getExtras().getString("driverId");
        Log.d("*DriverId*", driverId);
//        strVehicleStatus = getIntent().getExtras().getString("VehicleStatus");

        Initialize();
        Listener();
    }

    private void Initialize() {

        bottomViewCompany.BottomViewCompany(CompanyDriverDetailsActivity.this, StaticClass.Menu_DriverLists_company);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvEnlistYourDriver));

        rlBackLayout = findViewById(R.id.rlBackLayout);

        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);

        rlAddLayout = findViewById(R.id.rlAddLayout);
        rlAddLayout.setVisibility(View.VISIBLE);
        rightTopBarText= findViewById(R.id.rightTopBarText);
        rightTopBarText.setVisibility(View.GONE);
        ibEditMenu = findViewById(R.id.ibEditMenu);
        ibEditMenu.setVisibility(View.VISIBLE);

        viewpager = findViewById(R.id.viewpager);

        tabs = findViewById(R.id.tabs);
        //Adding the tabs using addTab() method
        tabs.addTab(tabs.newTab().setText("Driver Information"));
        tabs.addTab(tabs.newTab().setText("Availability"));
        tabs.setTabGravity(TabLayout.GRAVITY_FILL);

        //Creating our pager adapter
        final CompanyDriverListPager adapter = new CompanyDriverListPager(getSupportFragmentManager(), tabs.getTabCount(), driverId);

        //Adding adapter to pager
        viewpager.setAdapter(adapter);

        //Adding onTabSelectedListener to swipe views
        tabs.setOnTabSelectedListener(this);

        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabs));

//        bottomview.BottomView(CompanyVehicleDetailsActivity.this,StaticClass.Menu_profile);
    }

    private void Listener() {

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

        ibEditMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag= StaticClass.transitionflagNext;
                Intent editIntent = new Intent(CompanyDriverDetailsActivity.this, CompanyEditDriverActivity.class);
                editIntent.putExtra("driverId", driverId);
                startActivity(editIntent);

                finish();

            }
        });
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewpager.setCurrentItem(tab.getPosition());
        int pos = tab.getPosition();
        if (pos == 1){
            rlAddLayout.setVisibility(View.GONE);
        }else if (pos == 0){
            rlAddLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyDriverDetailsActivity.this, transitionflag);
    }


    @Override
    protected void onResume() {
        super.onResume();
//        if(StaticClass.MyVehicleAddUpdate) {
//            transitionflag = StaticClass.transitionflagBack;
//            finish();
//        }

        //StaticClass.BottomProfile = false;
//        if(StaticClass.BottomProfile || StaticClass.MyVehicleIsDelete){
//            finish();
//        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();
        }

        if (StaticClass.isVehicleAvailabilitySave){
            finish();
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomViewCompany.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View v = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (v instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];
            if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom())) {
                try {
                    InputMethodManager inputManager = (InputMethodManager) CompanyDriverDetailsActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getApplicationWindowToken(), 0);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }
}
