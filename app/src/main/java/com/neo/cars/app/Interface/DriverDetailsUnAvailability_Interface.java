package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.DriverUnAvailabilityDetailsModel;

public interface DriverDetailsUnAvailability_Interface {

    void driverUnAvailableDetails(DriverUnAvailabilityDetailsModel driverUnAvailabilityModel, String status, String msg);
}
