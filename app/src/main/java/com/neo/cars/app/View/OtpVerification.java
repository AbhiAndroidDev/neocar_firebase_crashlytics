package com.neo.cars.app.View;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.neo.cars.app.NetworkNotAvailable;
import com.neo.cars.app.R;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Webservice.MobileLogin_Webservice;

/**
 * Created by joydeep on 6/3/18.
 */

public class OtpVerification {

    private View v;
    private Activity mcontext;
    private Context ab;
    private EditText et_otpnumber;
    private Button btnGenerateOtp;
    private String st_mobile="", st_otp="", userType="";
    private ConnectionDetector cd;

    public View OtpVerification(Activity context,String mobile, String otp, String user_type){
        mcontext = context;
        ab = context;
        st_mobile=mobile;
        st_otp=otp;
        userType = user_type;

        v = LayoutInflater.from(mcontext).inflate(R.layout.login_with_otp, null);

        Initialize();
        Listener();
        return v;
    }

    private void Initialize() {

        cd = new ConnectionDetector(ab);

        et_otpnumber = v.findViewById(R.id.et_otpnumber);
        btnGenerateOtp = v.findViewById(R.id.btnGenerateOtp);
    }

    private void Listener(){

        btnGenerateOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean cancel = false;
                View focusView = null;

                String sotp=et_otpnumber.getText().toString().trim();

                if (TextUtils.isEmpty(sotp)) {
                    et_otpnumber.setError(mcontext.getString(R.string.error_field_required));
                    focusView = et_otpnumber;
                    cancel = true;
                }

                if (cancel) {
                    focusView.requestFocus();
                }else{

                    if(cd.isConnectingToInternet()){
                        new MobileLogin_Webservice().MobileLogin(mcontext,st_mobile,sotp, userType);
                    }else{
                        Intent i = new Intent(ab, NetworkNotAvailable.class);
                        ab.startActivity(i);
                    }
                }
            }
        });
    }
}
