package com.neo.cars.app.SetGet;

public class VehicleUnAvailabilityModel {

    String unavailable_date;

    public String getUnavailable_date() {
        return unavailable_date;
    }

    public void setUnavailable_date(String unavailable_date) {
        this.unavailable_date = unavailable_date;
    }
}
