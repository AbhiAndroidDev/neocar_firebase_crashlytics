package com.neo.cars.app.Interface;

import com.neo.cars.app.SetGet.StateListModel;
import com.neo.cars.app.SetGet.UserDocumentModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;

import java.util.ArrayList;

/**
 * Created by parna on 23/3/18.
 */

public interface StateListInterface {

    void StateList(ArrayList<StateListModel> arrlistState);

}
