package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.FilterVehicleType_Interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 7/5/18.
 */

public class FilterVehicleType_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "";
    private CustomDialog pdCusomeDialog;
    private Gson gson;
    private SharedPrefUserDetails sharedPref;
    private VehicleTypeModel vehicleTypeModel;
    private ArrayList<VehicleTypeModel> arrlistVehicleType;
    private JSONObject jsonObjectVehicletype;

    public void vehicletypeWebservice (Activity context){

        mcontext = context;

        sharedPref = new SharedPrefUserDetails(mcontext);
        arrlistVehicleType  = new ArrayList<>();
        gson = new Gson();
        vehicleTypeModel = new VehicleTypeModel();


        VehicleTypeModel vehicleTypeModel = new VehicleTypeModel();
        vehicleTypeModel.setId("");
        vehicleTypeModel.setType_name("Any");
        vehicleTypeModel.setSitting_capacity("any");
        vehicleTypeModel.setLuggae_capacity("any");

        arrlistVehicleType.add(vehicleTypeModel);

        showProgressDialog();

        StringRequest vehicletypepRequest = new StringRequest(Request.Method.POST, Urlstring.vehicle_type,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        vehicletypepRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(vehicletypepRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response) {
        JSONObject jobj_main = null;
        String max_slider = "", min_slider = "";

        try {
            jobj_main = new JSONObject(response);

            Msg = jobj_main.optJSONObject("vehicle_type").optString("message");
            Status = jobj_main.optJSONObject("vehicle_type").optString("status");
            max_slider = jobj_main.optJSONObject("vehicle_type").optString("max_slider");
            min_slider = jobj_main.optJSONObject("vehicle_type").optString("min_slider");



            JSONArray jsonArrayDetails = jobj_main.optJSONObject("vehicle_type").optJSONArray("details");
            for (int i = 0; i < jsonArrayDetails.length(); i++) {
                jsonObjectVehicletype = jsonArrayDetails.optJSONObject(i);

                VehicleTypeModel vehicleTypeModel = new VehicleTypeModel();
                vehicleTypeModel.setId(jsonObjectVehicletype.optString("id"));
                vehicleTypeModel.setType_name(jsonObjectVehicletype.optString("type_name"));
                vehicleTypeModel.setSitting_capacity(jsonObjectVehicletype.optString("sitting_capacity"));
                vehicleTypeModel.setLuggae_capacity(jsonObjectVehicletype.optString("luggae_capacity"));

                arrlistVehicleType.add(vehicleTypeModel);

            }
            Log.d("VehicleListModel size:", "" + arrlistVehicleType.size());
            Log.e("VehicleListModel", "" + arrlistVehicleType);

        } catch (Exception e) {
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);
        }

        if (Status.equals(StaticClass.SuccessResult)) {
            ((FilterVehicleType_Interface)mcontext).VehicleTypeList(arrlistVehicleType, max_slider, min_slider);

        }

    }



}
