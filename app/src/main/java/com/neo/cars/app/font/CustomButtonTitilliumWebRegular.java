package com.neo.cars.app.font;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by parna on 12/3/18.
 */

public class CustomButtonTitilliumWebRegular extends AppCompatButton {
    public CustomButtonTitilliumWebRegular(Context context) {
        super(context);
    }

    public CustomButtonTitilliumWebRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomButtonTitilliumWebRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        if(!isInEditMode()){
            //------ Font------//
            Typeface typeface1 = Typeface.createFromAsset(getContext().getAssets(), "fonts/TitilliumWeb-Regular.ttf");
            //Typeface typeface2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/SaintAndrewdesKiwis.ttf");

            setTypeface(typeface1,   Typeface.NORMAL);
        }

    }


}
