package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SetGet.VehicleTypeModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.ThankYouActivity;
import com.neo.cars.app.Utils.CustomDialogText;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 10/5/18.
 */

public class ProcessBooking_webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strUserDeleted="";
    private SharedPrefUserDetails sharedPref;
    private CustomDialogText pdCusomeDialog;
    private Gson gson;
    private VehicleTypeModel vehicleTypeModel;
    private UserLoginDetailsModel UserLoginDetails;
    private JSONObject details;
    private JSONObject jsonObjDetails;
    private int transitionflag = StaticClass.transitionflagNext;

    public void processBooking (Activity context, final String strVehicleId, final String strDriverId, final String strDate,
                                final String strPickUptime, final String strPickUpLocation, final String strDropLocation, final String strBookingHourValue,
                                final String strHirePurpose, final String strHirePurposeId, final String strPickUpLocationId, final String strDropLocationId,
                                final String strAddlPassenger, final String strAdditionalRequest, final String strAdditionalInfo, final String strExpectedRoute,
                                final String strNetPayableAmount, final  String strDiscountAmount, final String strNightCharge, final String strVoucherCode,
                                final String TxnId,final String TxnInfo){

        String netPayableamout = strNetPayableAmount;

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        sharedPref = new SharedPrefUserDetails(mcontext);
        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();
        vehicleTypeModel = new VehicleTypeModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest stringProcessBookingRequest = new StringRequest(Request.Method.POST, Urlstring.process_booking,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response Booking hours", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }){

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("user_id", UserLoginDetails.getId());
                params.put("vehicle_id", strVehicleId);
                params.put("driver_id", strDriverId);
                params.put("booking_date", strDate);
                params.put("start_time", strPickUptime);
                params.put("pickup_location", strPickUpLocation);
                params.put("drop_location", strDropLocation);
                params.put("duration", strBookingHourValue);
                params.put("booking_purpose_id", strHirePurposeId);
                params.put("net_payable_amount", strNetPayableAmount);
                Log.d("night_charge***", strNightCharge);
                params.put("night_charge", strNightCharge);

                if(strPickUpLocationId != null){
                    params.put("pickup_location_id", strPickUpLocationId);
                }

                if(strDropLocationId != null){
                    params.put("drop_location_id", strDropLocationId);
                }

                if(strAddlPassenger != null){
                    params.put("addl_passenger", strAddlPassenger);
                }

                if(strAdditionalRequest != null){
                    params.put("addl_request", strAdditionalRequest);
                }

                if(strAdditionalInfo != null){
                    params.put("addl_info", strAdditionalInfo);
                }

                if(strExpectedRoute != null){
                    params.put("expected_route", strExpectedRoute);
                }

//                params.put("net_payable_amount", strNetPayableAmount);

                if(strDiscountAmount != null){
                    params.put("discount_amount", strDiscountAmount);
                }

                if(strVoucherCode != null){
                    params.put("voucher_code", strVoucherCode);
                }

                params.put("transaction_id", TxnId);
                params.put("transaction_info", TxnInfo);

                new PrintClass("process booking params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token", sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***" + params);
                return params;
            }
        };

        stringProcessBookingRequest.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(context).add(stringProcessBookingRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialogText(mcontext,mcontext.getResources().getString(R.string.BookingWaitMsg));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Apiparsedata(String response){
        JSONObject jobj_main = null;
        Bundle bnd=new Bundle();
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("process_booking").optString("user_deleted");
            Msg = jobj_main.optJSONObject("process_booking").optString("message");
            Status = jobj_main.optJSONObject("process_booking").optString("status");

            jsonObjDetails = jobj_main.optJSONObject("process_booking").optJSONObject("details");

            Log.e("driver_id", jsonObjDetails.optString("driver_id"));
            if(Status.equals(StaticClass.SuccessResult)){
                vehicleTypeModel.setBooking_id(jsonObjDetails.optString("booking_id"));
                vehicleTypeModel.setUser_id(jsonObjDetails.optString("user_id"));
                vehicleTypeModel.setDriver_id(jsonObjDetails.optString("driver_id"));
                vehicleTypeModel.setVehicle_id(jsonObjDetails.optString("vehicle_id"));
                vehicleTypeModel.setBooking_date(jsonObjDetails.optString("booking_date"));
                vehicleTypeModel.setStart_time(jsonObjDetails.optString("start_time"));
                vehicleTypeModel.setEnd_time(jsonObjDetails.optString("end_time"));
                vehicleTypeModel.setTotal_passenger(jsonObjDetails.optString("total_passenger"));
                vehicleTypeModel.setDuration(jsonObjDetails.optString("duration"));
                vehicleTypeModel.setPickup_location(jsonObjDetails.optString("pickup_location"));
                vehicleTypeModel.setDrop_location(jsonObjDetails.optString("drop_location"));
                vehicleTypeModel.setTotal_amount(jsonObjDetails.optString("total_amount"));
                vehicleTypeModel.setNight_charge(jsonObjDetails.optString("night_charge"));
                vehicleTypeModel.setNet_payable_amount(jsonObjDetails.optString("net_payable_amount"));
                vehicleTypeModel.setVehicle_city_name(jsonObjDetails.optString("vehicle_city_name"));

                bnd.putString("booking_date",jsonObjDetails.optString("booking_date"));
                Log.d("****start_time*****", jsonObjDetails.optString("start_time"));
                bnd.putString("start_time",jsonObjDetails.optString("start_time"));
                bnd.putString("duration",jsonObjDetails.optString("duration"));
                bnd.putString("pickup_location",jsonObjDetails.optString("pickup_location"));
                bnd.putString("net_payable_amount",jsonObjDetails.optString("net_payable_amount"));
                bnd.putSerializable("vehicle_city_name", jsonObjDetails.optString("vehicle_city_name"));

            }else if (Status.equals("0")){
                //new CustomToast(mcontext, Msg);
                details = jobj_main.optJSONObject("process_booking").optJSONObject("details");
                JSONArray jarrError = details.optJSONArray("errors");
                if (jarrError.length()>0){
                    Msg = jarrError.getString(0);
                    new CustomToast(mcontext, Msg);
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else{
            if (Status.equals(StaticClass.SuccessResult)) {
                Intent thankyouIntent = new Intent(mcontext, ThankYouActivity.class);
                thankyouIntent.putExtras(bnd);
                mcontext.startActivity(thankyouIntent);
                mcontext.finish();
            } else {
            }
        }
    }
}
