package com.neo.cars.app.app;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.multidex.MultiDex;
import android.support.v4.content.ContextCompat;

//import com.google.android.gms.analytics.GoogleAnalytics;
//import com.google.android.gms.analytics.Tracker;
//import com.twitter.sdk.android.core.DefaultLogger;
//import com.twitter.sdk.android.core.Twitter;
//import com.twitter.sdk.android.core.TwitterAuthConfig;
//import com.twitter.sdk.android.core.TwitterConfig;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;


/**
 * Created by kamail on 30/8/17.
 */

public class App extends Application {
    Environment environment;
    //private FirebaseAnalytics mFirebaseAnalytics;

    public enum TrackerName {
        APP_TRACKER, // tracker used only in this app
        GLOBAL_TRACKER, // tracker used by all the apps from a company . eg: roll-up tracking.
        ECOMMERCE_TRACKER, // tracker used by all ecommerce transactions from a company .
    }

//    public HashMap<TrackerName, Tracker> mTrackers = new HashMap<>();

//    public synchronized Tracker getTracker(TrackerName trackerId) {
//        if (!mTrackers.containsKey(trackerId)) {
//            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
//            Tracker tracker = (trackerId == TrackerName.APP_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
//                    : (trackerId == TrackerName.GLOBAL_TRACKER) ? analytics.newTracker(R.xml.global_tracker)
//                    : analytics.newTracker("My Tracker");
//            mTrackers.put(trackerId, tracker);
//        }
//        return mTrackers.get(trackerId);
//    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        //mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

      //  Fabric.with(this, new Crashlytics());
//        TwitterConfig config = new TwitterConfig.Builder(this)
//                .logger(new DefaultLogger(Log.DEBUG))
//                .twitterAuthConfig(new TwitterAuthConfig(StaticClass.TWITTER_KEY, StaticClass.TWITTER_SECRET_KEY))
//                .debug(true)
//                .build();
//        Twitter.initialize(config);

        environment = Environment.TEST;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(
                this, Manifest.permission.WRITE_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_GRANTED) { // Permission is not granted return false;
            return true;
        }
        return false;
    }
}