package com.neo.cars.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.neo.cars.app.Utils.AdvancedWebView;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.ConnectionDetector;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.Webservice.Urlstring;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

public class CompanyCmsPageActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private ImageButton ibNavigateMenu;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title ;

    private Bundle bundle;
    private String strCmsValue="";
    private ConnectionDetector cd;
    private Context context;
    private AdvancedWebView webView;
    private RelativeLayout rlBackLayout;

    private int transitionflag = StaticClass.transitionflagNext;
    private String SlugName="", urlpath="";
    private int count = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_cms_page);

        new AnalyticsClass(CompanyCmsPageActivity.this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        ibNavigateMenu = findViewById(R.id.ibNavigateMenu);
        rlBackLayout = findViewById(R.id.rlBackLayout);


        bundle = getIntent().getExtras();
        if(bundle != null){
            strCmsValue = bundle.getString("cms");
            // tv_toolbar_title.setText(strCmsValue);

            if (strCmsValue.equals(StaticClass.TermsCondition)) {
//                SlugName="terms-and-conditions";
                SlugName="company-terms-and-conditions";
                tv_toolbar_title.setText(getResources().getString(R.string.tvCompTermsAndCondition));
                urlpath = Urlstring.slug_url + "/"+SlugName;

            }else if (strCmsValue.equals(StaticClass.PrivacyPolicy)) {
                SlugName="privacy-policy";
                tv_toolbar_title.setText(getResources().getString(R.string.tvPrivacyPolicyHeader));
                urlpath = Urlstring.slug_url + "/"+SlugName;

            }else if (strCmsValue.equals(StaticClass.HowToUse)) {
//                SlugName="how-to-use";
                SlugName="neocars-guidelines";
                tv_toolbar_title.setText(getResources().getString(R.string.tvNeoCarsGuideLines));
                urlpath = Urlstring.slug_url + "/"+SlugName;

            }else if (strCmsValue.equals(StaticClass.FAQ)) {
//                SlugName="faq";
                SlugName="company-faq";
                tv_toolbar_title.setText(getResources().getString(R.string.tvCompanyFAQ));
                urlpath = Urlstring.slug_url + "/"+SlugName;

            } else if (strCmsValue.equals(StaticClass.ContactUs)) {
                SlugName="contact-us";
                tv_toolbar_title.setText(getResources().getString(R.string.tvContactUsHeader));
                urlpath = Urlstring.slug_url + "/"+SlugName;

            }else if (strCmsValue.equals(StaticClass.AboutUs)) {
                SlugName="about-us";
                tv_toolbar_title.setText(getResources().getString(R.string.tvAboutUsHeader));
                urlpath = Urlstring.slug_url + "/"+SlugName;
            }
        }

        Initialize();
        Listener();

    }


    private void Initialize(){
        context = this;
        cd = new ConnectionDetector(this);
        webView=(AdvancedWebView)findViewById(R.id.av_webView);


        if (NetWorkStatus.isNetworkAvailable(CompanyCmsPageActivity.this)) {
            startWebView(urlpath);

        } else {
            new CustomToast(this, "Please check your internet connection");

        }


        //   bottomview.BottomView(CmsPageActivity.this, StaticClass.Menu_profile);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       /* try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private void Listener(){

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });

    }

    private void startWebView(String url) {

        //Create new webview Client to show progress dialog
        //When opening a url or click on link

        webView.setWebViewClient(new WebViewClient() {
            ProgressDialog progressDialog;

            //If you will not use this method url links are opeen in new brower not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            //Show loader on url load
            public void onLoadResource (WebView view, String url) {
                try {
                    if (progressDialog == null) {
                        if(count==1) {
                            count++;
                            progressDialog = new ProgressDialog(CompanyCmsPageActivity.this);
                            progressDialog.setMessage("Loading...");
                            progressDialog.show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            public void onPageFinished(WebView view, String url) {
                try{
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                        progressDialog = null;
                    }
                }catch(Exception exception){
                    exception.printStackTrace();
                }
            }

        });

        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);
        //Load url in webview
        webView.loadUrl(url);

    }

    @Override
    public void onBackPressed() {
        transitionflag = StaticClass.transitionflagBack;
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(CompanyCmsPageActivity.this, transitionflag);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // StaticClass.BottomProfile = false;
        if(StaticClass.BottomProfileCompany){
            finish();
        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }

    }
}
