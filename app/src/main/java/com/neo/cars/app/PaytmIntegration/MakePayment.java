package com.neo.cars.app.PaytmIntegration;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.neo.cars.app.R;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

//implementing PaytmPaymentTransactionCallback to track the payment result.
public class MakePayment extends Activity implements PaytmPaymentTransactionCallback {
    private static final String PAYTM_GENERATE_CHECK_SUM = "http://neocars.testyourprojects.biz/paytmApp/generateChecksum.php";
    Paytm paytm;
    //the textview in the interface where we have the price
    TextView tvMessage;
    String customerid = "CUST123456", amount = "1.00";
    Button btnClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_payment_paytm);

//        if(getIntent().getExtras()!=null){
//            customerid=getIntent().getExtras().getString("cus_id");
//            amount=getIntent().getExtras().getString("amount");
//        }

        tvMessage=findViewById(R.id.tvMessage);
        tvMessage.setText("Please do not close the app until complete the payment.");
        btnClose=findViewById(R.id.btnClose);
        btnClose.setVisibility(View.INVISIBLE);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

         paytm = new Paytm(
                Constants.M_ID,
                Constants.CHANNEL_ID,
                amount,
                Constants.WEBSITE,
                Constants.CALLBACK_URL,
                Constants.INDUSTRY_TYPE_ID,
                customerid
        );

        //calling the method generateCheckSum() which will generate the paytm checksum for payment
        generateCheckSum();


    }

    private void generateCheckSum(){

        StringRequest request = new StringRequest(Request.Method.POST, PAYTM_GENERATE_CHECK_SUM, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                   System.out.println(jsonObject.toString(3));

                    initializePaytmPayment(jsonObject.getString("CHECKSUMHASH"), paytm);

                } catch (JSONException e) {

                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();

                param.put("MID",  paytm.getmId());
                param.put("ORDER_ID", paytm.getOrderId());
                param.put("CUST_ID", paytm.getCustId());
                param.put("CHANNEL_ID", paytm.getChannelId());
                param.put("TXN_AMOUNT", paytm.getTxnAmount());
                param.put("WEBSITE", paytm.getWebsite());
                param.put("CALLBACK_URL", paytm.getCallBackUrl() + paytm.getOrderId());
                param.put("INDUSTRY_TYPE_ID", paytm.getIndustryTypeId());

                System.out.println("***generateCheckSum param : " + param.toString());

                return param;
            }
        };

        request.setRetryPolicy((new DefaultRetryPolicy(30000, 1, 1.0f)));
        Volley.newRequestQueue(MakePayment.this).add(request);
    }


    private void initializePaytmPayment(String checksumHash, Paytm paytm) {

        //getting paytm service
        PaytmPGService Service = PaytmPGService.getStagingService("");

        //use this when using for production
        //PaytmPGService Service = PaytmPGService.getProductionService();

        //creating a hashmap and adding all the values required
        HashMap<String, String> paramMap = new HashMap<>();
        paramMap.put("MID", Constants.M_ID);
        paramMap.put("ORDER_ID", paytm.getOrderId());
        paramMap.put("CUST_ID", paytm.getCustId());
        paramMap.put("CHANNEL_ID", paytm.getChannelId());
        paramMap.put("TXN_AMOUNT", paytm.getTxnAmount());
        paramMap.put("WEBSITE", paytm.getWebsite());
        paramMap.put("CALLBACK_URL", paytm.getCallBackUrl() + paytm.getOrderId());
        paramMap.put("CHECKSUMHASH", checksumHash);
        paramMap.put("INDUSTRY_TYPE_ID", paytm.getIndustryTypeId());

        System.out.println("***PaytmOrder param : " + paramMap.toString());
        //creating a paytm order object using the hashmap
        PaytmOrder order = new PaytmOrder(paramMap);

        //intializing the paytm service
        Service.initialize(order, null);

        //finally starting the payment transaction
        Service.startPaymentTransaction(this, true, true, this);

    }

    //all these overriden method is to detect the payment result accordingly
    @Override
    public void onTransactionResponse(Bundle bundle) {

        Toast.makeText(this, bundle.toString(), Toast.LENGTH_LONG).show();
        tvMessage.setText("Your transaction has been completed successfully. ");
        btnClose.setVisibility(View.VISIBLE);
    }

    @Override
    public void networkNotAvailable() {
        Toast.makeText(this, "Network error", Toast.LENGTH_LONG).show();
    }

    @Override
    public void clientAuthenticationFailed(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void someUIErrorOccurred(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorLoadingWebPage(int i, String s, String s1) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressedCancelTransaction() {
        Toast.makeText(this, "Back Pressed", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onTransactionCancel(String s, Bundle bundle) {
        Toast.makeText(this, s + bundle.toString(), Toast.LENGTH_LONG).show();
    }
}
