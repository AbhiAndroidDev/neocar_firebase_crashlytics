package com.neo.cars.app.font;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by parna on 2/3/18.
 */

public class CustomEditTextTitilliumWebRegular extends AppCompatEditText {
    public CustomEditTextTitilliumWebRegular(Context context) {
        super(context);
    }

    public CustomEditTextTitilliumWebRegular(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomEditTextTitilliumWebRegular(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        if(!isInEditMode()){
            //------ Font------//
            Typeface typeface1 = Typeface.createFromAsset(getContext().getAssets(), "fonts/TitilliumWeb-Regular.ttf");
            //Typeface typeface2 = Typeface.createFromAsset(getContext().getAssets(),"fonts/SaintAndrewdesKiwis.ttf");

            setTypeface(typeface1,   Typeface.NORMAL);
        }

    }
}
