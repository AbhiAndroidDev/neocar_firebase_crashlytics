package com.neo.cars.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;

import com.neo.cars.app.Interface.AddRatingInterface;
import com.neo.cars.app.SetGet.AddRatingModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.AnalyticsClass;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.NetWorkStatus;
import com.neo.cars.app.Utils.OnPauseSlider;
import com.neo.cars.app.Utils.StaticClass;
import com.neo.cars.app.View.BottomView;
import com.neo.cars.app.Webservice.AddRating_Webservice;
import com.neo.cars.app.dialog.CustomAlertDialogOKCancel;
import com.neo.cars.app.font.CustomButtonTitilliumSemibold;
import com.neo.cars.app.font.CustomEditTextTitilliumWebRegular;
import com.neo.cars.app.font.CustomTextviewTitilliumWebRegular;

import java.util.ArrayList;

public class AddRatingActivity extends AppCompatActivity implements AddRatingInterface {

    private Toolbar toolbar;
    private CustomTextviewTitilliumWebRegular tv_toolbar_title;
    private CustomEditTextTitilliumWebRegular writeReview;
    private RelativeLayout rlBackLayout;
    private LinearLayout sendAnonymmousReviewll;
    private int transitionflag = StaticClass.transitionflagNext;
    private RatingBar ratingForVehicle, ratingForDriver;
    private CustomButtonTitilliumSemibold save, cancel;
    private CheckBox checkBox;
    private String isCheckedAnonymous = "N";
    private CustomEditTextTitilliumWebRegular edt;
    private float vehicleRating, driverRating;
    private String strWriteReview = "", vehicleId = "", driverId = "", bookingId = "";
    private Context context;
    private BottomView bottomview = new BottomView();
    private SharedPrefUserDetails sharedPrefUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_rating);
        new AnalyticsClass(AddRatingActivity.this);

        initialize();
        listener();
    }

    private void initialize() {
        context = AddRatingActivity.this;

        sharedPrefUser = new SharedPrefUserDetails(this);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);

        vehicleId = getIntent().getExtras().getString("vehicle_id");
        driverId = getIntent().getExtras().getString("driver_id");
        bookingId = getIntent().getExtras().getString("booking_id");

        tv_toolbar_title = findViewById(R.id.tv_toolbar_title);
        tv_toolbar_title.setText(getResources().getString(R.string.tvRatings));

        rlBackLayout = findViewById(R.id.rlBackLayout);
        ratingForVehicle = findViewById(R.id.rating_vehicle);
        ratingForDriver = findViewById(R.id.rating_driver);
        sendAnonymmousReviewll = findViewById(R.id.sendAnonymmousReviewll);
        checkBox = findViewById(R.id.check_anonymous);
        save = findViewById(R.id.btnSaveRating);
        cancel = findViewById(R.id.btnCancelRating);
        writeReview = findViewById(R.id.writeReview);

        bottomview.BottomView(AddRatingActivity.this, StaticClass.Menu_Search);

    }

    private void listener() {

        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBox.isChecked()) {
//                    checkBox.setChecked(false);
                    isCheckedAnonymous = "N";
                }else {
//                    checkBox.setChecked(true);
                    isCheckedAnonymous = "Y";
                }
            }
        });


        sendAnonymmousReviewll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkBox.isChecked()) {
                    checkBox.setChecked(false);
                    isCheckedAnonymous = "N";
                }else {
                    checkBox.setChecked(true);
                    isCheckedAnonymous = "Y";
                }
            }
        });

        ratingForVehicle.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                vehicleRating = ratingBar.getRating();
                Log.d("vehicleRating", ""+vehicleRating);
            }
        });

        ratingForDriver.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                driverRating = ratingBar.getRating();
                Log.d("driverRating", ""+driverRating);
            }
        });

        rlBackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(AddRatingActivity.this,
                        AddRatingActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                        AddRatingActivity.this.getResources().getString(R.string.yes),
                        AddRatingActivity.this.getResources().getString(R.string.no));
                alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                        transitionflag = StaticClass.transitionflagBack;
                        finish();

                    }
                });

                alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialogYESNO.dismiss();
                    }
                });

                alertDialogYESNO.show();
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkValidation();

            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transitionflag = StaticClass.transitionflagBack;
                finish();
            }
        });
    }

    private void checkValidation() {
        strWriteReview = writeReview.getText().toString().trim();
        String strVehicleRating = ""+vehicleRating;
        String strDriverRating = ""+driverRating;
        if (vehicleRating == 0.0){
            new CustomToast(AddRatingActivity.this, "Vehicle rating is mandatory");

        }else if (driverRating == 0.0){
            new CustomToast(AddRatingActivity.this, "Driver rating is mandatory");

        }else if (NetWorkStatus.isNetworkAvailable(context)){

            //CS means Car Seeker, CW means car Owner
                new AddRating_Webservice().addRating(AddRatingActivity.this, bookingId,
                        "CS", sharedPrefUser.getUserid(), strWriteReview, strVehicleRating,
                        strDriverRating, vehicleId, driverId, isCheckedAnonymous);

            }else {
                Intent i = new Intent(context, NetworkNotAvailable.class);
                startActivity(i);
            }
    }

    @Override
    public void onAddRating(ArrayList<AddRatingModel> addRatingModelList) {


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(StaticClass.BottomProfile){
            finish();
        }

        if( StaticClass.MyBookingListFlag){
           finish();

        }

        if (StaticClass.isLoginFalg) {
            transitionflag = StaticClass.transitionflagBack;
            finish();

        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        new OnPauseSlider(AddRatingActivity.this, transitionflag);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            bottomview.timer.cancel();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();

        final CustomAlertDialogOKCancel alertDialogYESNO=new CustomAlertDialogOKCancel(AddRatingActivity.this,
                AddRatingActivity.this.getResources().getString(R.string.Are_yousureyouwanttoexitwithoutsavinginformation),
                AddRatingActivity.this.getResources().getString(R.string.yes),
                AddRatingActivity.this.getResources().getString(R.string.no));
        alertDialogYESNO.setOnAcceptButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
                transitionflag = StaticClass.transitionflagBack;
                finish();

            }
        });

        alertDialogYESNO.setOnCancelButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialogYESNO.dismiss();
            }
        });

        alertDialogYESNO.show();

    }
}
