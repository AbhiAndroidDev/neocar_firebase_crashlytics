package com.neo.cars.app.Webservice;

import android.app.Activity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.neo.cars.app.Interface.CompanyDriverList_interface;
import com.neo.cars.app.R;
import com.neo.cars.app.SetGet.CompanyDriverListModel;
import com.neo.cars.app.SetGet.UserLoginDetailsModel;
import com.neo.cars.app.SharedPreference.SharedPrefUserDetails;
import com.neo.cars.app.Utils.CustomDialog;
import com.neo.cars.app.Utils.CustomToast;
import com.neo.cars.app.Utils.PrintClass;
import com.neo.cars.app.Utils.StaticClass;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by parna on 9/11/18.
 */

public class CompanyDriverList_Webservice {

    private Activity mcontext;
    private String Status = "0", Msg = "", strCanAddVehicle="", strUserDeleted="";

    private SharedPrefUserDetails sharedPref;
    private CustomDialog pdCusomeDialog;

    private Gson gson;
    private UserLoginDetailsModel UserLoginDetails;
    private ArrayList<CompanyDriverListModel> arrlistDriver = new ArrayList<>();
    private CompanyDriverList_interface companyDriverList_Interface;
    private int transitionflag = StaticClass.transitionflagNext;

    public void companyDriverList(Activity context, CompanyDriverList_interface mComapnyDriverList_Interface ){

        mcontext = context;
        Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        companyDriverList_Interface = mComapnyDriverList_Interface;

        sharedPref = new SharedPrefUserDetails(mcontext);

        gson = new Gson();
        UserLoginDetails=new UserLoginDetailsModel();

        String struserdetails = sharedPref.getObjectFromPreferenceUserDetails();
        UserLoginDetails = gson.fromJson(struserdetails, UserLoginDetailsModel.class);

        showProgressDialog();

        StringRequest userVehicleListRequest  = new StringRequest(Request.Method.POST, Urlstring.company_driver_list,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        hideProgressDialog();
                        Log.d("Response****", response);
                        Apiparsedata(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        try {
                            hideProgressDialog();
                            new CustomToast(mcontext, Msg);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("user_id", sharedPref.getUserid());

                new PrintClass("params******getParams***"+params);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("xsrf-token",sharedPref.getKEY_Access_Token());

                new PrintClass("params******Header***"+params);
                return params;
            }
        };

        userVehicleListRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 1, 1.0f));
        Volley.newRequestQueue(context).add(userVehicleListRequest);

    }

    private void showProgressDialog() {
        try {
            pdCusomeDialog = new CustomDialog(mcontext,mcontext.getResources().getString(R.string.PleaseWait));
            pdCusomeDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void hideProgressDialog() {
        try {
            if (pdCusomeDialog.isShowing()) {
                pdCusomeDialog.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void Apiparsedata(String response){

        JSONObject jobj_main = null;
        try{
            jobj_main = new JSONObject(response);

            strUserDeleted = jobj_main.optJSONObject("company_driver_list").optString("user_deleted");
            Msg = jobj_main.optJSONObject("company_driver_list").optString("message");
            Status= jobj_main.optJSONObject("company_driver_list").optString("status");

            JSONArray details=jobj_main.optJSONObject("company_driver_list").optJSONArray("details");
            if (Status.equals(StaticClass.SuccessResult)){
                arrlistDriver = new ArrayList<>();
                for (int i=0; i<details.length(); i++){

                    JSONObject jsonObject = details.optJSONObject(i);
                    CompanyDriverListModel companyDriverListModel = new CompanyDriverListModel();
                    companyDriverListModel.setDriver_id(jsonObject.optString("driver_id"));
                    companyDriverListModel.setName(jsonObject.optString("name"));
                    companyDriverListModel.setAge(jsonObject.optString("age"));
                    companyDriverListModel.setContactno(jsonObject.optString("contact_no"));
                    companyDriverListModel.setStatus(jsonObject.optString("status"));
                    companyDriverListModel.setRecent_photograph(jsonObject.optString("recent_photograph"));

                    arrlistDriver.add(companyDriverListModel);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            Msg = mcontext.getResources().getString(R.string.TryAfterSomeTime);

        }

        if ("Y".equalsIgnoreCase(strUserDeleted)){
            StaticClass.isLoginFalg=true;
            transitionflag = StaticClass.transitionflagBack;
            mcontext.finish();

        }else {
            if (Status.equals(StaticClass.SuccessResult)) {
                // ((UserVehicleList_Interface)).UserVehicleListInterface(arrlistVehicle);
                companyDriverList_Interface.companyDriverList(arrlistDriver);
            }
        }

    }
}
