package com.neo.cars.app.SetGet;

import java.util.ArrayList;

/**
 * Created by parna on 11/4/18.
 */

public class ExploreModel {

    String state_id;
    String state_name;
    String city_id;
    String city_name;
    String has_more_vehicle;
    ArrayList<ExploreVehicleListModel> arr_exploreVehicleListModel;

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getHas_more_vehicle() {
        return has_more_vehicle;
    }

    public void setHas_more_vehicle(String has_more_vehicle) {
        this.has_more_vehicle = has_more_vehicle;
    }

    public ArrayList<ExploreVehicleListModel> getArr_exploreVehicleListModel() {
        return arr_exploreVehicleListModel;
    }

    public void setArr_exploreVehicleListModel(ArrayList<ExploreVehicleListModel> arr_exploreVehicleListModel) {
        this.arr_exploreVehicleListModel = arr_exploreVehicleListModel;
    }
}




